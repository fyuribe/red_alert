//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace datos_cambios_procesos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cat_Unidades_Negocio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Cat_Unidades_Negocio()
        {
            this.Ope_Alertas_Rojas = new HashSet<Ope_Alertas_Rojas>();
        }
    
        public int Unidad_Negocio_ID { get; set; }
        public int Empresa_ID { get; set; }
        public int Planta_ID { get; set; }
        public int Estatus_ID { get; set; }
        public string Nombre { get; set; }
        public string Observaciones { get; set; }
        public string Usuario_Creo { get; set; }
        public Nullable<System.DateTime> Fecha_Creo { get; set; }
        public string Usuario_Modifico { get; set; }
        public Nullable<System.DateTime> Fecha_Modifico { get; set; }
    
        public virtual Apl_Empresas Apl_Empresas { get; set; }
        public virtual Apl_Estatus Apl_Estatus { get; set; }
        public virtual Apl_Plantas Apl_Plantas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ope_Alertas_Rojas> Ope_Alertas_Rojas { get; set; }
    }
}
