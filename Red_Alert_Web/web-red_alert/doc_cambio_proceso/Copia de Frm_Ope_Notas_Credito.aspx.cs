﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using SIAC.Sessiones;
using SIAC.Metodos_Generales;
using SIAC.Constantes;
using SIAC.Facturacion_Global.Negocios;
using SIAC.Estados.Negocios;
using SIAC.Ope_Facturas_Series.Negocio;
using SIAC_Ope_Facturas_Detalles.Negocio;
using SIAC_Apl_Parametros_Facturacion.Negocio;
using SIAC.Envio_Email;
using SIAC.Timbrado;
using SIAC.Bitacora.Negocios;
using SIAC.Seguridad;
using System.Xml.Linq;
using SIAC_Ope_Notas_Credito.Negocio;
using System.Text.RegularExpressions;
using System.Xml;
using System.Text;
using System.Data.SqlClient;


public partial class paginas_Comercializacion_Facturacion_Global_Frm_Ope_Notas_Credito : System.Web.UI.Page
{
    #region Page Load
    /*
     * Mensajes de error error, info, question, warning
     */

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Estado_Formulario("Inicial");//Habilita la configuración inicial de los controles de la página.
                Configuracion_Formulario(true);
                Cargar_Combo_Series();
                Cargar_Combo_Series_Facturas();
                Consulta_Parametro_Impuesto();
                Session["Dt_Factura_Selecionada"] = Hacer_Tabla_Factura_Seleccionada();
                Session["Dt_Partidas_Devolver"] = Hacer_Tabla_Partidas_Factura();
            }
            else
            {
                Muestra_Oculta_Divs();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }

    #endregion

    #region Metodos

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consulta_Parametro_Impuesto
    ///DESCRIPCIÓN: Consulta el porcentaje de Iva del sistema.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consulta_Parametro_Impuesto()
    {
        Cls_Apl_Parametros_Facturacion_Negocio Rs_Impuestos = new Cls_Apl_Parametros_Facturacion_Negocio();
        DataTable Dt_Impuestos = Rs_Impuestos.Consultar_Parametros();
        if (Dt_Impuestos != null)
        {
            if (Dt_Impuestos.Rows.Count > 0)
            {
                Lbl_Iva.Text = "0.0";
            }
        }
        else
        {
            Lbl_Iva.Text = "16";
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Muestra_Oculta_Divs
    ///DESCRIPCIÓN: Funcion que se llama cuando se hace un postback, para mostrar
    ///             los div's correspondientes al tipo de nota, y tambien habilita
    ///             el campo de tipo de cambio, si es Dolar.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Muestra_Oculta_Divs()
    {
        //Div_Descuento_Pago.Style.Add("display", "none");
        //Div_Devolucion.Style.Add("display", "none");
        String Tipo = Cmb_Tipo_Nota.SelectedValue.ToString();
        switch (Tipo)
        {
            case "DEVOLUCION":
                Div_Devolucion.Style.Add("display", "block");
                break;
            case "DESCUENTO":
                break;
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Construir_Detalles
    ///DESCRIPCIÓN: Crea el concepto que se va a utlizar para crear el xml y el pdf cuando
    ///             la nota de credito es por descuento o pronto pago.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  19/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Construir_Detalles(String Tipo)
    {
        DataTable Dt_Detalles_Facturas = Hacer_Tabla_Partidas_Factura();
        StringBuilder Str_Descripcion = new StringBuilder();

        Actualizar_Facturas_Seleccionadas();
        DataTable Dt_Facturas_Seleccionadas = new DataTable();
        Dt_Facturas_Seleccionadas = ((DataTable)Session["Dt_Factura_Seleccionada"]);

        Str_Descripcion.Length = 0;
        Str_Descripcion.Append("NOTA DE CREDITO POR " + Tipo + " PARA LAS FACTURAS ");
        int Cont_Filas = 0;
        foreach (DataRow Dr_Factura_Seleccionada in Dt_Facturas_Seleccionadas.Rows)
        {
            if (Cont_Filas > 0)
            {
                Str_Descripcion.Append(", ");
            }
            Str_Descripcion.Append(Dr_Factura_Seleccionada[Ope_Facturas.Campo_No_Factura].ToString() + " - ");
            Str_Descripcion.Append(Dr_Factura_Seleccionada[Ope_Facturas.Campo_Serie].ToString());
            Cont_Filas++;
        }

        DataRow Dr = Dt_Detalles_Facturas.NewRow();
        Dr[Ope_Notas_Credito_Detalle.Campo_Cantidad] = "1";
        Dr[Ope_Notas_Credito_Detalle.Campo_Codigo] = String.Empty;
        Dr[Ope_Notas_Credito_Detalle.Campo_Cuenta_Predial] = String.Empty;
        Dr[Ope_Notas_Credito_Detalle.Campo_Descripcion] = Str_Descripcion.ToString();
        Dr[Ope_Notas_Credito_Detalle.Campo_Unidad] = "NO APLICA";
        Dr[Ope_Notas_Credito_Detalle.Campo_Porcentaje_Impuesto] = Lbl_Iva.Text;
        Dr[Ope_Notas_Credito_Detalle.Campo_Precio_Unitario] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Txt_Subtotal.Text));
        Dr[Ope_Notas_Credito_Detalle.Campo_Subtotal_Detalle] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Txt_Subtotal.Text));
        Dr[Ope_Notas_Credito_Detalle.Campo_Iva] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Txt_Iva.Text));
        Dr[Ope_Notas_Credito_Detalle.Campo_Total_Detalle] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Txt_Total.Text));
        Dt_Detalles_Facturas.Rows.Add(Dr);

        Session["Dt_Factura_Seleccionada"] = Dt_Facturas_Seleccionadas;
        Rellena_Grid();

        return Dt_Detalles_Facturas;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Construir_Alta_Detalles
    ///DESCRIPCIÓN: Construye el datable necesario para el alta de los detalles de la
    ///             nota de credito cuando es por descuento o pronto pago.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  19/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Construir_Alta_Detalles()
    {
        DataTable Dt_Detalles_Facturas = Hacer_Tabla_Partidas_Factura();
        DataTable Dt_Factura = new DataTable();

        Actualizar_Facturas_Seleccionadas();
        DataTable Dt_Facturas_Seleccionadas = new DataTable();
        Dt_Facturas_Seleccionadas = ((DataTable)Session["Dt_Factura_Seleccionada"]);

        Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Facutra = new Cls_Ope_Cor_Facturacion_Global_Negocio();
        foreach (DataRow Dr_Factura_Seleccionada in Dt_Facturas_Seleccionadas.Rows)
        {
            Rs_Facutra.P_No_Factura = Dr_Factura_Seleccionada[Ope_Facturas.Campo_No_Factura].ToString();
            Rs_Facutra.P_Serie = Dr_Factura_Seleccionada[Ope_Facturas.Campo_Serie].ToString();
            Dt_Factura = Rs_Facutra.Consultar_Factura();

            if (Dt_Factura.Rows.Count > 0)
            {
                DataRow Dr = Dt_Detalles_Facturas.NewRow();
                Dr[Ope_Notas_Credito_Detalle.Campo_No_Factura] = Dt_Factura.Rows[0][Ope_Facturas.Campo_No_Factura].ToString(); // Guarda el número de factura
                Dr[Ope_Notas_Credito_Detalle.Campo_Serie_Factura] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Serie].ToString(); // Guarda la serie de la factura
                Dr[Ope_Notas_Credito_Detalle.Campo_Cantidad] = "1";
                Dr[Ope_Notas_Credito_Detalle.Campo_Codigo] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Tipo_Moneda].ToString(); // Guarda la moneda de la factura
                Dr[Ope_Notas_Credito_Detalle.Campo_Cuenta_Predial] = String.Empty;
                Dr[Ope_Notas_Credito_Detalle.Campo_Descripcion] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Razon_Social].ToString();
                Dr[Ope_Notas_Credito_Detalle.Campo_Unidad] = "NO APLICA";
                Dr[Ope_Notas_Credito_Detalle.Campo_Porcentaje_Impuesto] = String.Empty;
                Dr[Ope_Notas_Credito_Detalle.Campo_Precio_Unitario] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura.Rows[0][Ope_Facturas.Campo_Total].ToString())); // Total de la factura
                Dr[Ope_Notas_Credito_Detalle.Campo_Precio_Unitario] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura.Rows[0][Ope_Facturas.Campo_Total].ToString())); // Total de la factura
                Dr[Ope_Notas_Credito_Detalle.Campo_Subtotal_Detalle] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura.Rows[0][Ope_Facturas.Campo_Saldo].ToString())); // Saldo de la factura
                Dr[Ope_Notas_Credito_Detalle.Campo_Iva] = String.Format("{0:#,###0.#0}", 0);
                Dr[Ope_Notas_Credito_Detalle.Campo_Total_Detalle] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dr_Factura_Seleccionada["IMPORTE"].ToString())); // Importe

                Dt_Detalles_Facturas.Rows.Add(Dr);
            }
        }
        Session["Dt_Factura_Seleccionada"] = Dt_Facturas_Seleccionadas;
        Rellena_Grid();

        return Dt_Detalles_Facturas;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Construir_Alta_Detalles_Descuento
    ///DESCRIPCIÓN: 
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  19/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Construir_Alta_Detalles_Descuento(DataTable Dt_Detalles)
    {
        DataTable Dt_Factura = new DataTable();
        Actualizar_Facturas_Seleccionadas();
        DataTable Dt_Facturas_Seleccionadas = new DataTable();
        Dt_Facturas_Seleccionadas = ((DataTable)Session["Dt_Factura_Seleccionada"]);

        Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Facutra = new Cls_Ope_Cor_Facturacion_Global_Negocio();
        foreach (DataRow Dr_Factura_Seleccionada in Dt_Facturas_Seleccionadas.Rows)
        {
            Rs_Facutra.P_No_Factura = Dr_Factura_Seleccionada[Ope_Facturas.Campo_No_Factura].ToString();
            Rs_Facutra.P_Serie = Dr_Factura_Seleccionada[Ope_Facturas.Campo_Serie].ToString();
            Dt_Factura = Rs_Facutra.Consultar_Factura();

            if (Dt_Factura.Rows.Count > 0)
            {
                foreach (DataRow Dr in Dt_Detalles.Rows)
                {
                    Dr[Ope_Notas_Credito_Detalle.Campo_No_Factura] = Dt_Factura.Rows[0][Ope_Facturas.Campo_No_Factura].ToString(); // Guarda el número de factura
                    Dr[Ope_Notas_Credito_Detalle.Campo_Serie_Factura] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Serie].ToString(); // Guarda la serie de la factura
                    //Dr[Ope_Notas_Credito_Detalle.Campo_Codigo] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Tipo_Moneda].ToString(); // Guarda la moneda de la factura
                }
            }
        }
        Session["Dt_Factura_Seleccionada"] = Dt_Facturas_Seleccionadas;
        Rellena_Grid();

        return Dt_Detalles;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Hacer_Tabla_Factura_Seleccionada
    ///DESCRIPCIÓN: Crea la tabla de facturas seleccionadas.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Hacer_Tabla_Factura_Seleccionada()
    {
        DataTable Dt_Factura_Selecionada;
        Dt_Factura_Selecionada = new DataTable();
        Dt_Factura_Selecionada.Columns.Add(Ope_Facturas.Campo_Serie);
        Dt_Factura_Selecionada.Columns.Add(Ope_Facturas.Campo_No_Factura);
        Dt_Factura_Selecionada.Columns.Add(Ope_Facturas.Campo_Razon_Social);
        Dt_Factura_Selecionada.Columns.Add(Ope_Facturas.Campo_Tipo_Moneda);
        Dt_Factura_Selecionada.Columns.Add(Ope_Facturas.Campo_Total);
        Dt_Factura_Selecionada.Columns.Add(Ope_Facturas.Campo_Saldo);
        Dt_Factura_Selecionada.Columns.Add("IMPORTE");
        return Dt_Factura_Selecionada;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Hacer_Tabla_Partidas_Factura
    ///DESCRIPCIÓN: Crea la tabla de las partidas de la factura.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Hacer_Tabla_Partidas_Factura()
    {
        DataTable Dt_Partidas_Factura;
        Dt_Partidas_Factura = new DataTable();
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_No_Factura);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Serie_Factura);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Producto_Id);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Cantidad);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Codigo);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Cuenta_Predial);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Descripcion);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Unidad);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Porcentaje_Impuesto);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Precio_Unitario);
        Dt_Partidas_Factura.Columns.Add("COSTO");
        Dt_Partidas_Factura.Columns.Add("SUBTOTAL_DETALLE");
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Iva);
        Dt_Partidas_Factura.Columns.Add(Ope_Notas_Credito_Detalle.Campo_Total_Detalle);
        return Dt_Partidas_Factura;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crea_Tabla_Impuestos
    ///DESCRIPCIÓN: Crea una tabla con la estructura de impuestos.
    ///PARAMETROS:   
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  23/Mayo/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Hacer_Tabla_Impuestos()
    {
        DataTable Dt_Partida;
        Dt_Partida = new DataTable();
        Dt_Partida.Columns.Add("IMPUESTO");
        Dt_Partida.Columns.Add("TASA");
        Dt_Partida.Columns.Add("IMPORTE");
        return Dt_Partida;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN: Muestra el mensaje de error.
    ///PARAMETROS:  Mensaje, texto a mostrar
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  14/Marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Mensaje_Error(String Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Visible = true;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Text += Mensaje + "</br>";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mensaje_Error
    ///DESCRIPCIÓN: Limpia el mensaje de error
    ///PARAMETROS:  
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  14/Marzo/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Visible = false;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Formulario
    ///DESCRIPCIÓN: Establece en que estado esta el formulario, si esta disponible para guardar,
    ///             crear una nueva determinacion.
    ///PARAMETROS:  Estado, Estado en el que se cargara la configuración de los
    ///                     controles.
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  02/Febrero/2012
    ///MODIFICO:    Luis A. Salas
    ///FECHA_MODIFICO: 18/Julio/2013
    ///CAUSA_MODIFICACIÓN: Se adapto para el formulario.
    ///*******************************************************************************
    private void Estado_Formulario(String Estado)
    {
        switch (Estado)
        {
            case "Inicial":
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/Imagenes/paginas/icono_nuevo.png";
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ToolTip = "Salir";
                Btn_Salir.ImageUrl = "~/Imagenes/paginas/icono_salir.png";
                Btn_Cancelar_Nota.Visible = false;
                Btn_Exportar_PDF.Visible = false;
                Btn_Enviar_Nota_Credito.Visible = false;
                Btn_Ver_XML.Visible = false;
                Btn_Buscar.Enabled = true;
                Btn_Agregar_Factura.Style.Add("display", "none");
                //Configuracion_Acceso("Frm_Ope_Notas_Credito.aspx");
                Limpiar_Formulario();
                break;
            case "Nuevo":
                Btn_Nuevo.AlternateText = "Guardar";
                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Nuevo.ImageUrl = "~/Imagenes/paginas/icono_guardar.png";
                Btn_Salir.AlternateText = "Cancelar";
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/Imagenes/paginas/icono_cancelar.png";
                Btn_Cancelar_Nota.Visible = false;
                Btn_Exportar_PDF.Visible = false;
                Btn_Ver_XML.Visible = false;
                Limpiar_Formulario();
                Div_Descuento_Pago.Style.Add("display", "block");
                Div_Devolucion.Style.Add("display", "none");
                break;
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    ///DESCRIPCIÓN: Limpia los controles del formulario
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Formulario()
    {
        Txt_No_Nota.Text = "";
        Txt_Tipo_Cambio.Text = "";
        Txt_Rfc.Text = "";
        Txt_Calle.Text = "";
        Txt_Num_Ext.Text = "";
        Txt_Num_Int.Text = "";
        Txt_Colonia.Text = "";
        Txt_Ciudad.Text = "";
        Txt_Localidad.Text = "";
        Txt_Codigo_Postal.Text = "";
        Txt_Estado.Text = "";
        Txt_Pais.Text = "";
        Txt_Fecha_Emision.Text = "";
        Txt_Cliente.Text = "";
        Chk_Por_Fecha_Emision.Checked = false;
        Chk_Por_Fecha_Vencimiento.Checked = false;
        Div_Devolucion.Style.Add("display", "none");
        Div_Descuento_Pago.Style.Add("display", "none");
        Lbl_Iva.Text = "0.00";
        Txt_Descuento_Facturas.Text = "";
        Txt_Fecha_Emision_Inicial.Text = "";
        Txt_Fecha_Emision_Final.Text = "";
        Txt_Fecha_Vencimiento_Inicial.Text = "";
        Txt_Fecha_Vencimiento_Final.Text = "";
        Txt_Busqueda_Cliente.Text = "";
        Txt_Busqueda_Factura.Text = "";
        Txt_Buscar_No_Factura.Text = "";
        Txt_Subtotal.Text = "";
        Txt_Iva.Text = "";
        Txt_Total.Text = "";
        Txt_Comentarios.Text = "";
        Hfd_Cliente_Id.Value = String.Empty;
        Hfd_Correo_Cliente.Value = String.Empty;
        Hdf_Comentarios_Cancelacion.Value = String.Empty;
        Cmb_Busqueda_Serie.SelectedIndex = -1;
        Cmb_Tipo_Moneda.SelectedIndex = -1;
        Cmb_Estatus.SelectedIndex = -1;
        Cmb_Serie.SelectedIndex = -1;
        Cmb_Tipo_Nota.SelectedIndex = -1;
        Grid_Facturas.DataSource = new DataTable();
        Grid_Facturas.DataBind();
        Grid_Facturas_Seleccionadas.DataSource = new DataTable();
        Grid_Facturas_Seleccionadas.DataBind();
        Grid_Partidas_Factura.DataSource = new DataTable();
        Grid_Partidas_Factura.DataBind();
        Grid_Partidas_Devolver.DataSource = new DataTable();
        Grid_Partidas_Devolver.DataBind();
        Mensaje_Error();
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Establece la configuración del formulario
    ///PARAMETROS:  Habilitado, Especifica si estara habilitado o no el componente
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Configuracion_Formulario(Boolean Habilitado)
    {
        Cmb_Serie.Enabled = !Habilitado;
        Txt_No_Nota.Enabled = !Habilitado;
        Txt_Comentarios.Enabled = !Habilitado;
        Cmb_Tipo_Nota.Enabled = !Habilitado;
        Cmb_Tipo_Moneda.Enabled = !Habilitado;
        Pnl_Busqueda_Facturas.Enabled = !Habilitado;
        Txt_Busqueda_Factura.Enabled = !Habilitado;
        Btn_Buscar_Cliente.Enabled = !Habilitado;
        Pnl_Busqueda_Partida.Enabled = !Habilitado;
        Txt_Busca_No_Recibo.Enabled = !Habilitado;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Series
    ///DESCRIPCIÓN: Metodo usado para cargar las series.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Combo_Series()
    {
        /*
         * MODIFICAR PARA QUE SOLO CARGUE LAS SERIES QUE 
         * CORRESPONDEN A NOTAS DE CREDITO.
         */
        DataTable Dt_Series;
        try
        {
            Mensaje_Error();
            Cls_Ope_Facturas_Series_Negocio Rs_Series = new Cls_Ope_Facturas_Series_Negocio();
            Cmb_Serie.DataTextField = Cat_Facturas_Series.Campo_Serie;
            Cmb_Serie.DataValueField = Cat_Facturas_Series.Campo_Serie_Id;
            Cmb_Busqueda_Serie_Nota_Credito.DataTextField = Cat_Facturas_Series.Campo_Serie;
            Cmb_Busqueda_Serie_Nota_Credito.DataValueField = Cat_Facturas_Series.Campo_Serie_Id;

            Rs_Series.P_Tipo_Serie = "NOTA CREDITO";
            Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
            Cmb_Serie.DataSource = Dt_Series;
            Cmb_Serie.DataBind();
            Cmb_Serie.Items.Insert(0, new ListItem("<SELECCIONE>", "0"));
            Cmb_Busqueda_Serie_Nota_Credito.DataSource = Dt_Series;
            Cmb_Busqueda_Serie_Nota_Credito.DataBind();
            Cmb_Busqueda_Serie_Nota_Credito.Items.Insert(0, new ListItem("<SELECCIONE>", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Serie: " + ex.Message.ToString(), ex);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Series_Facturas
    ///DESCRIPCIÓN: Metodo usado para cargar las series de la factura.
    ///PARAMETROS: 
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  14/Agosto/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Combo_Series_Facturas()
    {
        DataTable Dt_Series;
        try
        {
            Mensaje_Error();
            Cls_Ope_Facturas_Series_Negocio Rs_Series = new Cls_Ope_Facturas_Series_Negocio();
            Cmb_Busqueda_Serie.DataTextField = Cat_Facturas_Series.Campo_Serie;
            Cmb_Busqueda_Serie.DataValueField = Cat_Facturas_Series.Campo_Serie_Id;

            Rs_Series.P_Tipo_Serie = "FACTURA";
            Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
            Cmb_Busqueda_Serie.DataSource = Dt_Series;
            Cmb_Busqueda_Serie.DataBind();
            Cmb_Busqueda_Serie.Items.Insert(0, new ListItem("<SELECCIONE>", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Serie: " + ex.Message.ToString(), ex);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consulta_Maximo_Serie
    ///DESCRIPCIÓN: Consulta el consecutivo de la serie.
    ///PARAMETROS:  Tipo_Serie, Cadena que contiene el tipo de serie a consultar.
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  22/Abril/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consulta_Maximo_Serie(String Tipo_Serie)
    {
        String Str_Consecutivo = "0";
        String Serie_Id = "0";
        String Serie = String.Empty;
        string Estatus = "";
        Int16 Facturas_Canceladas = 0;
        Int16 Timbre = 0;
        Int16 Timbre_Inicial = 0;
        Int16 Timbre_Final = 0;
        Int32 Timbre_Usados = 0;
        DataTable Dt_Facturas = new DataTable();
        DataTable Dt_Series = new DataTable();
        DataTable Dt_Ultima_Nota = new DataTable();

        Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Facturas = new Cls_Ope_Cor_Facturacion_Global_Negocio();
        Cls_Ope_Facturas_Series_Negocio Rs_Series = new Cls_Ope_Facturas_Series_Negocio();
        Cls_Ope_Notas_Credito_Negocio Rs_Notas_credito = new Cls_Ope_Notas_Credito_Negocio();

        // Busca si hay series del tipo de serie y por default
        Rs_Series.P_Tipo_Serie = Tipo_Serie;
        Rs_Series.P_Tipo_Default = "SI";
        Rs_Series.P_Estatus = "ACTIVO";
        Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
        if (Dt_Series != null && Dt_Series.Rows.Count > 0)
        {
            Serie_Id = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie_Id].ToString().Trim();
            Serie = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie].ToString().Trim();

            Dt_Series = Rs_Series.Consultar_Serie();

            Rs_Notas_credito.P_Serie = Serie;
            Dt_Ultima_Nota = Rs_Notas_credito.Consultar_Ultima_Nota();
            Timbre = Int16.Parse(Dt_Ultima_Nota.Rows[0]["No_Nota_Credito"].ToString());

            Timbre_Inicial = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Inicial].ToString());
            Timbre_Final = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Final].ToString());
            Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());
            Estatus = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Estatus].ToString();

            Txt_Total_Timbres.Text = Dt_Series.Rows[0]["Total_Timbres"].ToString();
            Timbre_Usados = (Timbre - Timbre_Inicial) + Facturas_Canceladas;
            Txt_Timbres_Restantes.Text = (Int32.Parse(Txt_Total_Timbres.Text) - Timbre_Usados).ToString();
        }
        else
        {
            // En caso de que el tipo de serie no tenga un default, se consulta la serie
            // que no este por default, pero que sea de ese tipo de serie.
            Rs_Series.P_Tipo_Default = "NO";
            Rs_Series.P_Estatus = "ACTIVO";
            Rs_Series.P_Tipo_Serie = Tipo_Serie;
            Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
            if (Dt_Series != null && Dt_Series.Rows.Count > 0)
            {
                Serie_Id = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie_Id].ToString().Trim();
                Serie = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie].ToString().Trim();

                Dt_Series = Rs_Series.Consultar_Serie();

                Rs_Notas_credito.P_Serie = Serie;
                Dt_Ultima_Nota = Rs_Notas_credito.Consultar_Ultima_Nota();
                try
                {
                    Timbre = Int16.Parse(Dt_Ultima_Nota.Rows[0]["No_Nota_Credito"].ToString());
                }
                catch (Exception ex)
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append("<script type = 'text/javascript'>");
                    sb.Append(ex.ToString());
                    // System.Windows.Forms.MessageBox.Show(ex.ToString());
                }

                Timbre_Inicial = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Inicial].ToString());
                Timbre_Final = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Final].ToString());
                Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());
                Estatus = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Estatus].ToString();

                Txt_Total_Timbres.Text = Dt_Series.Rows[0]["Total_Timbres"].ToString();
                Timbre_Usados = (Timbre - Timbre_Inicial) + Facturas_Canceladas;
                Txt_Timbres_Restantes.Text = (Int32.Parse(Txt_Total_Timbres.Text) - Timbre_Usados).ToString();
            }
            else
            {
                // Busca cualquier otra serie que este activa o pendiente.
                Rs_Series.P_Tipo_Serie = String.Empty;
                Dt_Series = Rs_Series.Consultar_Serie_Activa_Pendiente();
                if (Dt_Series != null && Dt_Series.Rows.Count > 0)
                {
                    Serie_Id = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie_Id].ToString().Trim();
                    Serie = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie].ToString().Trim();

                    Dt_Series = Rs_Series.Consultar_Serie();

                    Rs_Notas_credito.P_Serie = Serie;
                    Dt_Ultima_Nota = Rs_Notas_credito.Consultar_Ultima_Nota();
                    Timbre = Int16.Parse(Dt_Ultima_Nota.Rows[0]["No_Nota_Credito"].ToString());

                    Timbre_Inicial = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Inicial].ToString());
                    Timbre_Final = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Final].ToString());
                    Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());
                    Estatus = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Estatus].ToString();

                    Txt_Total_Timbres.Text = Dt_Series.Rows[0]["Total_Timbres"].ToString();
                    Timbre_Usados = (Timbre - Timbre_Inicial) + Facturas_Canceladas;
                    Txt_Timbres_Restantes.Text = (Int32.Parse(Txt_Total_Timbres.Text) - Timbre_Usados).ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('No hay series activas o pendientes!!!');", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','No hay series activas o pendientes!!!', 'info');", true);
                }
            }
        }
        if (!Serie_Id.Equals("0"))
        {
            Cmb_Serie.SelectedIndex = Cmb_Serie.Items.IndexOf(Cmb_Serie.Items.FindByValue(HttpUtility.HtmlDecode(Serie_Id)));
            Cmb_Serie.Enabled = true;
        }
        else
        {
            Cmb_Serie.SelectedIndex = -1;
            Cmb_Serie.Enabled = false;

        }
        if (!String.IsNullOrEmpty(Serie.Trim()))
        {
            Str_Consecutivo = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Ope_Notas_Credito.Tabla_Ope_Notas_Credito, Ope_Notas_Credito.Campo_No_Nota_Credito, Ope_Notas_Credito.Campo_Serie + "= '" + Serie + "'", 10);
            Txt_No_Nota.Text = Str_Consecutivo;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Clientes
    ///DESCRIPCIÓN: Llena el grid de los clientes.
    ///PARAMETROS:     
    ///             Pagina, Pagina en la cual se mostrará el Grid_VIew
    ///CREO:        Luis Alberto Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Factura_Detalles(int Pagina)
    {
        try
        {

            Grid_Partidas_Factura.DataSource = (DataTable)Session["Dt_Partidas_Factura"];
            Grid_Partidas_Factura.PageIndex = Pagina;
            Grid_Partidas_Factura.DataBind();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Clientes
    ///DESCRIPCIÓN: Llena el grid de los clientes.
    ///PARAMETROS:     
    ///             Pagina, Pagina en la cual se mostrará el Grid_VIew
    ///CREO:        Luis Alberto Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    ///
    public static String No_Factura = String.Empty;
    private void Llenar_Grid_Clientes(int Pagina)
    {
        DataTable Dt_Clientes = new DataTable();
        try
        {
            Cls_Ope_Notas_Credito_Negocio Rs_Notas_Credito = new Cls_Ope_Notas_Credito_Negocio();
            Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Clientes = new Cls_Ope_Cor_Facturacion_Global_Negocio();
            //if (Txt_Busqueda_Cliente.Text.Trim() != "")
            //{
            //    Rs_Notas_Credito.P_Razon_Social = Txt_Busqueda_Cliente.Text.ToUpper();
            //}
            //Dt_Clientes = Rs_Notas_Credito.Consultar_Clientes_Facturas();
            //Dt_Clientes.Clear();
            if (Dt_Clientes.Rows.Count <= 0)
            {
                if (Txt_Busqueda_Cliente.Text.Trim() != "")
                {
                    Rs_Clientes.P_Razon_Social_Facturacion = Txt_Busqueda_Cliente.Text.ToUpper();
                }
                if (Txt_No_Factura_Busqueda.Text.Trim() != "")
                {
                    Rs_Clientes.P_No_Factura = Convert.ToInt32(Txt_No_Factura_Busqueda.Text).ToString("D10");
                    No_Factura = Rs_Clientes.P_No_Factura;
                }
                Rs_Clientes.P_Cancelada = "NO";
                Dt_Clientes = Rs_Clientes.Consultar_Factura();
                Dt_Clientes.Columns.Add("NOMBRE_CORTO", typeof(String));
                if (Dt_Clientes.Rows.Count > 0)
                {
                    Dt_Clientes.Rows[0]["NOMBRE_CORTO"] = Dt_Clientes.Rows[0]["RAZON_SOCIAL"].ToString();
                }
            }


            Grid_Clientes.DataSource = Dt_Clientes;
            Grid_Clientes.PageIndex = Pagina;
            Grid_Clientes.DataBind();
            Pnl_Clientes.Visible = true;
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Facturas
    ///DESCRIPCIÓN: Llena el grid de las facturas.
    ///PARAMETROS:     
    ///             Pagina, Pagina en la cual se mostrará el Grid_VIew
    ///CREO:        Luis Alberto Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Facturas(int Pagina)
    {
        DataTable Dt_Facturas = new DataTable();
        Boolean Continua = true;
        try
        {
            Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Facturas = new Cls_Ope_Cor_Facturacion_Global_Negocio();
            if (Txt_Busqueda_Factura.Text != "")
            {
                //if (Txt_Busqueda_Factura.Text.Trim() != "")
                //{
                    Rs_Facturas.P_Razon_Social_Facturacion = Txt_Busqueda_Factura.Text.ToUpper();
                //}
                    Rs_Facturas.P_Cliente_Id = Hfd_Cliente_Id.Value.Trim();
                if (Cmb_Tipo_Nota.Text == "DEVOLUCION")
                {
                    Rs_Facturas.P_Cancelada = "NO";
                    Rs_Facturas.P_Pagada = "NO";
                }
                else
                {
                    Rs_Facturas.P_Cancelada = "NO";
                    Rs_Facturas.P_Pagada = "NO";
                }
                if (Cmb_Busqueda_Serie.SelectedIndex > 0)
                {
                    Rs_Facturas.P_Serie = Cmb_Busqueda_Serie.Items[Cmb_Busqueda_Serie.SelectedIndex].ToString();
                }
                if (Txt_No_Factura_Busqueda.Text != "")
                {
                    Rs_Facturas.P_No_Factura = String.Format("{0:0000000000}", Convert.ToInt32(Txt_No_Factura_Busqueda.Text.Trim()));
                    //Datos.P_No_Proyecto = Format(CInt(Consecutivo_Proyecto) + 1, "0000000000")
                }
                if (Chk_Por_Fecha_Emision.Checked)
                {
                    if (Txt_Fecha_Emision_Inicial.Text.Trim() != "" && Txt_Fecha_Emision_Final.Text.Trim() != "")
                    {
                        Rs_Facturas.P_Fecha_Emision_Inicial = Txt_Fecha_Emision_Inicial.Text.ToUpper();
                        Rs_Facturas.P_Fecha_Emision_Final = Txt_Fecha_Emision_Final.Text.ToUpper();
                        Btn_Txt_Fecha_Emision_Inicial.Enabled = true;
                        Btn_Txt_Fecha_Emision_Final.Enabled = true;
                    }
                    else
                    {
                        if (Txt_Fecha_Emision_Inicial.Text.Trim() == "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de emisi&oacute; inicial');", true);
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Facturaci&oacute;n Electr&oacute;nica", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de emisi&oacute; inicial', 'warning');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de emisi&oacute; final');", true);
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Facturaci&oacute;n Electr&oacute;nica", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de emisi&oacute; final', 'warning');", true);
                        }
                        Continua = false;
                    }
                }
                if (Chk_Por_Fecha_Vencimiento.Checked)
                {
                    if (Txt_Fecha_Vencimiento_Inicial.Text.Trim() != "" && Txt_Fecha_Vencimiento_Final.Text.Trim() != "")
                    {
                        Rs_Facturas.P_Fecha_Vencimiento_Inicial = Txt_Fecha_Vencimiento_Inicial.Text.ToUpper();
                        Rs_Facturas.P_Fecha_Vencimiento_Final = Txt_Fecha_Vencimiento_Final.Text.ToUpper();
                        Btn_Txt_Fecha_Vencimiento_Inicial.Enabled = true;
                        Btn_Txt_Fecha_Vencimiento_Final.Enabled = true;
                    }
                    else
                    {
                        if (Txt_Fecha_Vencimiento_Inicial.Text.Trim() == "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de vencimiento inicial');", true);
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Facturaci&oacute;n Electr&oacute;nica", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de vencimiento inicial', 'warning');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de vencimiento final');", true);
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Facturaci&oacute;n Electr&oacute;nica", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de vencimiento final', 'warning');", true);
                        }

                        Continua = false;
                    }
                }
                if (Continua)
                {
                    Dt_Facturas = Rs_Facturas.Consultar_Factura_Productos();

                    // Elimina las facturas que ya esten seleccionadas.
                    int Indice = 0;
                    Boolean Existe = false;
                    DataTable Dt_Factura_Selecionada = ((DataTable)Session["Dt_Factura_Seleccionada"]);
                    for (; ; )
                    {
                        if (Indice < Dt_Facturas.Rows.Count)
                        {
                            DataRow Factura = Dt_Facturas.Rows[Indice];
                            Existe = false;
                            foreach (DataRow Factura_Seleccionada in Dt_Factura_Selecionada.Rows)
                            {
                                if (Factura[Ope_Facturas.Campo_No_Factura].ToString().Equals(Factura_Seleccionada[Ope_Facturas.Campo_No_Factura].ToString())
                                    && Factura[Ope_Facturas.Campo_Serie].ToString().Equals(Factura_Seleccionada[Ope_Facturas.Campo_Serie].ToString()))
                                {
                                    Existe = true;
                                    break;
                                }
                            }
                            if (Existe)
                            {
                                Dt_Facturas.Rows.RemoveAt(Indice);
                            }
                            else
                            {
                                Indice++;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (Dt_Facturas.Rows.Count > 0)
                    {
                        if (!Cmb_Tipo_Nota.SelectedValue.ToString().Equals("DEVOLUCION"))
                        {
                            Btn_Agregar_Factura.Style.Add("display", "block");
                        }
                        else
                        {
                            Btn_Agregar_Factura.Style.Add("display", "none");
                        }
                    }
                    else
                    {
                        Btn_Agregar_Factura.Style.Add("display", "none");
                    }
                    if (Convert.ToDouble(Dt_Facturas.Rows[0]["Saldo"].ToString()) > 0)
                    {
                        Grid_Facturas.Columns[9].Visible = true;
                        Grid_Facturas.DataSource = Dt_Facturas;
                        Grid_Facturas.PageIndex = Pagina;
                        Grid_Facturas.DataBind();
                        Grid_Facturas.Columns[9].Visible = false;
                        Session["Dt_Facturas_Buscadas"] = Dt_Facturas;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Esta Factura ya no puede ser abonada, pues su saldo es menor o igual a cero.');", true);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Seleccione un cliente');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Facturaci&oacute;n Electr&oacute;nica", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Seleccione un cliente', 'warning');", true);
                Cmb_Busqueda_Serie.SelectedIndex = -1;
                Txt_Buscar_No_Factura.Text = "";
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Facturas
    ///DESCRIPCIÓN: Llena el grid de las facturas.
    ///PARAMETROS:     
    ///             1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
    ///CREO:        Luis Alberto Salas
    ///FECHA_CREO:  11/Abril/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Notas_Credito(int Pagina)
    {
        DataTable Dt_Notas_Credito = new DataTable();
        Boolean Continua = true;
        try
        {
            Cls_Ope_Notas_Credito_Negocio Rs_Notas_Credito = new Cls_Ope_Notas_Credito_Negocio();
            if (Txt_Busqueda_Nota_Credito.Text.Trim() != "")
            {
                Rs_Notas_Credito.P_Razon_Social = Txt_Busqueda_Nota_Credito.Text.ToUpper();
            }
            if (Cmb_Busqueda_Estatus_Nota_Credito.SelectedIndex > 0)
            {
                Rs_Notas_Credito.P_Estatus = Cmb_Busqueda_Estatus_Nota_Credito.SelectedValue.ToString();
            }
            if (Cmb_Busqueda_Serie_Nota_Credito.SelectedIndex > 0)
            {
                Rs_Notas_Credito.P_Serie = Cmb_Busqueda_Serie_Nota_Credito.Items[Cmb_Busqueda_Serie_Nota_Credito.SelectedIndex].ToString();
            }
            if (Chk_Por_Fecha_Emision_Nota_Credito.Checked)
            {
                if (Txt_Fecha_Emision_Inicial_Nota_Credito.Text.Trim() != "" && Txt_Fecha_Emision_Final_Nota_Credito.Text.Trim() != "")
                {
                    Rs_Notas_Credito.P_Fecha_Emision_Inicial = Txt_Fecha_Emision_Inicial_Nota_Credito.Text.ToUpper();
                    Rs_Notas_Credito.P_Fecha_Emision_Final = Txt_Fecha_Emision_Final_Nota_Credito.Text.ToUpper();
                    Btn_Txt_Fecha_Emision_Inicial_Nota_Credito.Enabled = true;
                    Btn_Txt_Fecha_Emision_Final_Nota_Credito.Enabled = true;
                }
                else
                {
                    if (Txt_Fecha_Emision_Inicial_Nota_Credito.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de emisi&oacute; inicial');", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Crédito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de emisi&oacute; inicial', 'warning');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de emisi&oacute; final');", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Crédito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de emisi&oacute; final', 'warning');", true);
                    }
                    Continua = false;
                }
            }
            if (Chk_Por_Fecha_Vencimiento_Nota_Credito.Checked)
            {
                if (Txt_Fecha_Vencimiento_Inicial_Nota_Credito.Text.Trim() != "" && Txt_Fecha_Vencimiento_Final_Nota_Credito.Text.Trim() != "")
                {
                    Rs_Notas_Credito.P_Fecha_Vencimiento_Inicial = Txt_Fecha_Vencimiento_Inicial_Nota_Credito.Text.ToUpper();
                    Rs_Notas_Credito.P_Fecha_Vencimiento_Final = Txt_Fecha_Vencimiento_Final_Nota_Credito.Text.ToUpper();
                    Btn_Txt_Fecha_Vencimiento_Inicial_Nota_Credito.Enabled = true;
                    Btn_Txt_Fecha_Vencimiento_Final_Nota_Credito.Enabled = true;
                }
                else
                {
                    if (Txt_Fecha_Vencimiento_Inicial_Nota_Credito.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de vencimiento inicial');", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Crédito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de vencimiento inicial', 'warning');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Falta la fecha de vencimiento final');", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Crédito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Falta la fecha de vencimiento final', 'warning');", true);
                    }
                    Continua = false;
                }
            }
            if (Continua)
            {
                Dt_Notas_Credito = Rs_Notas_Credito.Consultar_Nota_Credito();
                Grid_Notas_Credito.Columns[1].Visible = true;
                Grid_Notas_Credito.DataSource = Dt_Notas_Credito;
                Grid_Notas_Credito.PageIndex = Pagina;
                Grid_Notas_Credito.DataBind();
                Grid_Notas_Credito.Columns[1].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Actualizar_Facturas_Seleccionadas
    ///DESCRIPCIÓN: Actualiza el datatable de la sesion que contiene las facturas
    ///             seleccionadas.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Actualizar_Facturas_Seleccionadas()
    {
        if (Grid_Facturas_Seleccionadas.Rows.Count > 0)
        {
            DataTable Dt_Auxiliar = Hacer_Tabla_Factura_Seleccionada();
            DataTable Dt_Factura_Selecionada = ((DataTable)Session["Dt_Factura_Seleccionada"]);
            DataRow Dr;
            foreach (GridViewRow Fila in Grid_Facturas_Seleccionadas.Rows)
            {
                Dr = Dt_Auxiliar.NewRow();
                Dr[Ope_Facturas.Campo_Serie] = Dt_Factura_Selecionada.Rows[Fila.RowIndex][Ope_Facturas.Campo_Serie].ToString();
                Dr[Ope_Facturas.Campo_No_Factura] = Dt_Factura_Selecionada.Rows[Fila.RowIndex][Ope_Facturas.Campo_No_Factura].ToString();
                Dr[Ope_Facturas.Campo_Razon_Social] = Dt_Factura_Selecionada.Rows[Fila.RowIndex][Ope_Facturas.Campo_Razon_Social].ToString();
                Dr[Ope_Facturas.Campo_Tipo_Moneda] = Dt_Factura_Selecionada.Rows[Fila.RowIndex][Ope_Facturas.Campo_Tipo_Moneda].ToString();
                Dr[Ope_Facturas.Campo_Total] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura_Selecionada.Rows[Fila.RowIndex][Ope_Facturas.Campo_Total].ToString()));
                Dr[Ope_Facturas.Campo_Saldo] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura_Selecionada.Rows[Fila.RowIndex][Ope_Facturas.Campo_Saldo].ToString()));
                Dr["IMPORTE"] = ((TextBox)Fila.FindControl("Txt_Importe_Factura")).Text;
                Dt_Auxiliar.Rows.Add(Dr);
            }
            Session["Dt_Factura_Seleccionada"] = Dt_Auxiliar;
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Rellena_Grid
    ///DESCRIPCIÓN: Rellena el grid de las partidas con el datatable de las facturas
    ///             selesccionadas se encuentra en la sesión.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Rellena_Grid()
    {
        DataTable Dt_Auxiliar = (DataTable)Session["Dt_Factura_Seleccionada"];
        Grid_Facturas_Seleccionadas.DataSource = Dt_Auxiliar;
        Grid_Facturas_Seleccionadas.DataBind();

        //GridViewRow Fila_Grid;
        //int Cont_Filas = 0;
        //Limpia_Totales();
        //foreach (DataRow Fila_Tabla in Dt_Auxiliar.Rows)
        //{
        //    Fila_Grid = Grid_Facturas_Seleccionadas.Rows[Cont_Filas];
        //    ((TextBox)Fila_Grid.FindControl("Txt_Importe_Factura")).Text = String.Format("{0:#,###0.#0}", Convert.ToDouble(Fila_Tabla["TOTAL"]));
        //    Calcular_Totales(Double.Parse(Fila_Tabla["TOTAL"].ToString()));
        //    Cont_Filas++;
        //}
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Limpia_Totales
    ///DESCRIPCIÓN: Limpia los campos que contiene los totales.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpia_Totales()
    {
        Txt_Subtotal.Text = String.Format("{0:#,###0.#0}", 0);
        Txt_Iva.Text = String.Format("{0:#,###0.#0}", 0);
        Txt_Total.Text = String.Format("{0:#,###0.#0}", 0);
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Calcular_Totales
    ///DESCRIPCIÓN: Calcula los totales de las facturas seleccionadas.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Calcular_Totales(Double Importe)
    {
        Double Subtotal = 0;
        Double Iva = 0;
        Double Total = 0;
        Double Iva_Sistema = 0;

        if (!String.IsNullOrEmpty(Lbl_Iva.Text))
        {
            Iva_Sistema = Double.Parse(Lbl_Iva.Text) / 100;
        }
        if (!String.IsNullOrEmpty(Txt_Total.Text))
        {
            Total = Double.Parse(Txt_Total.Text);
        }

        Total += Importe;
        Subtotal = Total / (Iva_Sistema + 1);
        Iva = Total - Subtotal;
        Txt_Subtotal.Text = String.Format("{0:#,###0.#0}", Subtotal);
        Txt_Iva.Text = String.Format("{0:#,###0.#0}", Iva);
        Txt_Total.Text = String.Format("{0:#,###0.#0}", Total);
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Calcular_Totales_Partidas
    ///DESCRIPCIÓN: Calcula los totales de las partidas a devolver.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Calcular_Totales_Partidas(Double Importe, Double Impuesto)
    {
        Double Subtotal = 0;
        Double Iva = 0;
        Double Total = 0;
        Double Iva_Sistema = 0;

        //if (Impuesto != 0)
        //{
        //    Iva_Sistema = Impuesto / 100;
        //}
        if (!String.IsNullOrEmpty(Txt_Subtotal.Text))
        {
            Subtotal = Double.Parse(Txt_Subtotal.Text);
        }

        //Subtotal += Importe;
        //Iva = Subtotal * Iva_Sistema;
        Total = Subtotal + Impuesto;
        Txt_Subtotal.Text = String.Format("{0:#,###0.#0}", Subtotal);
        //Txt_Iva.Text = String.Format("{0:#,###0.#0}", Iva);
        Txt_Total.Text = String.Format("{0:#,###0.#0}", Total);
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Consulta_Datos_Cliente
    ///DESCRIPCIÓN: Consulta los datos del cliente y rellena los campos.
    ///PARAMETROS: 
    ///             Cliente_Id, puede contener el id del cliente o la razon social.
    ///             Is_Id, si es el id que se le manda, se coloca en true, si es la
    ///                 razon social, se coloca en false.
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consulta_Datos_Cliente(String Cliente_Id, Boolean Is_Id)
    {
        try
        {
            if (!String.IsNullOrEmpty(Cliente_Id))
            {
                DataTable Dt_Clientes = new DataTable();
                DataTable Dt_Estado = new DataTable();
                DataTable Dt_Cliente_Detalle = new DataTable();
                String Estado_Id = String.Empty;

                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Clientes = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                if (Is_Id)
                {
                    Rs_Clientes.P_Cliente_Id = Cliente_Id;
                    Dt_Clientes = Rs_Clientes.Consultar_Clientes();
                }
                else
                {
                    Rs_Clientes.P_Razon_Social_Facturacion = Cliente_Id;
                    Rs_Clientes.P_Cancelada = "NO";
                    Dt_Clientes = Rs_Clientes.Consultar_Factura();
                    Dt_Clientes.Columns.Add("NOMBRE_CORTO", typeof(String));
                    Dt_Clientes.Rows[0]["NOMBRE_CORTO"] = Dt_Clientes.Rows[0]["RAZON_SOCIAL"].ToString();

                }

                Hfd_Cliente_Id.Value = Cliente_Id;
                Hfd_Correo_Cliente.Value = Dt_Clientes.Rows[0]["Email"].ToString();
                if (Dt_Clientes.Rows[0]["Razon_Social"].ToString() != "")
                {
                    Txt_Cliente.Text = Dt_Clientes.Rows[0]["Razon_Social"].ToString();
                }
                else
                {
                    Txt_Cliente.Text = Dt_Clientes.Rows[0]["Nombre_Corto"].ToString();
                }

                Txt_Rfc.Text = Dt_Clientes.Rows[0]["RFC"].ToString();
                Txt_Calle.Text = Dt_Clientes.Rows[0]["Calle"].ToString();
                Txt_Num_Int.Text = Dt_Clientes.Rows[0]["Numero_Interior"].ToString();
                Txt_Num_Ext.Text = Dt_Clientes.Rows[0]["Numero_Exterior"].ToString();
                Txt_Colonia.Text = Dt_Clientes.Rows[0]["Colonia"].ToString();
                Txt_Ciudad.Text = Dt_Clientes.Rows[0]["Ciudad"].ToString();
                Txt_Localidad.Text = Dt_Clientes.Rows[0]["Localidad"].ToString();
                Txt_Codigo_Postal.Text = Dt_Clientes.Rows[0]["CP"].ToString();
                Estado_Id = Dt_Clientes.Rows[0]["Estado"].ToString();

                //Consulta el nombre del estado correspondiente
                //if (Estado_Id.Length > 0)
                //{
                //    Cls_Cat_Cor_Estados_Negocio Rs_Estado = new Cls_Cat_Cor_Estados_Negocio();
                //    Rs_Estado.P_Nombre = Estado_Id;
                //    Dt_Estado = Rs_Estado.Buscar_Estado();
                //    Estado_Id = Dt_Estado.Rows[0][Cat_Cor_Estados.Campo_Nombre].ToString();
                //}
                Txt_Estado.Text = Estado_Id;
                Txt_Pais.Text = Dt_Clientes.Rows[0]["Pais"].ToString();
                if (Dt_Clientes.Rows[0]["Razon_Social"].ToString() != "")
                {
                    Txt_Busqueda_Factura.Text = Dt_Clientes.Rows[0]["Razon_Social"].ToString();
                }
                else
                {
                    Txt_Busqueda_Factura.Text = Dt_Clientes.Rows[0]["Nombre_Corto"].ToString();
                }
                Txt_Busqueda_Factura.Enabled = false;
            }
            //else
            //{
            //    Hfd_Cliente_Id.Value = String.Empty;
            //    Hfd_Correo_Cliente.Value = String.Empty;
            //    Txt_Cliente.Text = String.Empty;
            //    Txt_Rfc.Text = String.Empty;
            //    Txt_Calle.Text = String.Empty;
            //    Txt_Num_Int.Text = String.Empty;
            //    Txt_Num_Ext.Text = String.Empty;
            //    Txt_Colonia.Text = String.Empty;
            //    Txt_Ciudad.Text = String.Empty;
            //    Txt_Localidad.Text = String.Empty;
            //    Txt_Codigo_Postal.Text = String.Empty;
            //    Txt_Estado.Text = String.Empty;
            //    Txt_Pais.Text = String.Empty;
            //}
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Actualiza_Estatus_Serie
    ///DESCRIPCIÓN: Actualiza el estatus de la serie.
    ///PARAMETROS:  
    ///             Serie, Nombre de la serie a actualizar.
    ///             Consecutivo, Timbre actual.
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  22/Abril/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Actualiza_Estatus_Serie(String Serie, String Consecutivo, Boolean Cancelado)
    {
        int Timbre;
        int Timbre_Final;
        int Timbre_Inicial;
        int Timbre_Usados;
        int Facturas_Canceladas;
        String Estatus = String.Empty;
        DataTable Dt_Series = new DataTable();
        DataTable Dt_Ultima_Nota = new DataTable();

        Cls_Ope_Facturas_Series_Negocio Rs_Serie = new Cls_Ope_Facturas_Series_Negocio();
        Cls_Ope_Notas_Credito_Negocio Rs_Notas_credito = new Cls_Ope_Notas_Credito_Negocio();

        Rs_Serie.P_Estatus = "ACTIVO";
        Rs_Serie.P_Tipo_Serie = "NOTA CREDITO";
        Rs_Serie.P_Serie = Serie;
        Dt_Series = Rs_Serie.Consultar_Serie();

        Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());

        if (Cancelado == true)
        {
            Rs_Serie.P_Tipo_Serie = "NOTA CREDITO";
            Rs_Serie.P_Cancelaciones = Convert.ToString(Facturas_Canceladas + 1);
            Rs_Serie.P_Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString();
            Rs_Serie.Modificar_Serie();

            Rs_Notas_credito.P_Serie = Serie;
            Dt_Ultima_Nota = Rs_Notas_credito.Consultar_Ultima_Nota();
            Timbre = Int16.Parse(Dt_Ultima_Nota.Rows[0]["No_Nota_Credito"].ToString());
        }
        else
        {
            Timbre = Int16.Parse(Consecutivo);
        }

        Timbre_Final = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Final].ToString());
        Timbre_Inicial = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Inicial].ToString());
        Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());
        Estatus = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Estatus].ToString();
        Rs_Serie.P_Serie_Id = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie_Id].ToString();
        Rs_Serie.P_Serie = String.Empty;


        Timbre_Usados = Timbre + Facturas_Canceladas;

        if (Timbre_Usados >= Timbre_Final)
        {
            Rs_Serie.P_Tipo_Serie = "NOTA CREDITO";
            Rs_Serie.P_Estatus = "TERMINADO";
            Rs_Serie.Modificar_Serie();
        }
    }


    private void Actualiza_Estatus_Serie(String Serie, String Consecutivo, Boolean Cancelado, SqlTransaction Obj_Transaccion, SqlConnection Obj_Conexion)
    {
        int Timbre;
        int Timbre_Final;
        int Timbre_Inicial;
        int Timbre_Usados;
        int Facturas_Canceladas;
        String Estatus = String.Empty;
        DataTable Dt_Series = new DataTable();
        DataTable Dt_Ultima_Nota = new DataTable();

        Cls_Ope_Facturas_Series_Negocio Rs_Serie = new Cls_Ope_Facturas_Series_Negocio();
        Cls_Ope_Notas_Credito_Negocio Rs_Notas_credito = new Cls_Ope_Notas_Credito_Negocio();

        Rs_Serie.P_Estatus = "ACTIVO";
        Rs_Serie.P_Tipo_Serie = "NOTA CREDITO";
        Rs_Serie.P_Serie = Serie;
        Dt_Series = Rs_Serie.Consultar_Serie(Rs_Serie.P_Serie, Rs_Serie.P_Estatus, Obj_Transaccion, Obj_Conexion);

        Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());

        if (Cancelado == true)
        {
            Rs_Serie.P_Tipo_Serie = "NOTA CREDITO";
            Rs_Serie.P_Cancelaciones = Convert.ToString(Facturas_Canceladas + 1);
            Rs_Serie.P_Serie_Id = Dt_Series.Rows[0]["Serie_Id"].ToString();
            //Rs_Serie.Modificar_Serie();
            Rs_Serie.Actualizar_Cantidad_Cancelados(Rs_Serie.P_Serie_Id, Facturas_Canceladas + 1, Obj_Transaccion, Obj_Conexion);

            Rs_Notas_credito.P_Serie = Serie;
            Dt_Ultima_Nota = Rs_Notas_credito.Consultar_Ultima_Nota(Obj_Transaccion, Obj_Conexion);
            Timbre = Int16.Parse(Dt_Ultima_Nota.Rows[0]["No_Nota_Credito"].ToString());
        }
        else
        {
            Timbre = Int16.Parse(Consecutivo);
        }

        Timbre_Final = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Final].ToString());
        Timbre_Inicial = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Inicial].ToString());
        Facturas_Canceladas = Int16.Parse(Dt_Series.Rows[0]["Cancelaciones"].ToString());
        Estatus = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Estatus].ToString();
        Rs_Serie.P_Serie_Id = Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Serie_Id].ToString();
        Rs_Serie.P_Serie = String.Empty;


        Timbre_Usados = Timbre + Facturas_Canceladas;

        if (Timbre_Usados >= Timbre_Final)
        {
            Rs_Serie.P_Tipo_Serie = "NOTA CREDITO";
            Rs_Serie.P_Estatus = "TERMINADO";
            Rs_Serie.Modificar_Serie(Rs_Serie.P_Serie_Id, Rs_Serie.P_Estatus, Obj_Transaccion, Obj_Conexion);
        }
    }



    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Crea_Xml_CFDI
    ///DESCRIPCIÓN: Crea el archivo Xml de la factura.
    ///PARAMETROS:  Rs_Facturas, 
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  23/Mayo/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private XDocument Crea_Xml_CFDI(ref Cls_Ope_Notas_Credito_Negocio Rs_Nota_Credito, DataTable Dt_Factura_Detalles, DataTable Dt_Empresa, DataTable Dt_Impuestos, DataTable Dt_Parametros_Facturacion)
    {
        // Declaracion de Documento XML
        XDocument Xml_Documento = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));

        // Nodo donde se almacena la respuesta
        XmlDocument Xml_Respuesta = new XmlDocument();
        XmlNode Nodo_Respuesta;
        XElement Nodo_Timbrado;

        // Declaracion de los nodos del xml
        XElement Nodo_Raiz;
        XElement Nodo_Emisor;
        XElement Nodo_Emisor_Domicilio;
        XElement Nodo_Regimen_Fiscal;
        XElement Nodo_Receptor;
        XElement Nodo_Receptor_Domicilio;
        XElement Nodo_Conceptos;
        XElement Nodo_Cuenta_Predial;
        XElement Nodo_Concepto;
        XElement Nodo_Impuestos;
        XElement Nodo_Traslados;
        XElement Nodo_Complemento;

        // Variables del timbre
        String Xml = String.Empty;
        String Timbrado_VersionSat = String.Empty;
        String Timbrado_UUID = String.Empty;
        String Timbrado_FechaTimbrado = String.Empty;
        String Timbrado_selloCFD = String.Empty;
        String Timbrado_noCertificadoSAT = String.Empty;
        String Timbrado_selloSAT = String.Empty;
        String Timbrado_Ambiente = String.Empty;
        String Respuesta = String.Empty;
        String Codigo_Bidimensional = String.Empty;

        // Parametros de timbrado
        String Codigo_Usuario = String.Empty;
        String Codigo_Proveedor = String.Empty;
        String Sucursal_Id = String.Empty;
        String Version_Timbrado = String.Empty;

        // Variables para Impuestos
        Double Impuestos_Trasladados = 0;

        // Fecha Timbrado
        DateTime Fecha_Timbrado;

        // Declaracion de NameSpace
        XNamespace cfdi = "http://www.sat.gob.mx/cfd/3";
        XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
        XNamespace implocal = "http://www.sat.gob.mx/implocal";
        XNamespace tfd = "http://www.sat.gob.mx/TimbreFiscalDigital";

        //DataTable Dt_Parametros_Facturacion = new DataTable();
        try
        {
            DateTime Fecha = DateTime.Now.AddMinutes(-10);
            // Crea Nodo Raiz
            Nodo_Raiz = new XElement(cfdi + "Comprobante");
            /*if (Rs_Nota_Credito.P_Tipo_Factura.Equals("ARRENDAMIENTOS") || Rs_Nota_Credito.P_Tipo_Factura.Equals("HONORARIOS"))
            {
                Nodo_Raiz.SetAttributeValue(xsi + "schemaLocation", "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd");
                Nodo_Raiz.SetAttributeValue(XNamespace.Xmlns + "implocal", "http://www.sat.gob.mx/implocal");
            }
            else
            {
                Nodo_Raiz.SetAttributeValue(xsi + "schemaLocation", "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd");
            }*/
            Nodo_Raiz.SetAttributeValue(xsi + "schemaLocation", "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd");
            Nodo_Raiz.SetAttributeValue(XNamespace.Xmlns + "xsi", "http://www.w3.org/2001/XMLSchema-instance");
            Nodo_Raiz.SetAttributeValue(XNamespace.Xmlns + "cfdi", "http://www.sat.gob.mx/cfd/3");
            Nodo_Raiz.SetAttributeValue("version", "3.2");
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Serie))
            {
                Nodo_Raiz.SetAttributeValue("serie", Rs_Nota_Credito.P_Serie);
            }
            Nodo_Raiz.SetAttributeValue("folio", Convert.ToInt32(Rs_Nota_Credito.P_No_Nota_Credito).ToString());
            Nodo_Raiz.SetAttributeValue("fecha", String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Rs_Nota_Credito.P_Fecha_Xml)) + "T" + String.Format("{0:HH:mm:ss}", Convert.ToDateTime(Rs_Nota_Credito.P_Fecha_Xml)));
            Nodo_Raiz.SetAttributeValue("sello", Rs_Nota_Credito.P_Timbre_Sello_Cfd);
            Nodo_Raiz.SetAttributeValue("formaDePago", "PAGO EN UNA SOLA EXHIBICION"); // <---------------------------- AQUI
            Nodo_Raiz.SetAttributeValue("noCertificado", Rs_Nota_Credito.P_No_Certificado);
            Nodo_Raiz.SetAttributeValue("certificado", Rs_Nota_Credito.P_Certificado);
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Forma_Pago))
            {
                Nodo_Raiz.SetAttributeValue("condicionesDePago", Rs_Nota_Credito.P_Forma_Pago);
            }
            Nodo_Raiz.SetAttributeValue("subTotal", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Nota_Credito.P_Subtotal)));
            if (Rs_Nota_Credito.P_Tipo_Moneda.Equals("PESOS"))
            {
                Nodo_Raiz.SetAttributeValue("Moneda", Rs_Nota_Credito.P_Tipo_Moneda);
            }
            else
            {
                Nodo_Raiz.SetAttributeValue("TipoCambio", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Nota_Credito.P_Tipo_Cambio)));
                Nodo_Raiz.SetAttributeValue("Moneda", Rs_Nota_Credito.P_Tipo_Moneda);
            }
            Nodo_Raiz.SetAttributeValue("total", String.Format("{0:#0.00}", Convert.ToDouble(Rs_Nota_Credito.P_Total)));
            Nodo_Raiz.SetAttributeValue("tipoDeComprobante", Rs_Nota_Credito.P_Tipo_Comprobante.ToLower());
            Nodo_Raiz.SetAttributeValue("metodoDePago", Rs_Nota_Credito.P_Metodo_Pago);
            Nodo_Raiz.SetAttributeValue("LugarExpedicion", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Ciudad"].ToString()) + ", " + Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Estado"].ToString()));

            // Crea Nodo Emisor        
            if (Dt_Empresa != null)
            {
                Nodo_Emisor = new XElement(cfdi + "Emisor");
                //Nodo_Emisor.SetAttributeValue("rfc", Dt_Empresa.Rows[0][Cat_Empresas.Campo_RFC].ToString());
                Nodo_Emisor.SetAttributeValue("rfc", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["RFC"].ToString()));
                //Nodo_Emisor.SetAttributeValue("rfc", "AAA010101AAA");
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Razon_Social"].ToString()))
                {
                    Nodo_Emisor.SetAttributeValue("nombre", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Razon_Social"].ToString()));
                }
                // Crea nodo de domicilio fiscal
                Nodo_Emisor_Domicilio = new XElement(cfdi + "DomicilioFiscal");
                Nodo_Emisor_Domicilio.SetAttributeValue("calle", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Calle"].ToString()));
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("noExterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Numero_Exterior"].ToString()));
                }
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Numero_Interior"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("noInterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Interior"].ToString()));
                }
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Colonia"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("colonia", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Colonia"].ToString()));
                }
                if (!String.IsNullOrEmpty(Dt_Empresa.Rows[0]["Localidad"].ToString()))
                {
                    Nodo_Emisor_Domicilio.SetAttributeValue("localidad", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Localidad"].ToString()));
                }
                Nodo_Emisor_Domicilio.SetAttributeValue("municipio", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Ciudad"].ToString()));
                Nodo_Emisor_Domicilio.SetAttributeValue("estado", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Estado"].ToString()));
                Nodo_Emisor_Domicilio.SetAttributeValue("pais", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["Pais"].ToString()));
                Nodo_Emisor_Domicilio.SetAttributeValue("codigoPostal", Cls_Metodos_Generales.CFD_Elimina_Espacios(Dt_Empresa.Rows[0]["CP"].ToString()));

                Nodo_Emisor.Add(Nodo_Emisor_Domicilio);

                // Agrega salto de linea
                //Nodo_Emisor.InnerText = "\n";

                // Crea nodo expedido en por sucursal <------------- PREGUNTAR
                /*If Sucursal = True Then
                'Agrega los elementos al Emisor
                Set Nodo_ExpedidoEn = CFD_Documento.createElement("cfdi:ExpedidoEn")
                    'Agrega los atributos del domicilio al nodo Emisor
                    Nodo_ExpedidoEn.SetAttributeValue "calle", CFD_ExpedidoEn.Calle
                    If CFD_ExpedidoEn.No_Exterior <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "noExterior", CFD_ExpedidoEn.No_Exterior
                    End If
                    If CFD_ExpedidoEn.No_Interior <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "noInterior", CFD_ExpedidoEn.No_Interior
                    End If
                    If CFD_ExpedidoEn.Colonia <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "colonia", CFD_ExpedidoEn.Colonia
                    End If
                    If CFD_ExpedidoEn.Ciudad <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "municipio", CFD_ExpedidoEn.Ciudad
                    End If
                    If CFD_ExpedidoEn.Estado <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "estado", CFD_ExpedidoEn.Estado
                    End If
                    If CFD_ExpedidoEn.Pais <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "pais", CFD_ExpedidoEn.Pais
                    End If
                    If CFD_ExpedidoEn.CP <> "" Then
                        Nodo_ExpedidoEn.SetAttributeValue "codigoPostal", CFD_ExpedidoEn.CP
                    End If
                   'Asigna el nodo del domicilio al nodo emisor
                    Nodo_Emisor.appendChild Nodo_ExpedidoEn
                'Agrega un salto de linea
                Nodo_Emisor.appendChild CFD_Documento.createTextNode(vbCrLf)
            End If*/

                // Crea nodo de regimen fiscal
                Nodo_Regimen_Fiscal = new XElement(cfdi + "RegimenFiscal");
                Nodo_Regimen_Fiscal.SetAttributeValue("Regimen", Rs_Nota_Credito.P_Regimen_Fiscal);
                Nodo_Emisor.Add(Nodo_Regimen_Fiscal);

                Nodo_Raiz.Add(Nodo_Emisor);
            }

            // Crea nodo receptor
            Nodo_Receptor = new XElement(cfdi + "Receptor");
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Rfc))
            {
                Nodo_Receptor.SetAttributeValue("rfc", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Rfc));
            }
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Razon_Social))
            {
                Nodo_Receptor.SetAttributeValue("nombre", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Razon_Social));
            }

            // Crea nodo de domicilio del receptor
            Nodo_Receptor_Domicilio = new XElement(cfdi + "Domicilio");
            Nodo_Receptor_Domicilio.SetAttributeValue("calle", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Calle));
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Numero_Exterior))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("noExterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Numero_Exterior));
            }
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Numero_Interior))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("noInterior", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Numero_Interior));
            }
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Colonia))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("colonia", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Colonia));
            }
            if (!String.IsNullOrEmpty(Rs_Nota_Credito.P_Localidad))
            {
                Nodo_Receptor_Domicilio.SetAttributeValue("localidad", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Localidad));
            }
            Nodo_Receptor_Domicilio.SetAttributeValue("municipio", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Ciudad));
            Nodo_Receptor_Domicilio.SetAttributeValue("estado", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Estado));
            Nodo_Receptor_Domicilio.SetAttributeValue("pais", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Pais));
            Nodo_Receptor_Domicilio.SetAttributeValue("codigoPostal", Cls_Metodos_Generales.CFD_Elimina_Espacios(Rs_Nota_Credito.P_Cp));

            Nodo_Receptor.Add(Nodo_Receptor_Domicilio);
            Nodo_Raiz.Add(Nodo_Receptor);

            // Crea nodo Conceptos
            Nodo_Conceptos = new XElement(cfdi + "Conceptos");
            foreach (DataRow Fila_Tabla in Dt_Factura_Detalles.Rows)
            {
                // Valida que el concepto tenga cantidad y total
                if (Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Cantidad].ToString()) > 0)

                //if (Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Cantidad].ToString()) > 0 &&
                //Convert.ToDouble(Fila_Tabla["SUBTOTAL_DETALLE"].ToString()) > 0)
                {
                    Nodo_Concepto = new XElement(cfdi + "Concepto");
                    Nodo_Concepto.SetAttributeValue("cantidad", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Cantidad].ToString())));
                    Nodo_Concepto.SetAttributeValue("unidad", Fila_Tabla[Ope_Facturas_Detalle.Campo_Unidad].ToString());
                    if (!String.IsNullOrEmpty(Fila_Tabla[Ope_Facturas_Detalle.Campo_Codigo].ToString()))
                    {
                        Nodo_Concepto.SetAttributeValue("noIdentificacion", Fila_Tabla[Ope_Facturas_Detalle.Campo_Codigo].ToString());
                    }
                    Nodo_Concepto.SetAttributeValue("descripcion", Cls_Metodos_Generales.CFD_Elimina_Espacios(Fila_Tabla[Ope_Facturas_Detalle.Campo_Descripcion].ToString()));
                    Nodo_Concepto.SetAttributeValue("valorUnitario", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Precio_Unitario].ToString())));
                    Nodo_Concepto.SetAttributeValue("importe", String.Format("{0:#0.00}", Convert.ToDouble(Fila_Tabla["SUBTOTAL_DETALLE"].ToString())));

                    // Si tiene cuenta predial
                    //if (!String.IsNullOrEmpty(Fila_Tabla[Ope_Facturas_Detalle.Campo_Cuenta_Predial].ToString()))
                    //{
                    //    // Crea nodo Cuenta Predial
                    //    Nodo_Cuenta_Predial = new XElement(cfdi + "CuentaPredial");
                    //    Nodo_Cuenta_Predial.SetAttributeValue("numero", Fila_Tabla[Ope_Facturas_Detalle.Campo_Cuenta_Predial].ToString());

                    //    Nodo_Concepto.Add(Nodo_Cuenta_Predial);
                    //}

                    Nodo_Conceptos.Add(Nodo_Concepto);
                }
            }

            Nodo_Raiz.Add(Nodo_Conceptos);

            // Crea nodo impuestos
            Nodo_Impuestos = new XElement(cfdi + "Impuestos");
            Impuestos_Trasladados = 0;

            if (Dt_Impuestos.Rows.Count > 0)
            {
                foreach (DataRow Fila_Tabla in Dt_Impuestos.Rows)
                {
                    Impuestos_Trasladados += Double.Parse(Fila_Tabla["IMPORTE"].ToString());
                }
                Nodo_Impuestos.SetAttributeValue("totalImpuestosTrasladados", String.Format("{0:#0.00}", Impuestos_Trasladados));
            }

            Nodo_Traslados = new XElement(cfdi + "Traslados");
            if (Dt_Impuestos.Rows.Count > 0)
            {
                foreach (DataRow Fila_Tabla in Dt_Impuestos.Rows)
                {
                    // Crea nodo de traslado por cada impuesto
                    XElement Nodo_Traslado = new XElement(cfdi + "Traslado");
                    Nodo_Traslado.SetAttributeValue("impuesto", Fila_Tabla["IMPUESTO"].ToString());
                    Nodo_Traslado.SetAttributeValue("tasa", String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["TASA"].ToString())));
                    Nodo_Traslado.SetAttributeValue("importe", String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString())));

                    Nodo_Traslados.Add(Nodo_Traslado);
                }
            }

            Nodo_Impuestos.Add(Nodo_Traslados);
            Nodo_Raiz.Add(Nodo_Impuestos);

            // Crea nodo Complementos
            Nodo_Complemento = new XElement(cfdi + "Complemento");

            /*Impuestos_Locales = 0;
            if (Dt_Impuestos_Locales.Rows.Count > 0)
            {
                foreach (DataRow Fila_Tabla in Dt_Impuestos_Locales.Rows)
                {
                    Impuestos_Locales += Double.Parse(Fila_Tabla["IMPORTE"].ToString());
                }
            }

            if (Impuestos_Locales > 0)
            {
                XElement Nodo_Impuesto_Local = new XElement(implocal + "ImpuestosLocales");

                Nodo_Impuesto_Local.SetAttributeValue("version", "1.0");
                Nodo_Impuesto_Local.SetAttributeValue("TotaldeRetenciones", String.Format("{0:#0.00}", Impuestos_Locales));
                Nodo_Impuesto_Local.SetAttributeValue("TotaldeTraslados", String.Format("{0:#0.00}", 0));
                Nodo_Impuesto_Local.SetAttributeValue(XNamespace.Xmlns + "implocal", "http://www.sat.gob.mx/implocal");

                Nodo_Complemento.Add(Nodo_Impuesto_Local);

                foreach (DataRow Fila_Tabla in Dt_Impuestos_Locales.Rows)
                {
                    XElement Nodo_Elemento = new XElement(implocal + "RetencionesLocales");

                    Nodo_Elemento.SetAttributeValue("ImpLocRetenido", Fila_Tabla["IMPUESTO"].ToString());
                    Nodo_Elemento.SetAttributeValue("TasadeRetencion", String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["TASA"].ToString())));
                    Nodo_Elemento.SetAttributeValue("Importe", String.Format("{0:#0.00}", Double.Parse(Fila_Tabla["IMPORTE"].ToString())));

                    Nodo_Impuesto_Local.Add(Nodo_Elemento);
                }

                Nodo_Complemento.Add(Nodo_Impuesto_Local);
            }
            Nodo_Raiz.Add(Nodo_Complemento);*/

            Xml_Documento.Add(Nodo_Raiz);

            // Se inicializan las variables del timbre
            Timbrado_VersionSat = String.Empty;
            Timbrado_UUID = String.Empty;
            Timbrado_FechaTimbrado = String.Empty;
            Timbrado_selloCFD = String.Empty;
            Timbrado_noCertificadoSAT = String.Empty;
            Timbrado_selloSAT = String.Empty;

            // Parametros del timbrado
            //Cls_Apl_Parametros_Impuestos_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Impuestos_Negocio();
            //Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();
            if (Dt_Parametros_Facturacion != null)
            {
                if (Dt_Parametros_Facturacion.Rows.Count > 0)
                {
                    Version_Timbrado = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Version].ToString();
                    Timbrado_Ambiente = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ambiente_Timbrado].ToString();
                    Sucursal_Id = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Sucursal_Id].ToString();
                    Codigo_Usuario = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Codigo_Usuario].ToString();
                    Codigo_Proveedor = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Codigo_Usuario_Proveedor].ToString();

                    if (Timbrado_Ambiente.Equals("PRUEBA"))
                    {
                        Respuesta = Cls_Timbrado.Generar_Timbrado(false, Version_Timbrado, Codigo_Proveedor, Codigo_Usuario, Sucursal_Id, Xml_Documento.ToString());
                    }
                    else
                    {
                        Respuesta = Cls_Timbrado.Generar_Timbrado(true, Version_Timbrado, Codigo_Proveedor, Codigo_Usuario, Sucursal_Id, Xml_Documento.ToString());
                    }
                    try
                    {
                        Xml_Respuesta.LoadXml(Respuesta);

                        Nodo_Respuesta = Xml_Respuesta.SelectSingleNode("//Timbre");
                        if (Nodo_Respuesta != null)
                        {
                            Timbrado_VersionSat = Nodo_Respuesta.Attributes[0].InnerText;
                            Timbrado_UUID = Nodo_Respuesta.Attributes[1].InnerText;
                            Timbrado_FechaTimbrado = Nodo_Respuesta.Attributes[2].InnerText;
                            Timbrado_selloCFD = Nodo_Respuesta.Attributes[3].InnerText;
                            Timbrado_noCertificadoSAT = Nodo_Respuesta.Attributes[4].InnerText;
                            Timbrado_selloSAT = Nodo_Respuesta.Attributes[5].InnerText;

                            Nodo_Timbrado = new XElement(tfd + "TimbreFiscalDigital");
                            Nodo_Timbrado.SetAttributeValue(xsi + "schemaLocation", "http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/TimbreFiscalDigital/TimbreFiscalDigital.xsd");
                            Nodo_Timbrado.SetAttributeValue(XNamespace.Xmlns + "tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");
                            Nodo_Timbrado.SetAttributeValue("version", Timbrado_VersionSat);
                            Nodo_Timbrado.SetAttributeValue("UUID", Timbrado_UUID);
                            Nodo_Timbrado.SetAttributeValue("FechaTimbrado", Timbrado_FechaTimbrado);
                            Nodo_Timbrado.SetAttributeValue("selloCFD", Timbrado_selloCFD);
                            Nodo_Timbrado.SetAttributeValue("noCertificadoSAT", Timbrado_noCertificadoSAT);
                            Nodo_Timbrado.SetAttributeValue("selloSAT", Timbrado_selloSAT);

                            // Complementa la factura
                            Timbrado_FechaTimbrado = Timbrado_FechaTimbrado.Replace('T', ' ');
                            Fecha_Timbrado = Convert.ToDateTime(Timbrado_FechaTimbrado);
                            Rs_Nota_Credito.P_Timbre_Version = Timbrado_VersionSat;
                            Rs_Nota_Credito.P_Timbre_Uuid = Timbrado_UUID;
                            Rs_Nota_Credito.P_Timbre_Fecha_Timbrado = Fecha_Timbrado.ToString();
                            Rs_Nota_Credito.P_Timbre_Sello_Cfd = Timbrado_selloCFD;
                            Rs_Nota_Credito.P_Timbre_No_Certificado_Sat = Timbrado_noCertificadoSAT;
                            Rs_Nota_Credito.P_Timbre_Sello_Sat = Timbrado_selloSAT;

                            Nodo_Complemento.Add(Nodo_Timbrado);
                            Nodo_Raiz.Add(Nodo_Complemento);
                        }
                        else
                        {
                            Nodo_Respuesta = Xml_Respuesta.SelectSingleNode("//Error");

                            if (Nodo_Respuesta != null)
                            {
                                throw new Exception("ErrorTimbrado  Error: " + Nodo_Respuesta.Attributes[0].InnerText + " " + Nodo_Respuesta.Attributes[1].InnerText);
                            }
                            else
                            {
                                throw new Exception("ErrorTimbrado Error: El Servidor de Timbrado no respondio");
                            }
                        }
                    }
                    catch (Exception Ex_Respuesta)
                    {
                        throw new Exception(Ex_Respuesta.Message);
                    }
                }
                else
                {
                    throw new Exception("No se encontraron los par&aacute;metros del timbrado.");
                }
            }
            else
            {
                throw new Exception("No se encontraron los par&aacute;metros del timbrado.");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Crea_Xml_CFDI Error: " + Ex.Message);
        }
        return Xml_Documento;
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Mostrar_PDF
    ///DESCRIPCIÓN: Crea y muestra el pdf en una ventana del navegador.
    ///PARAMETROS:  No_Factura, Numero de la factura a crear.
    ///             No_Serie, Serie de la factura a crear.
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  23/Mayo/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataSet Crea_Pdf(Cls_Ope_Notas_Credito_Negocio Rs_Nota_Credito, String Regimen_Fiscal, String[] Ruta_Imagen_Factura, String Tipo_Comprobante)
    {
        DataSet Ds_Nota_Credito = new DataSet();
        String Cadena_Original = String.Empty;
        try
        {
            DataTable Dt_Nota_Credito = new DataTable();
            DataTable Dt_Nota_Credito_Detalles = new DataTable();
            DataTable Dt_Empresa = ((DataTable)Session["Dt_Datos_Empresa"]);
            DataTable Dt_Impuestos = Hacer_Tabla_Impuestos();

            Dt_Nota_Credito = Rs_Nota_Credito.Consultar_Nota_Credito();
            Dt_Nota_Credito_Detalles = Rs_Nota_Credito.Consultar_Detalle_Nota_Credito();

            String Tipo = Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Tipo_Nota_Credito].ToString();
            String Expedida_En = String.Empty;
            Rs_Nota_Credito.P_Subtotal = Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Subtotal].ToString();
            Rs_Nota_Credito.P_Iva = Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Iva].ToString();
            Rs_Nota_Credito.P_Total = Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Total].ToString();

            // Rellena el DataTable de impuestos
            Cls_Apl_Parametros_Facturacion_Negocio Rs_Impuestos = new Cls_Apl_Parametros_Facturacion_Negocio();
            DataTable Dt_Porcentaje_Impuestos = Rs_Impuestos.Consultar_Parametros();

            DataRow Dr_Impuestos = Dt_Impuestos.NewRow();
            if (Double.Parse((String.IsNullOrEmpty(Rs_Nota_Credito.P_Iva) ? "0" : Rs_Nota_Credito.P_Iva)) > 0)
            {
                Dr_Impuestos["IMPUESTO"] = "IVA";
                Dr_Impuestos["TASA"] = Dt_Porcentaje_Impuestos.Rows[0]["Porcentaje_IVA"].ToString();
                //Dt_Nota_Credito.Rows[0]["Tasa_IVA_General"] = Dt_Porcentaje_Impuestos.Rows[0]["Porcentaje_IVA"].ToString();
                Dr_Impuestos["IMPORTE"] = Rs_Nota_Credito.P_Iva;
            }
            else
            {
                Dr_Impuestos["IMPUESTO"] = "IVA";
                Dr_Impuestos["TASA"] = "0";
                //Dt_Nota_Credito.Rows[0]["Tasa_IVA_General"] = "0";
                Dr_Impuestos["IMPORTE"] = "0";
            }
            Dt_Impuestos.Rows.Add(Dr_Impuestos);

            Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Datos_Fiscales = new Cls_Ope_Cor_Facturacion_Global_Negocio();
            Rs_Datos_Fiscales.Empresa_ID = "00001";
            Dt_Empresa = Rs_Datos_Fiscales.Consultar_Datos_Fiscales();

            Expedida_En = (Dt_Empresa.Rows[0]["Ciudad"].ToString() + ", " + Dt_Empresa.Rows[0]["Estado"].ToString()).ToUpper(); ;
            //Cadena_Original = Cls_Timbrado.Genera_Cadena_Original(Dt_Nota_Credito, Expedida_En, Tipo_Comprobante, Regimen_Fiscal, Dt_Empresa, Dt_Nota_Credito_Detalles, Dt_Impuestos);

            Cadena_Original = "||" + Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Timbre_Version].ToString().Trim();
            Cadena_Original += "|" + Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Timbre_Uuid].ToString().Trim();
            Cadena_Original += "|" + String.Format("{0:yyyy-MM-dd}", Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Timbre_Fecha_Timbrado]);
            Cadena_Original += "T" + String.Format("{0:HH:mm:ss}", Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Timbre_Fecha_Timbrado]);
            Cadena_Original += "|" + Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Timbre_Sello_Cfd].ToString().Trim();
            Cadena_Original += "|" + Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Timbre_No_Certificado_Sat].ToString().Trim() + "||";

            // Renombra las tablas
            Dt_Nota_Credito.TableName = "DT_NOTA_CREDITO";
            Dt_Nota_Credito_Detalles.TableName = "DT_NOTA_CREDITO_DETALLES";
            Dt_Empresa.TableName = "CAT_EMPRESAS";
            Dt_Porcentaje_Impuestos.TableName = "APL_CAT_PARAMETROS_IMPUESTOS";

            // Agrega otros campos que se van a enviar.
            // Encabezado factura
            Dt_Nota_Credito.Columns.Add("IMPORTE_LETRA", typeof(String));
            Dt_Nota_Credito.Columns.Add("CADENA_ORIGINAL", typeof(String));
            Dt_Nota_Credito.Columns.Add("IMAGEN_QR", typeof(Byte[]));
            Dt_Nota_Credito.Columns.Add("TASA_IVA_GENERAL", typeof(String));
            Dt_Nota_Credito.Columns.Add("NO_CUENTA", typeof(String));
            Dt_Nota_Credito.Columns.Add("TIPO_SERVICIO", typeof(String));
            Dt_Nota_Credito.Columns.Add("BIMESTRE_PAGADO", typeof(String));

            // Datos de la Empresa
            if (!Dt_Empresa.Columns.Contains("IMAGEN"))
            {
                Dt_Empresa.Columns.Add("IMAGEN", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("IMAGEN_2"))
            {
                Dt_Empresa.Columns.Add("IMAGEN_2", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("IMAGEN_3"))
            {
                Dt_Empresa.Columns.Add("IMAGEN_3", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("IMAGEN_4"))
            {
                Dt_Empresa.Columns.Add("IMAGEN_4", typeof(Byte[]));
            }
            if (!Dt_Empresa.Columns.Contains("REGIMEN_FISCAL"))
            {
                Dt_Empresa.Columns.Add("REGIMEN_FISCAL", typeof(String));
            }
            if (!Dt_Empresa.Columns.Contains("EXPEDIDO_EN"))
            {
                Dt_Empresa.Columns.Add("EXPEDIDO_EN", typeof(String));
            }

            // Rellena los nuevos campos
            // Encabezado factura
            if (Double.Parse((String.IsNullOrEmpty(Rs_Nota_Credito.P_Iva) ? "0" : Rs_Nota_Credito.P_Iva)) > 0)
                Dt_Nota_Credito.Rows[0]["Tasa_IVA_General"] = Dt_Porcentaje_Impuestos.Rows[0]["Porcentaje_IVA"].ToString();
            else
                Dt_Nota_Credito.Rows[0]["Tasa_IVA_General"] = "0.00";
            Dt_Nota_Credito.Rows[0]["IMPORTE_LETRA"] = Cls_Metodos_Generales.Convertir_Cantidad_Letras(Convert.ToDecimal(Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Total].ToString()), Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Tipo_Moneda].ToString());
            Dt_Nota_Credito.Rows[0]["CADENA_ORIGINAL"] = Cadena_Original;
            Dt_Nota_Credito.Rows[0]["IMAGEN_QR"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath("../" + Dt_Nota_Credito.Rows[0][Ope_Notas_Credito.Campo_Ruta_Codigo_Bd].ToString()), 200, 300);
            Dt_Nota_Credito.Rows[0]["NO_CUENTA"] = Rs_Nota_Credito.P_No_Cuenta;
            Dt_Nota_Credito.Rows[0]["TIPO_SERVICIO"] = Rs_Nota_Credito.P_Tipo_Servicio;
            Dt_Nota_Credito.Rows[0]["BIMESTRE_PAGADO"] = Rs_Nota_Credito.P_Bimestre_Pagado;

            // Datos de la Empresa
            Dt_Empresa.Rows[0]["REGIMEN_FISCAL"] = Regimen_Fiscal;
            Dt_Empresa.Rows[0]["EXPEDIDO_EN"] = Expedida_En;
            Dt_Empresa.Rows[0]["IMAGEN"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath(Ruta_Imagen_Factura[0]), 400, 300);
            Dt_Empresa.Rows[0]["IMAGEN_2"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath(Ruta_Imagen_Factura[1]), 400, 300);
            Dt_Empresa.Rows[0]["IMAGEN_3"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath(Ruta_Imagen_Factura[2]), 400, 300);
            Dt_Empresa.Rows[0]["IMAGEN_4"] = Cls_Metodos_Generales.Convertir_Imagen_Bytes(Server.MapPath(Ruta_Imagen_Factura[3]), 400, 300);

            Ds_Nota_Credito.Tables.Add(Dt_Nota_Credito.Copy());
            Ds_Nota_Credito.Tables.Add(Dt_Nota_Credito_Detalles.Copy());
            Ds_Nota_Credito.Tables.Add(Dt_Empresa.Copy());
            Ds_Nota_Credito.Tables.Add(Dt_Porcentaje_Impuestos.Copy());
        }
        catch (Exception Ex)
        {
            throw new Exception("Crea_Pdf Error:[" + Ex.Message + "]");
        }

        return Ds_Nota_Credito;
    }

    #endregion

    #region Eventos

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Factura_Click
    ///DESCRIPCIÓN: Agrega la factura seleccionada del grid_facturas, al grid de las
    ///             facutras seleccionadas.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Agregar_Factura_Click(object sender, EventArgs e)
    {
        try
        {
            Actualizar_Facturas_Seleccionadas();
            DataTable Dt_Auxiliar = new DataTable();
            Dt_Auxiliar = ((DataTable)Session["Dt_Factura_Seleccionada"]);

            if (Grid_Facturas.SelectedIndex > -1)
            {
                DataTable Dt_Facturas = new DataTable();
                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Factura = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                Rs_Factura.P_No_Factura = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[0].ToString();
                Rs_Factura.P_Serie = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[1].ToString();
                Dt_Facturas = Rs_Factura.Consultar_Factura();

                // Si todavia no se ha agregado alguna factura, fija el cliente.
                if (Dt_Auxiliar.Rows.Count == 0)
                {
                    Consulta_Datos_Cliente(Dt_Facturas.Rows[0][Ope_Facturas.Campo_Cliente_Id].ToString(), true);
                    Cmb_Tipo_Moneda.Enabled = false;
                    Txt_Tipo_Cambio.Enabled = false;
                    Cmb_Tipo_Nota.Enabled = false;
                }
                if (Hfd_Impuesto.Value == "")
                {
                    DataRow Dr = Dt_Auxiliar.NewRow();
                    Dr[Ope_Facturas.Campo_Serie] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_Serie].ToString();
                    Dr[Ope_Facturas.Campo_No_Factura] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_No_Factura].ToString();
                    Dr[Ope_Facturas.Campo_Razon_Social] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_Razon_Social].ToString();
                    Dr[Ope_Facturas.Campo_Tipo_Moneda] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_Tipo_Moneda].ToString();
                    Dr[Ope_Facturas.Campo_Total] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Facturas.Rows[0][Ope_Facturas.Campo_Total].ToString()));
                    Dr[Ope_Facturas.Campo_Saldo] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Facturas.Rows[0][Ope_Facturas.Campo_Saldo].ToString()));
                    Dr["IMPORTE"] = "0";
                    Dt_Auxiliar.Rows.Add(Dr);
                    Hfd_Impuesto.Value = Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[6].Text.Trim();

                    // Elimina del grid la factura seleccionada.
                    DataTable Dt_Facturas_Busqueda = ((DataTable)Session["Dt_Facturas_Buscadas"]);
                    Dt_Facturas_Busqueda.Rows.RemoveAt(Grid_Facturas.SelectedIndex);
                    Grid_Facturas.DataSource = Dt_Facturas_Busqueda;
                    Grid_Facturas.DataBind();
                    Session["Dt_Facturas_Buscadas"] = Dt_Facturas_Busqueda;
                    Grid_Facturas.SelectedIndex = -1;
                }
                else
                {
                    if (Hfd_Impuesto.Value == Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[6].Text.Trim())
                    {
                        DataRow Dr = Dt_Auxiliar.NewRow();
                        Dr[Ope_Facturas.Campo_Serie] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_Serie].ToString();
                        Dr[Ope_Facturas.Campo_No_Factura] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_No_Factura].ToString();
                        Dr[Ope_Facturas.Campo_Razon_Social] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_Razon_Social].ToString();
                        Dr[Ope_Facturas.Campo_Tipo_Moneda] = Dt_Facturas.Rows[0][Ope_Facturas.Campo_Tipo_Moneda].ToString();
                        Dr[Ope_Facturas.Campo_Total] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Facturas.Rows[0][Ope_Facturas.Campo_Total].ToString()));
                        Dr[Ope_Facturas.Campo_Saldo] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Facturas.Rows[0][Ope_Facturas.Campo_Saldo].ToString()));
                        Dr["IMPORTE"] = "0";
                        Dt_Auxiliar.Rows.Add(Dr);

                        // Elimina del grid la factura seleccionada.
                        DataTable Dt_Facturas_Busqueda = ((DataTable)Session["Dt_Facturas_Buscadas"]);
                        Dt_Facturas_Busqueda.Rows.RemoveAt(Grid_Facturas.SelectedIndex);
                        Grid_Facturas.DataSource = Dt_Facturas_Busqueda;
                        Grid_Facturas.DataBind();
                        Session["Dt_Facturas_Buscadas"] = Dt_Facturas_Busqueda;
                        Grid_Facturas.SelectedIndex = -1;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Solo se pueden seleccionar Facturas <br/> con el mismo tipo de impuesto.');", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('Notas de Cr&eacute;dito','Solo se pueden seleccionar Facturas <br/> con el mismo tipo de impuesto.', 'warning');", true);
                    }
                }
                Grid_Facturas_Seleccionadas.Columns[6].Visible = true;
                Grid_Facturas_Seleccionadas.Columns[7].Visible = false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('No se ha seleccionado ninguna factura.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('Notas de Cr&eacute;dito','No se ha seleccionado ninguna factura.', 'warning');", true);
            }
            Session["Dt_Factura_Seleccionada"] = Dt_Auxiliar;
            Rellena_Grid();
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Partida_Click
    ///DESCRIPCIÓN: Agrega la partida selecionada a las partidas a devolver.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Agregar_Partida_Click(object sender, EventArgs e)
    {
        Cls_Ope_Cor_Facturacion_Global_Negocio Obj_Facturacion_Global = new Cls_Ope_Cor_Facturacion_Global_Negocio();
        DataTable Aux = null;

        try
        {
            int Indice = 0;
            int Indice_2 = Grid_Partidas_Factura.SelectedIndex;
            String Indice_3 = String.Empty;
            int Pagina = Grid_Partidas_Factura.PageIndex;
            int Cantidad = Convert.ToInt32(Hfd_Cantidad_Partida.Value);

            Indice_3 = Convert.ToString(Pagina) + Convert.ToString(Indice_2);
            Indice = Convert.ToInt32(Indice_3);

            if (Indice > -1)
            {
                if (Cantidad > 0)
                {
                    Boolean Existe = false;
                    DataTable Dt_Partidas_Factura = new DataTable();
                    DataTable Dt_Partidas_Devolver = new DataTable();

                    Double Precio = 0;
                    Double Subtotal = 0;
                    Double Porcentaje = 0;
                    Double Iva = 0;
                    Double Total = 0;

                    Obj_Facturacion_Global.P_No_Recibo = string.Format("{0:0000000000}", Convert.ToInt32(Grid_Partidas_Factura.Rows[Grid_Partidas_Factura.SelectedIndex].Cells[2].Text.Trim()));
                    Aux = Obj_Facturacion_Global.Consultar_Recibo_NC();
                    if (Aux.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('El numero de recibo seleccionado, ya esta en una nota de credito activa. Favor de verificarlo');", true);
                        return;
                    }

                    Dt_Partidas_Factura = ((DataTable)Session["Dt_Partidas_Factura"]);

                    if (Grid_Partidas_Devolver.Rows.Count == 0)
                    {
                        Dt_Partidas_Devolver = Hacer_Tabla_Partidas_Factura();
                        Dt_Partidas_Devolver.Columns.Add("NO_RECIBO", typeof(String));
                        Dt_Partidas_Devolver.Columns.Add("NO_CUENTA", typeof(String));
                        Dt_Partidas_Devolver.Columns.Add("TIPO_SERVICIO", typeof(String));
                        Dt_Partidas_Devolver.Columns.Add("BIMESTRE_PAGADO", typeof(String));
                    }
                    else
                    {
                        Dt_Partidas_Devolver = ((DataTable)Session["Dt_Partidas_Devolver"]);
                    }

                    if (Dt_Partidas_Devolver.Rows.Count == 0)
                    {
                        Consulta_Datos_Cliente(HttpUtility.HtmlDecode(Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[3].Text), false);
                        Cmb_Tipo_Moneda.Enabled = false;
                        Txt_Tipo_Cambio.Enabled = false;
                        Cmb_Tipo_Nota.Enabled = false;
                        Pnl_Busqueda_Facturas.Enabled = false;
                    }
                    else
                    {
                        // Comprueba si existe la partida
                        String Codigo_Seleccionado = Grid_Partidas_Factura.Rows[Grid_Partidas_Factura.SelectedIndex].Cells[3].Text;
                        String Descripcion_Seleccionado = Grid_Partidas_Factura.Rows[Grid_Partidas_Factura.SelectedIndex].Cells[4].Text;
                        foreach (DataRow Fila_Dt_Devolver in Dt_Partidas_Devolver.Rows)
                        {
                            if ((Codigo_Seleccionado == Fila_Dt_Devolver[Ope_Facturas_Detalle.Campo_Codigo].ToString())
                                && (Descripcion_Seleccionado == Fila_Dt_Devolver[Ope_Facturas_Detalle.Campo_Descripcion].ToString()))
                            {
                                Existe = true;
                                break;
                            }
                        }
                    }

                    if (!Existe)
                    {
                        Limpia_Totales();
                        DataTable Dt_Facturas = null;
                        Cls_Ope_Cor_Facturacion_Global_Negocio Obj_Facturas = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                        Obj_Facturas.P_No_Factura = Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[2].Text;
                        Dt_Facturas = Obj_Facturas.Consultar_Factura();
                        //si el campo cliente id es null o vacio es una factura global
                        if (string.IsNullOrEmpty(Dt_Facturas.Rows[0]["Cliente_ID"].ToString()))
                        {
                            foreach (DataRow Fila_Tabla in Dt_Partidas_Factura.Rows)
                            {
                                if (Fila_Tabla["NO_RECIBO"].ToString() == Grid_Partidas_Factura.Rows[Grid_Partidas_Factura.SelectedIndex].Cells[2].Text.Trim())
                                {
                                    if (Fila_Tabla[Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto].ToString() != "")
                                    {
                                        Porcentaje = Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto].ToString());
                                    }
                                    Precio = Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Precio_Unitario].ToString());

                                    if (Fila_Tabla[Ope_Facturas_Detalle.Campo_Descripcion].ToString() != "IVA")
                                    {
                                        DataRow Dr = Dt_Partidas_Devolver.NewRow();
                                        //Dr[Ope_Facturas_Detalle.Campo_Producto_Id] = Dt_Partidas_Factura.Rows[Indice][Ope_Facturas_Detalle.Campo_Producto_Id];
                                        Dr[Ope_Facturas_Detalle.Campo_No_Factura] = Fila_Tabla[Ope_Facturas_Detalle.Campo_No_Factura];
                                        Dr["SERIE_FACTURA"] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Serie];
                                        Dr["NO_RECIBO"] = Fila_Tabla["NO_RECIBO"];
                                        Dr[Ope_Facturas_Detalle.Campo_Cantidad] = Cantidad;
                                        Dr[Ope_Facturas_Detalle.Campo_Codigo] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Codigo];
                                        //if (!String.IsNullOrEmpty(Fila_Tabla[Ope_Facturas_Detalle.Campo_Cuenta_Predial].ToString()))
                                        //{
                                        //    Dr[Ope_Facturas_Detalle.Campo_Cuenta_Predial] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Cuenta_Predial];
                                        //}
                                        Dr[Ope_Facturas_Detalle.Campo_Descripcion] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Descripcion];
                                        Dr[Ope_Facturas_Detalle.Campo_Unidad] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Unidad];
                                        Dr["NO_CUENTA"] = Fila_Tabla["NO_CUENTA"];
                                        Dr["BIMESTRE_PAGADO"] = Fila_Tabla["BIMESTRE_PAGADO"];
                                        Dr["TIPO_SERVICIO"] = Fila_Tabla["TIPO_SERVICIO"];
                                        Dr[Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto] = Porcentaje;
                                        Dr[Ope_Facturas_Detalle.Campo_Precio_Unitario] = String.Format("{0:#,###0.#0}", Precio);
                                        Dr["COSTO"] = String.Format("{0:#,###0.#0}", 0);
                                        Dr["SUBTOTAL_DETALLE"] = String.Format("{0:#,###0.#0}", Fila_Tabla[Ope_Facturas_Detalle.Campo_Subtotal]);
                                        Dr[Ope_Facturas_Detalle.Campo_Iva] = String.Format("{0:#,###0.#0}", Fila_Tabla[Ope_Facturas_Detalle.Campo_Iva]);
                                        Dr["TOTAL_DETALLE"] = String.Format("{0:#,###0.#0}", Fila_Tabla["TOTAL"]);
                                        Dt_Partidas_Devolver.Rows.Add(Dr);
                                        Subtotal = Convert.ToDouble(Fila_Tabla["TOTAL"]) + Subtotal;
                                    }
                                    else
                                    {
                                        Iva = Convert.ToDouble(Fila_Tabla["TOTAL"]) + Iva;
                                    }
                                }
                            }

                            Session["Dt_Partidas_Devolver"] = Dt_Partidas_Devolver;
                            Grid_Partidas_Devolver.DataSource = Dt_Partidas_Devolver;
                            Grid_Partidas_Devolver.DataBind();
                            Grid_Partidas_Factura.SelectedIndex = -1;

                            //if (!string.IsNullOrEmpty(Grid_Facturas.SelectedRow.Cells[6].Text))
                            if (Iva > 0)
                            {
                                Txt_Iva.Text = Iva.ToString("N2");
                                Lbl_Iva.Text = Grid_Facturas.SelectedRow.Cells[6].Text;
                            }
                            else
                            {
                                Txt_Iva.Text = Iva.ToString("N2");
                                if (Txt_Iva.Text != "")
                                {
                                    Lbl_Iva.Text = Porcentaje.ToString();
                                }
                            }
                            Txt_Subtotal.Text = Subtotal.ToString("N2");
                            Txt_Total.Text = (Subtotal + Iva).ToString("N2");

                        }
                        else
                        {   //si entra aqui es x k la factura no es global...
                            foreach (DataRow Fila_Tabla in Dt_Partidas_Factura.Rows)
                            {
                                if (Fila_Tabla["NO_RECIBO"].ToString() == Grid_Partidas_Factura.Rows[Grid_Partidas_Factura.SelectedIndex].Cells[2].Text.Trim())
                                {
                                    if (Fila_Tabla[Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto].ToString() != "")
                                    {
                                        Porcentaje = Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto].ToString());
                                    }
                                    Precio = Convert.ToDouble(Fila_Tabla[Ope_Facturas_Detalle.Campo_Precio_Unitario].ToString());

                                    //if (Fila_Tabla[Ope_Facturas_Detalle.Campo_Descripcion].ToString() != "IVA")
                                    //{
                                    DataRow Dr = Dt_Partidas_Devolver.NewRow();
                                    //Dr[Ope_Facturas_Detalle.Campo_Producto_Id] = Dt_Partidas_Factura.Rows[Indice][Ope_Facturas_Detalle.Campo_Producto_Id];
                                    Dr[Ope_Facturas_Detalle.Campo_No_Factura] = Fila_Tabla[Ope_Facturas_Detalle.Campo_No_Factura];
                                    Dr["SERIE_FACTURA"] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Serie];
                                    Dr["NO_RECIBO"] = Fila_Tabla["NO_RECIBO"];
                                    Dr[Ope_Facturas_Detalle.Campo_Cantidad] = Cantidad;
                                    Dr[Ope_Facturas_Detalle.Campo_Codigo] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Codigo];
                                    //if (!String.IsNullOrEmpty(Fila_Tabla[Ope_Facturas_Detalle.Campo_Cuenta_Predial].ToString()))
                                    //{
                                    //    Dr[Ope_Facturas_Detalle.Campo_Cuenta_Predial] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Cuenta_Predial];
                                    //}
                                    Dr[Ope_Facturas_Detalle.Campo_Descripcion] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Descripcion];
                                    Dr[Ope_Facturas_Detalle.Campo_Unidad] = Fila_Tabla[Ope_Facturas_Detalle.Campo_Unidad];
                                    Dr["NO_CUENTA"] = Fila_Tabla["NO_CUENTA"];
                                    Dr["BIMESTRE_PAGADO"] = Fila_Tabla["BIMESTRE_PAGADO"];
                                    Dr["TIPO_SERVICIO"] = Fila_Tabla["TIPO_SERVICIO"];
                                    Dr[Ope_Facturas_Detalle.Campo_Porcentaje_Impuesto] = Porcentaje;
                                    Dr[Ope_Facturas_Detalle.Campo_Precio_Unitario] = String.Format("{0:#,###0.#0}", Precio);
                                    Dr["COSTO"] = String.Format("{0:#,###0.#0}", 0);
                                    Dr["SUBTOTAL_DETALLE"] = String.Format("{0:#,###0.#0}", Fila_Tabla[Ope_Facturas_Detalle.Campo_Subtotal]);
                                    Dr[Ope_Facturas_Detalle.Campo_Iva] = String.Format("{0:#,###0.#0}", Fila_Tabla[Ope_Facturas_Detalle.Campo_Iva]);
                                    Dr["TOTAL_DETALLE"] = String.Format("{0:#,###0.#0}", Fila_Tabla["TOTAL"]);
                                    Dt_Partidas_Devolver.Rows.Add(Dr);
                                    Subtotal = Convert.ToDouble(Fila_Tabla["SUBTOTAL"]) + Subtotal;
                                    Iva = Convert.ToDouble(Fila_Tabla["IVA"]) + Iva;
                                    //}
                                }
                            }

                            Session["Dt_Partidas_Devolver"] = Dt_Partidas_Devolver;
                            Grid_Partidas_Devolver.DataSource = Dt_Partidas_Devolver;
                            Grid_Partidas_Devolver.DataBind();
                            Grid_Partidas_Factura.SelectedIndex = -1;

                            if (!string.IsNullOrEmpty(Grid_Facturas.SelectedRow.Cells[6].Text))
                            {
                                Txt_Iva.Text = Iva.ToString("N2");
                                Lbl_Iva.Text = Grid_Facturas.SelectedRow.Cells[6].Text;
                            }
                            else
                            {
                                Txt_Iva.Text = Iva.ToString("N2");
                                if (Txt_Iva.Text != "")
                                {
                                    Lbl_Iva.Text = Porcentaje.ToString();
                                }
                            }
                            Txt_Subtotal.Text = Subtotal.ToString("N2");
                            Txt_Total.Text = (Subtotal + Iva).ToString("N2");
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('La cantidad debe ser mayor a cero.');", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('Notas de Cr&eacute;dito','La cantidad debe ser mayor a cero.', 'warning');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Ya existe la partida seleccionada.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('Notas de Cr&eacute;dito','Ya existe la partida seleccionada.', 'warning');", true);
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Cliente_Grid_Click
    ///DESCRIPCIÓN: Busca el cliente.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Cliente_Grid_Click(object sender, EventArgs e)
    {
        Llenar_Grid_Clientes(0);
        Mdp_Clientes.Show();
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Factura_Click
    ///DESCRIPCIÓN: Busca la factura.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Factura_Click(object sender, EventArgs e)
    {
        Llenar_Grid_Facturas(0);
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Cerrar_Modal_Clientes_Click
    ///DESCRIPCIÓN: Cierra la ventana emergente de busqueda de clientes.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Cerrar_Modal_Clientes_Click(object sender, EventArgs e)
    {
        Mdp_Clientes.Hide();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Cerrar_Modal_Facturas_Click
    ///DESCRIPCIÓN: Cierra la ventana emergente de la búsqueda de facturas.
    ///PARAMETROS:  
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  22/Abril/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Cerrar_Modal_Notas_Credito_Click(object sender, ImageClickEventArgs e)
    {
        Mdp_Notas_Credito.Hide();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Factura_Grid_Click
    ///DESCRIPCIÓN: Evento que busca llama a la función Llenar_Grid_Facturas para buscar
    ///             la factura.
    ///PARAMETROS:  
    ///CREO:        Luis Alberto Salas
    ///FECHA_CREO:  22/Abril/2012
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Notas_Credito_Click(object sender, ImageClickEventArgs e)
    {
        Llenar_Grid_Notas_Credito(0);
        Mdp_Notas_Credito.Show();
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Guarda la nota de credito.
    ///PARAMETROS: 
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        if (Btn_Nuevo.AlternateText.Contains("Nuevo"))
        {
            Estado_Formulario("Nuevo");
            Configuracion_Formulario(false);
            Txt_Fecha_Emision.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
            Consulta_Maximo_Serie("NOTA CREDITO");

            Session["Dt_Factura_Seleccionada"] = Hacer_Tabla_Factura_Seleccionada();
            Btn_Buscar.Enabled = false;
        }
        else
        {
            Double Tipo_Cambio = 0.0;
            String Id_Creado = String.Empty;
            String Tipo = Cmb_Tipo_Nota.SelectedValue.ToString();

            String Nombre_Empresa = String.Empty;
            String Codigo_Bidimensional = String.Empty;
            Int32 Quitar_Tiempo = 0;

            // Complementos de factura
            String Cadena_Original = String.Empty;
            String Cadena_UTF = String.Empty;
            String Cadena_Sello = String.Empty;

            // Parametros de facturacion
            String Regimen_Fiscal = String.Empty;
            String Expedida_En = String.Empty;
            String Timbre_Version = String.Empty;
            String Archivo_Cer = String.Empty;
            String Archivo_Key = String.Empty;
            String Password_Llave = String.Empty;

            // Documento Xml
            XDocument Xml_Documento = new XDocument(new XDeclaration("1.0", "utf-8", "yes"));

            // Ruta de archivos
            String Ruta_Xml = String.Empty;
            String Ruta_Qr = String.Empty;
            String Ruta_Cert = String.Empty;
            String Ruta_Key = String.Empty;
            String Ruta_Pdf = String.Empty;
            String Tipo_Comprobante = String.Empty;
            String[] Ruta_Imagen_Empresa = new string[4];

            // Datos del correo
            String Mensaje = String.Empty;
            String Envia = String.Empty;
            String Mensaje_Error_Correo = String.Empty;
            Boolean Envio_Correo = false;

            // Datatables
            DataTable Dt_Impuestos = Hacer_Tabla_Impuestos();
            DataTable Dt_Vacio = Hacer_Tabla_Impuestos();
            DataTable Dt_Notas_Credito_Detalles = new DataTable();
            DataTable Dt_Notas_Credito_Detalles_Alta = new DataTable();
            DataTable Dt_Empresa = new DataTable();
            DataTable Dt_Factura = new DataTable();
            DataTable Dt_Parametros_Facturacion = new DataTable();
            DataTable Dt_Cotizacion = new DataTable();
            DataTable Dt_Prototipos = new DataTable();
            DataTable Dt_Datos_Fiscales = new DataTable();


            // Dataset de la factura
            DataSet Ds_Factura_Pdf = new DataSet();

            // Iva general
            Double Iva_General = 0;

            Boolean Se_Realizo_El_Registro_Factuacion = false;
            SqlConnection Obj_Conexion = null;
            SqlTransaction Obj_Transaccion = null;
            try
            {
                Obj_Conexion = new SqlConnection(Cls_Constantes.Str_Conexion);
                Cls_Ope_Notas_Credito_Negocio Rs_Notas_Credito = new Cls_Ope_Notas_Credito_Negocio();
                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Datos_Fiscales = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                Rs_Datos_Fiscales.Empresa_ID = "00001";
                Dt_Datos_Fiscales = Rs_Datos_Fiscales.Consultar_Datos_Fiscales();
                Nombre_Empresa = Dt_Datos_Fiscales.Rows[0]["NOMBRE_CORTO"].ToString();

                // Lee los parametros de facturacion
                Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
                Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();

                if (!String.IsNullOrEmpty(Convert.ToString(Dt_Parametros_Facturacion.Rows[0]["QUITAR_TIEMPO_TIMBRADO"])))
                {
                    Quitar_Tiempo = (Convert.ToInt32(Dt_Parametros_Facturacion.Rows[0]["QUITAR_TIEMPO_TIMBRADO"]) * (-1));
                }
                else
                {
                    Quitar_Tiempo = 0;
                }

                // Carga los detalles de la factura
                //Actualizar_Partidas();
                if (Tipo.Equals("DEVOLUCION"))
                {
                    Dt_Notas_Credito_Detalles = ((DataTable)Session["Dt_Partidas_Devolver"]);
                }
                else
                {
                    Dt_Notas_Credito_Detalles = Construir_Detalles(Tipo);
                }

                if (!String.IsNullOrEmpty(Lbl_Iva.Text))
                {
                    Iva_General = Double.Parse(Lbl_Iva.Text);
                }

                // Carga los datos de la empresa
                //Dt_Empresa = ((DataTable)Session["Dt_Datos_Empresa"]);
                Dt_Empresa = Dt_Datos_Fiscales;
                if (Txt_Tipo_Cambio.Text != String.Empty)
                {
                    Tipo_Cambio = Double.Parse(Regex.Replace(Txt_Tipo_Cambio.Text, @"[$,]", ""));
                }
                // Datos Generales de la Factura.
                Id_Creado = Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString().Trim() + "," + Txt_No_Nota.Text.Trim();
                Rs_Notas_Credito.P_No_Nota_Credito = Txt_No_Nota.Text.Trim();
                Rs_Notas_Credito.P_Serie = Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString().Trim();
                if (Tipo.Equals("DEVOLUCION"))
                {
                    Rs_Notas_Credito.P_No_Factura = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[0].ToString();
                    Rs_Notas_Credito.P_Serie_Factura = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[1].ToString();
                }
                else
                {
                    Rs_Notas_Credito.P_No_Factura = Grid_Facturas_Seleccionadas.Rows[0].Cells[1].Text.ToString();
                    Rs_Notas_Credito.P_Serie_Factura = Grid_Facturas_Seleccionadas.Rows[0].Cells[0].Text.ToString();
                }
                Rs_Notas_Credito.P_Fecha_Creo_Xml = DateTime.Now.AddMinutes(Quitar_Tiempo).ToString("dd/MM/yyyy HH:mm:ss");
                Rs_Notas_Credito.P_Fecha_Xml = Convert.ToString(DateTime.Now.AddMinutes(Quitar_Tiempo));
                Rs_Notas_Credito.P_Cliente_Id = Grid_Clientes.DataKeys[Grid_Clientes.SelectedRow.RowIndex].Value.ToString();
                Rs_Notas_Credito.P_Email = Hfd_Correo_Cliente.Value.ToString();

                // Es obligatoria
                Rs_Notas_Credito.P_Forma_Pago = "";
                Rs_Notas_Credito.P_Tipo_Comprobante = "EGRESO";
                Rs_Notas_Credito.P_Metodo_Pago = "NO IDENTIFICADO";
                Rs_Notas_Credito.P_Fecha_Emision = Rs_Notas_Credito.P_Fecha_Xml;
                Rs_Notas_Credito.P_Fecha_Pago = Rs_Notas_Credito.P_Fecha_Xml;
                Rs_Notas_Credito.P_Tipo_Nota_Credito = Cmb_Tipo_Nota.Text.ToUpper();
                Rs_Notas_Credito.P_Tipo_Moneda = Cmb_Tipo_Moneda.Text.ToUpper();
                Rs_Notas_Credito.P_Tipo_Cambio = Tipo_Cambio.ToString();
                Rs_Notas_Credito.P_Comentarios = Txt_Comentarios.Text.ToUpper();

                if (Tipo == "DEVOLUCION")
                {
                    if (Dt_Notas_Credito_Detalles.Rows.Count > 0)
                    {
                        Rs_Notas_Credito.P_No_Cuenta = Dt_Notas_Credito_Detalles.Rows[0]["NO_CUENTA"].ToString();
                        Rs_Notas_Credito.P_Tipo_Servicio = Dt_Notas_Credito_Detalles.Rows[0]["TIPO_SERVICIO"].ToString();
                        Rs_Notas_Credito.P_Bimestre_Pagado = Dt_Notas_Credito_Detalles.Rows[0]["BIMESTRE_PAGADO"].ToString();
                    }
                    else
                    {
                        Rs_Notas_Credito.P_No_Cuenta = "";
                        Rs_Notas_Credito.P_Tipo_Servicio = "";
                        Rs_Notas_Credito.P_Bimestre_Pagado = "";
                    }
                }
                else
                {
                    Rs_Notas_Credito.P_No_Cuenta = "";
                    Rs_Notas_Credito.P_Tipo_Servicio = "";
                    Rs_Notas_Credito.P_Bimestre_Pagado = "";
                }

                // Guarda los totales
                Rs_Notas_Credito.P_Subtotal = Regex.Replace(Txt_Subtotal.Text, @"[$,]", "");
                Rs_Notas_Credito.P_Iva = Regex.Replace(Txt_Iva.Text, @"[$,]", "");
                Rs_Notas_Credito.P_Total = Regex.Replace(Txt_Total.Text, @"[$,]", "");

                // Datos del cliente.
                Rs_Notas_Credito.P_Razon_Social = Txt_Cliente.Text.ToUpper();
                Rs_Notas_Credito.P_Rfc = Txt_Rfc.Text.ToUpper();
                Rs_Notas_Credito.P_Calle = Txt_Calle.Text.ToUpper();
                Rs_Notas_Credito.P_Numero_Interior = Txt_Num_Int.Text;
                Rs_Notas_Credito.P_Numero_Exterior = Txt_Num_Ext.Text;
                Rs_Notas_Credito.P_Colonia = Txt_Colonia.Text.ToUpper();
                Rs_Notas_Credito.P_Cp = Txt_Codigo_Postal.Text;
                Rs_Notas_Credito.P_Localidad = Txt_Localidad.Text.ToUpper();
                Rs_Notas_Credito.P_Ciudad = Txt_Ciudad.Text.ToUpper();
                Rs_Notas_Credito.P_Estado = Txt_Estado.Text.ToUpper();
                Rs_Notas_Credito.P_Pais = Txt_Pais.Text.ToUpper();

                // Datos de pago
                Rs_Notas_Credito.P_Estatus = "ACTIVA";
                Rs_Notas_Credito.P_Cancelada = "NO";

                DataRow Dr_Impuestos = Dt_Impuestos.NewRow();
                if (Double.Parse((String.IsNullOrEmpty(Rs_Notas_Credito.P_Iva) ? "0" : Rs_Notas_Credito.P_Iva)) > 0)
                {
                    Dr_Impuestos["IMPUESTO"] = "IVA";
                    //****traer impuesto de la partida****///
                    Dr_Impuestos["TASA"] = Dt_Parametros_Facturacion.Rows[0]["Porcentaje_IVA"].ToString();//"16.00";
                    Dr_Impuestos["IMPORTE"] = Rs_Notas_Credito.P_Iva;
                }
                else
                {
                    Dr_Impuestos["IMPUESTO"] = "IVA";
                    Dr_Impuestos["TASA"] = "0";
                    Dr_Impuestos["IMPORTE"] = "0";
                }
                Dt_Impuestos.Rows.Add(Dr_Impuestos);

                // Lee los parametros de facturacion
                if (Dt_Parametros_Facturacion != null)
                {
                    if (Dt_Parametros_Facturacion.Rows.Count > 0)
                    {
                        Regimen_Fiscal = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Regimen_Fiscal].ToString().ToUpper();
                        Rs_Notas_Credito.P_Regimen_Fiscal = Regimen_Fiscal;
                        Expedida_En = (Dt_Empresa.Rows[0]["Ciudad"].ToString() + ", " + Dt_Empresa.Rows[0]["Estado"].ToString()).ToUpper();
                        Timbre_Version = "3.2";
                        Password_Llave = Cls_Seguridad.Desencriptar(Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Password_Llave].ToString());

                        // Ruta de los archivos
                        Ruta_Xml = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Xml].ToString().Replace(@"\", "/") + "NCRED_" + Rs_Notas_Credito.P_Serie + "_" + Rs_Notas_Credito.P_No_Nota_Credito + ".xml";
                        Ruta_Qr = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Xml].ToString().Replace(@"\", "/") + "NCRED_" + Rs_Notas_Credito.P_Serie + "_" + Rs_Notas_Credito.P_No_Nota_Credito + ".png";
                        Ruta_Cert = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Certificado].ToString().Replace(@"\", "/");
                        Ruta_Key = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Llave].ToString().Replace(@"\", "/");
                        Ruta_Pdf = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Pdf].ToString().Replace(@"\", "/") + "NCRED_" + Rs_Notas_Credito.P_Serie + "_" + Rs_Notas_Credito.P_No_Nota_Credito + ".pdf";

                        // Complementa la factura.
                        Rs_Notas_Credito.P_Timbre_Version = Timbre_Version;

                        // Cadena_Original = Cls_Timbrado.Genera_Cadena_Original(Negocio, Expedida_En, Regimen_Fiscal, Dt_Datos_Fiscales, Dt_Detalles_Pago, Dt_Impuestos);

                        Cadena_Original = Cls_Timbrado.Genera_Cadena_Original(Rs_Notas_Credito, Expedida_En, Regimen_Fiscal, Dt_Empresa, Dt_Notas_Credito_Detalles, Dt_Impuestos);
                        Cadena_UTF = Cls_Timbrado.Valida_Caracteres_UTF(Cadena_Original);
                        Cadena_Sello = Cls_Timbrado.Genera_Sello(Cadena_UTF, Server.MapPath("../" + Ruta_Key), Password_Llave);
                        Rs_Notas_Credito.P_No_Certificado = Cls_Timbrado.Consulta_Serie_Certificado(Server.MapPath("../" + Ruta_Cert));
                        Rs_Notas_Credito.P_Certificado = Cls_Timbrado.Consulta_Certificado(Server.MapPath("../" + Ruta_Cert));
                        Rs_Notas_Credito.P_Timbre_Sello_Cfd = Cadena_Sello;




                        if (Tipo.Equals("DEVOLUCION"))
                        {
                            Dt_Notas_Credito_Detalles_Alta = Dt_Notas_Credito_Detalles.Copy();
                        }
                        else if (Tipo.Equals("DESCUENTO"))
                        {
                            Dt_Notas_Credito_Detalles_Alta = Construir_Alta_Detalles_Descuento(Dt_Notas_Credito_Detalles);
                        }
                        else
                        {
                            Dt_Notas_Credito_Detalles_Alta = Construir_Alta_Detalles();
                        }


                        // abrimos una conexion--------------------------------
                        Obj_Conexion.Open();
                        //-------- creamos una transacción
                        Obj_Transaccion = Obj_Conexion.BeginTransaction();
                        // registramos la Nota_Credito

                        Se_Realizo_El_Registro_Factuacion = Rs_Notas_Credito.Alta_Nota_Credito(Dt_Notas_Credito_Detalles_Alta, Obj_Transaccion, Obj_Conexion);

                        //---------------------------------------
                        if (Se_Realizo_El_Registro_Factuacion)
                        {


                            DataTable Dt_Facturas_Realizadas = new DataTable();
                            Double Saldo_Factura = 0.0;
                            Double Abono_Factura = 0.0;
                            if (Tipo == "DEVOLUCION")
                            {
                                Rs_Datos_Fiscales.P_No_Factura = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[0].ToString();
                                Rs_Datos_Fiscales.P_Serie = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[1].ToString();
                            }
                            else
                            {
                                Rs_Datos_Fiscales.P_No_Factura = Dt_Notas_Credito_Detalles_Alta.Rows[0]["No_Factura"].ToString();
                                Rs_Datos_Fiscales.P_Serie = Dt_Notas_Credito_Detalles_Alta.Rows[0]["Serie_Factura"].ToString();
                            }
                            Dt_Facturas_Realizadas = Rs_Datos_Fiscales.Consultar_Factura(Obj_Transaccion, Obj_Conexion);

                            Saldo_Factura = Convert.ToDouble(Dt_Facturas_Realizadas.Rows[0]["SALDO"]);
                            Abono_Factura = Convert.ToDouble(Dt_Facturas_Realizadas.Rows[0]["ABONO"]);

                            Rs_Datos_Fiscales.P_Abono = Convert.ToString(Convert.ToDouble(Rs_Notas_Credito.P_Total) + Abono_Factura);
                            Rs_Datos_Fiscales.P_Saldo = Convert.ToString(Saldo_Factura - Convert.ToDouble(Rs_Notas_Credito.P_Total));

                            Rs_Datos_Fiscales.Modificar_Factura(Obj_Transaccion, Obj_Conexion);

                            // Actualiza el estatus de la serie
                            Actualiza_Estatus_Serie(Rs_Notas_Credito.P_Serie, Rs_Notas_Credito.P_No_Nota_Credito, false, Obj_Transaccion, Obj_Conexion);

                            if (Tipo == "DEVOLUCION")
                            {
                                for (int Indice_Pago = 0; Indice_Pago < Dt_Notas_Credito_Detalles_Alta.Rows.Count; Indice_Pago++)
                                {
                                    for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Notas_Credito_Detalles_Alta.Rows.Count; Indice_Pago_Siguiente++)
                                    {
                                        if (Dt_Notas_Credito_Detalles_Alta.Rows[Indice_Pago]["NO_RECIBO"].ToString().Trim().Equals(Dt_Notas_Credito_Detalles_Alta.Rows[Indice_Pago_Siguiente]["NO_RECIBO"].ToString().Trim()))
                                        {
                                            Dt_Notas_Credito_Detalles_Alta.Rows.RemoveAt(Indice_Pago_Siguiente);
                                            Indice_Pago_Siguiente--;
                                        }
                                    }
                                }

                                Rs_Datos_Fiscales.P_Dt_No_Recibos = Dt_Notas_Credito_Detalles_Alta;
                                Rs_Datos_Fiscales.Actualizar_No_Factura_NC(Obj_Transaccion, Obj_Conexion);
                            }

                            // Crea el documento XML
                            Xml_Documento = Crea_Xml_CFDI(ref Rs_Notas_Credito, Dt_Notas_Credito_Detalles, Dt_Empresa, Dt_Impuestos, Dt_Parametros_Facturacion);


                            // Genera el codigo bidimensional
                            Codigo_Bidimensional = "?re=" + Dt_Empresa.Rows[0]["RFC"].ToString() + "&rr=" + Rs_Notas_Credito.P_Rfc
                                + "&tt=" + String.Format("{0:0000000000.000000}", Double.Parse(Rs_Notas_Credito.P_Total)) + "&id=" + Rs_Notas_Credito.P_Timbre_Uuid;
                            Rs_Notas_Credito.P_Ruta_Codigo_Bd = Ruta_Qr;

                            Rs_Notas_Credito.Actualizar_Timbre_Nota_Credito(Obj_Transaccion, Obj_Conexion);
                            Obj_Transaccion.Commit();

                            // Configura la pantalla por default
                            Session["Dt_Partidas_Devolver"] = null;
                            Configuracion_Formulario(true);
                            Estado_Formulario("Inicial");
                            // Cls_Apl_Bitacora_Negocio.Alta_Bitacora(Cls_Sessiones.Empleado_ID, "Guardar_Nota_Credito", "Frm_Ope_Notas_Credito.aspx", Id_Creado, "Evento de dar de alta una nota de credito");

                            // Guarda el documento XML.
                            Xml_Documento.Save(Server.MapPath("../" + Ruta_Xml));
                            Xml_Documento = null;

                            // Genera y guarda el codigo QR.
                            Cls_Timbrado.Generar_Codigo_QR(Codigo_Bidimensional, Server.MapPath("../" + Ruta_Qr), "");

                            // Crea y guarda el PDF
                            Tipo_Comprobante = "EGRESO";
                            Ruta_Imagen_Empresa[0] = "../" + Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura"].ToString().Replace(@"\", "/");
                            Ruta_Imagen_Empresa[1] = "../" + Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura_2"].ToString().Replace(@"\", "/");
                            Ruta_Imagen_Empresa[2] = "../" + Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura_3"].ToString().Replace(@"\", "/");
                            Ruta_Imagen_Empresa[3] = "../" + Dt_Parametros_Facturacion.Rows[0]["Imagen_Factura_4"].ToString().Replace(@"\", "/");
                            Ds_Factura_Pdf = Crea_Pdf(Rs_Notas_Credito, Regimen_Fiscal, Ruta_Imagen_Empresa, Tipo_Comprobante);
                            Cls_Metodos_Generales.Generar_Reporte(ref Ds_Factura_Pdf, "../../Rpt/Nota_Credito/", "Rpt_Ope_Nota_Credito.rpt", Ruta_Pdf, "Notas de Cr&eacute;dito", this, this.GetType());

                            // Envia el correo cliente                        
                            if (!String.IsNullOrEmpty(Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Envio_Automatico_Correo].ToString()))
                            {
                                // Forma el cuerpo del mensaje
                                Mensaje = "<font size='5'> " +
                                    "<b>Envi&oacute; de Nota de Cr&eacute;dito Autom&aacute;tico</b>" +
                                "</font>" +
                                "<font size='4'>" +
                                    "<p> " + Dt_Empresa.Rows[0]["Nombre_Corto"] + " acaba de emitir la nota de cr&eacute;dito " + Rs_Notas_Credito.P_Serie + " " + Rs_Notas_Credito.P_No_Nota_Credito + ", para su empresa. </p>" +
                                "</font>";
                                Ruta_Xml = Server.MapPath("../" + Ruta_Xml);
                                Ruta_Pdf = Server.MapPath("../" + Ruta_Pdf);

                                Envia = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Envio_Automatico_Correo].ToString();
                                if (Envia.Equals("SI"))
                                {
                                    try
                                    {
                                        //Cls_Enviar_Correo.Enviar_Correo_Archivos(new String[] { Ruta_Pdf, Ruta_Xml }, Mensaje, Rs_Notas_Credito.P_Email, Dt_Parametros_Facturacion.Rows[0][Apl_Cat_Parametros_Impuestos.Campo_Email_Origen_Correos].ToString(), "Envio de Nota de Credito");
                                        Envio_Correo = true;
                                    }
                                    catch (Exception ExC)
                                    {
                                        Mensaje_Error_Correo = ExC.Message;
                                        Envio_Correo = false;
                                    }
                                }
                            }
                            if (Envia.Equals("SI"))
                            {
                                if (Envio_Correo)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Se dio de alta correctamente la Nota de Credito No " + Id_Creado + "');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Se dio de alta correctamente la Nota de Cr&eacute;dito No " + Id_Creado + " y NO se pudo enviar al correo del cliente. Error: " + Mensaje_Error_Correo + "');", true);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Se dio de alta correctamente la Nota de Cr&eacute;dito No " + Id_Creado + ".');", true);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("No hay registros en los par&aacute;metros de facturaci&oacute;n.");
                    }
                }
                else
                {
                    throw new Exception("No hay registros en los par&aacute;metros de facturaci&oacute;n.");
                }
            }
            catch (Exception E)
            {

                if (Obj_Transaccion != null)
                {
                    Obj_Transaccion.Rollback();

                    if (Obj_Conexion != null)
                    {
                        Obj_Conexion.Close();
                    }
                }
                Mensaje_Error(E.Message);
            }
            finally
            {
                if (Obj_Conexion != null)
                {
                    Obj_Conexion.Close();
                    Obj_Conexion = null;
                }
            }
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento del boton para cancelar o salir de la forma.
    ///PARAMETROS:
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Contains("Cancelar"))
            {
                Estado_Formulario("Inicial");
                Configuracion_Formulario(true);
                //Btn_Agregar_Factura.Style.Add("display", "block");
                Btn_Agregar_Factura.Style.Add("display", "none");
            }
            else
            {
                Response.Redirect("../../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cmb_Serie_SelectedIndexChanged
    ///DESCRIPCIÓN: Evento que escucha cuando se selecciona una serie del combo, entonces
    ///             consulta el numero consecutivo en el que se encuentra la serie.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Serie_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Series = new DataTable();
        String Serie = String.Empty;
        String Consecutivo = "0";
        int Timbre_Actual;
        int Timbre_Inicial;
        try
        {
            Mensaje_Error();
            Serie = Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString();

            Cls_Ope_Facturas_Series_Negocio Rs_Series = new Cls_Ope_Facturas_Series_Negocio();
            Rs_Series.P_Serie = Serie;
            Dt_Series = Rs_Series.Consultar_Serie();
            Timbre_Inicial = Int16.Parse(Dt_Series.Rows[0][Cat_Facturas_Series.Campo_Timbre_Inicial].ToString());
            Consecutivo = Cls_Metodos_Generales.Obtener_ID_Consecutivo(Ope_Notas_Credito.Tabla_Ope_Notas_Credito, Ope_Notas_Credito.Campo_No_Nota_Credito, Ope_Notas_Credito.Campo_Serie + "= '" + Serie + "'", 10);
            Timbre_Actual = Int16.Parse(Consecutivo);
            // Si el timbre actual es 1, consulta el timbre inicial de la serie.
            if (Timbre_Actual == 1)
            {
                Consecutivo = Cls_Metodos_Generales.Convertir_A_Formato_ID(Timbre_Inicial, 10);
            }
            Txt_No_Nota.Text = Consecutivo;
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_XML_Click
    ///DESCRIPCIÓN: Si existe el XML en el servidor lo muestra.
    ///PARAMETROS:  
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  11/Septiembre/2013 12:07 pm
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Ver_XML_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_No_Nota.Text) && !String.IsNullOrEmpty(Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString()))
        {
            try
            {
                DataTable Dt_Parametros_Facturacion = new DataTable();
                DataTable Dt_Datos_Fiscales = new DataTable();
                String Nombre_Empresa = String.Empty;
                Cls_Ope_Notas_Credito_Negocio Rs_Notas_Credito = new Cls_Ope_Notas_Credito_Negocio();
                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Datos_Fiscales = new Cls_Ope_Cor_Facturacion_Global_Negocio();

                Dt_Datos_Fiscales = Rs_Datos_Fiscales.Consultar_Datos_Fiscales();

                Nombre_Empresa = Dt_Datos_Fiscales.Rows[0]["NOMBRE_CORTO"].ToString();

                Rs_Notas_Credito.P_No_Nota_Credito = Txt_No_Nota.Text;
                Rs_Notas_Credito.P_Serie = Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString();

                // Lee los parametros de facturacion
                Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
                Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();
                if (Dt_Parametros_Facturacion != null)
                {
                    if (Dt_Parametros_Facturacion.Rows.Count > 0)
                    {
                        String Ruta_Xml = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Xml].ToString().Replace(@"\", "/") + "NCRED_" + Rs_Notas_Credito.P_Serie + "_" + Rs_Notas_Credito.P_No_Nota_Credito + ".xml";
                        Cls_Metodos_Generales.Mostrar_Archivo(Ruta_Xml, "Archivo XML", this, this.GetType());
                    }
                    else
                    {
                        throw new Exception("No hay registros en los par&aacute;metros de facturaci&oacute;n.");
                    }
                }
                else
                {
                    throw new Exception("No hay registros en los par&aacute;metros de facturaci&oacute;n.");
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Auxiliar_Cancelar_Nota_Credito_Click
    ///DESCRIPCIÓN: Cancela la nota de credito.
    ///PARAMETROS:  
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  11/Septiembre/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Auxiliar_Cancelar_Nota_Credito_Click(object sender, EventArgs e)
    {
        // Cadenas
        String Motivo_Cancelacion = Hdf_Comentarios_Cancelacion.Value;
        String Timbrado_Ambiente = String.Empty;
        String Codigo_Usuario = String.Empty;
        String Codigo_Proveedor = String.Empty;
        String Respuesta = String.Empty;

        // Datable
        DataTable Dt_Empresa = new DataTable();
        DataTable Dt_Parametros_Facturacion = new DataTable();

        // Declaracion de Documento XML
        XmlDocument Xml_Respuesta = new XmlDocument();
        XmlNode Nodo_Respuesta;

        if (!String.IsNullOrEmpty(Hfd_Codigo_UUID.Value))
        {
            if (!String.IsNullOrEmpty(Hdf_Comentarios_Cancelacion.Value))
            {
                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Datos_Fiscales = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                Dt_Empresa = Rs_Datos_Fiscales.Consultar_Datos_Fiscales();

                // Carga los datos de la empresa
                //Dt_Empresa = ((DataTable)Session["Dt_Datos_Empresa"]);

                //Carga los datos de la factura
                Cls_Ope_Notas_Credito_Negocio Rs_Notas_Credito = new Cls_Ope_Notas_Credito_Negocio();
                Rs_Notas_Credito.P_No_Nota_Credito = Txt_No_Nota.Text;
                Rs_Notas_Credito.P_Serie = Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString();
                Rs_Notas_Credito.P_Cancelada = "SI";
                Rs_Notas_Credito.P_Fecha_Cancelacion = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
                Rs_Notas_Credito.P_Estatus = "CANCELADA";
                Rs_Notas_Credito.P_Motivo_Cancelacion = Motivo_Cancelacion.ToUpper();
                Rs_Notas_Credito.P_Usuario_Cancelacion = Cls_Sessiones.Nombre_Empleado;
                try
                {
                    // Parametros del timbrado
                    Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
                    Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();
                    if (Dt_Parametros_Facturacion != null)
                    {
                        if (Dt_Parametros_Facturacion.Rows.Count > 0)
                        {
                            Timbrado_Ambiente = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ambiente_Timbrado].ToString();
                            Codigo_Usuario = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Codigo_Usuario].ToString();
                            Codigo_Proveedor = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Codigo_Usuario_Proveedor].ToString();

                            // Realiza la cancelacion de al factura
                            if (Timbrado_Ambiente.Equals("PRUEBA"))
                            {
                                Respuesta = Cls_Timbrado.Cancelar_Factura(false, Dt_Empresa.Rows[0]["RFC"].ToString(), Codigo_Proveedor, Codigo_Usuario, Hfd_Codigo_UUID.Value);
                            }
                            else
                            {
                                Respuesta = Cls_Timbrado.Cancelar_Factura(true, Dt_Empresa.Rows[0]["RFC"].ToString(), Codigo_Proveedor, Codigo_Usuario, Hfd_Codigo_UUID.Value);
                            }
                            try
                            {
                                Xml_Respuesta.LoadXml(Respuesta);
                                Nodo_Respuesta = Xml_Respuesta.SelectSingleNode("//Error");
                                if (Nodo_Respuesta == null)
                                {
                                    if (Rs_Notas_Credito.Modificar_Nota_Credito())
                                    {
                                        Actualiza_Estatus_Serie(Rs_Notas_Credito.P_Serie, Rs_Notas_Credito.P_No_Nota_Credito, true);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Se cancelo la Nota de Credito.');", true);
                                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Se cancelo la Nota de Credito.', 'info');", true);
                                        Estado_Formulario("Inicial");
                                    }
                                }
                                else
                                {
                                    Nodo_Respuesta = Xml_Respuesta.SelectSingleNode("//Error");

                                    if (Nodo_Respuesta != null)
                                    {
                                        throw new Exception("ErrorTimbrado  Error: " + Nodo_Respuesta.Attributes[0].InnerText + " " + Nodo_Respuesta.Attributes[1].InnerText);
                                    }
                                    else
                                    {
                                        throw new Exception("ErrorTimbrado Error: El Servidor de Timbrado no respondio");
                                    }
                                }
                            }
                            catch (Exception Ex_Respuesta)
                            {
                                throw new Exception(Ex_Respuesta.Message);
                            }
                        }
                        else
                        {
                            throw new Exception("No se encontraron los par&aacute;metros del timbrado.");
                        }
                    }
                    else
                    {
                        throw new Exception("No se encontraron los par&aacute;metros del timbrado.");
                    }
                }
                catch (Exception E)
                {
                    Mensaje_Error(E.Message);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Favor de proporcionar el motivo de la Notas de Cr&eacute;dito.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Favor de proporcionar el motivo de la Notas de Cr&eacute;dito.', 'info');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('La nota de credito que desea cancelar <br/> no tiene folio de timbrado.');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','La nota de credito que desea cancelar <br/> no tiene folio de timbrado.', 'info');", true);
        }
    }

    protected void Btn_Enviar_Nota_Credito_Click(object sender, EventArgs e)
    {

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_PDF_Click
    ///DESCRIPCIÓN: Exporta la nota de crédito a PDF.
    ///PARAMETROS:  
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  29/Agosto/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Exportar_PDF_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_No_Nota.Text) && !String.IsNullOrEmpty(Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString()))
        {
            try
            {
                DataSet Ds_Nota_Credito_Pdf = new DataSet();
                DataTable Dt_Parametros_Facturacion = new DataTable();
                DataTable Dt_Datos_Fiscales = new DataTable();
                String Nombre_Empresa = String.Empty;
                Cls_Ope_Notas_Credito_Negocio Rs_Nota_Credito = new Cls_Ope_Notas_Credito_Negocio();
                Rs_Nota_Credito.P_No_Nota_Credito = Txt_No_Nota.Text;
                Rs_Nota_Credito.P_Serie = Cmb_Serie.Items[Cmb_Serie.SelectedIndex].ToString();

                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Datos_Fiscales = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                Rs_Datos_Fiscales.Empresa_ID = "00001";
                Dt_Datos_Fiscales = Rs_Datos_Fiscales.Consultar_Datos_Fiscales();
                Nombre_Empresa = Dt_Datos_Fiscales.Rows[0]["NOMBRE_CORTO"].ToString();

                // Lee los parametros de facturacion
                Cls_Apl_Parametros_Facturacion_Negocio Rs_Parametros_Facturacion = new Cls_Apl_Parametros_Facturacion_Negocio();
                Dt_Parametros_Facturacion = Rs_Parametros_Facturacion.Consultar_Parametros();
                if (Dt_Parametros_Facturacion != null)
                {
                    if (Dt_Parametros_Facturacion.Rows.Count > 0)
                    {
                        String Regimen_Fiscal = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Regimen_Fiscal].ToString().ToUpper();
                        String Tipo_Comprobante = "EGRESO";
                        String Ruta_Pdf = Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Ruta_Pdf].ToString().Replace(@"\", "/") + "NCRED_" + Rs_Nota_Credito.P_Serie + "_" + Rs_Nota_Credito.P_No_Nota_Credito + ".pdf";
                        String[] Ruta_Imagen_Empresa = new string[4];
                        Ruta_Imagen_Empresa[0] = "../" + Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Imagen_Factura].ToString().Replace(@"\", "/");
                        Ruta_Imagen_Empresa[1] = "../" + Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Imagen_Factura_2].ToString().Replace(@"\", "/");
                        Ruta_Imagen_Empresa[2] = "../" + Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Imagen_Factura_3].ToString().Replace(@"\", "/");
                        Ruta_Imagen_Empresa[3] = "../" + Dt_Parametros_Facturacion.Rows[0][Apl_Parametros_Facturacion.Campo_Imagen_Factura_4].ToString().Replace(@"\", "/");

                        Ds_Nota_Credito_Pdf = Crea_Pdf(Rs_Nota_Credito, Regimen_Fiscal, Ruta_Imagen_Empresa, Tipo_Comprobante);
                        Cls_Metodos_Generales.Generar_Reporte(ref Ds_Nota_Credito_Pdf, "../../Rpt/Nota_Credito/", "Rpt_Ope_Nota_Credito.rpt", Ruta_Pdf, "Notas de Crédito", this, this.GetType());
                    }
                    else
                    {
                        throw new Exception("No hay registros en los par&aacute;metros de facturaci&oacute;n.");
                    }
                }
                else
                {
                    throw new Exception("No hay registros en los par&aacute;metros de facturaci&oacute;n.");
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }
    }

    #endregion

    #region Grid

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Clientes_SelectedIndexChanged
    ///DESCRIPCIÓN: Funcion que muestra el cliente seleccionado del grid.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Clientes_SelectedIndexChanged(object sender, EventArgs e)
    {
        String Cliente_Id = String.Empty;

        if (Grid_Clientes.SelectedIndex > -1)
        {
            Cliente_Id = Grid_Clientes.DataKeys[Grid_Clientes.SelectedRow.RowIndex].Values["CLIENTE_ID"].ToString().Trim();
            if (String.IsNullOrEmpty(Cliente_Id))
            {
                Cliente_Id = Grid_Clientes.Rows[Grid_Clientes.SelectedIndex].Cells[2].Text;
                Consulta_Datos_Cliente(Cliente_Id, false);
            }
            else
            {
                Consulta_Datos_Cliente(Cliente_Id, true);
            }
            Llenar_Grid_Facturas(0);
            if (Grid_Facturas.Rows.Count == 1)
            {
                Txt_Buscar_No_Factura.Enabled = false;
                Btn_Buscar_Factura.Enabled = false;
            }
            else
            {
                Txt_Buscar_No_Factura.Enabled = true;
                Btn_Buscar_Factura.Enabled = true;
            }
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Clientes_PageIndexChanging
    ///DESCRIPCIÓN: Cambia de pagina el grid.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Clientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Clientes.SelectedIndex = -1;
        Llenar_Grid_Clientes(e.NewPageIndex);
        Mdp_Clientes.Show();
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Facturas_SelectedIndexChanged
    ///DESCRIPCIÓN: Funcion que muestra las partidas de la factura si el tipo de nota
    ///             es devolucion, en otro caso, solo guarda el id del cliente en
    ///             un campo oculto.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Facturas_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Grid_Facturas.SelectedIndex > -1)
        {
            // Si se selecciona una factura y el tipo de nota es por devolucion
            // muestra las partidas de la factura seleccionada.
            if (Cmb_Tipo_Nota.SelectedValue.ToString().Equals("DEVOLUCION"))
            {
                Cls_Ope_Facturas_Detalles_Negocio Rs_Detalle_Factura = new Cls_Ope_Facturas_Detalles_Negocio();
                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Factura = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                Rs_Detalle_Factura.P_No_Factura = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[0].ToString();
                Rs_Detalle_Factura.P_Serie = Grid_Facturas.DataKeys[Grid_Facturas.SelectedRow.RowIndex].Values[1].ToString();

                DataTable Dt_Detalles_Factura = new DataTable();
                Dt_Detalles_Factura = Rs_Detalle_Factura.Consultar_Factura();

                Dt_Detalles_Factura.Columns.Add("NO_RECIBO", typeof(String));
                Dt_Detalles_Factura.Columns.Add("NO_CUENTA", typeof(String));
                Dt_Detalles_Factura.Columns.Add("TIPO_SERVICIO", typeof(String));
                Dt_Detalles_Factura.Columns.Add("BIMESTRE_PAGADO", typeof(String));

                DataTable Dt_No_Recibo = new DataTable();

                if (Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[9].Text.Trim() != "&nbsp;")
                {
                    Rs_Factura.Codigo_Barras = Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[9].Text.Trim();
                    Dt_No_Recibo = Rs_Factura.Consultar_Recibos_Facturados();
                    if (Dt_No_Recibo.Rows.Count > 0)
                    {
                        for (int Indice_Pago = 0; Indice_Pago < Dt_Detalles_Factura.Rows.Count; Indice_Pago++)
                        {
                            Dt_Detalles_Factura.Rows[Indice_Pago]["NO_RECIBO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                            //Dt_Detalles_Factura.Rows[Indice_Pago]["NO_CUENTA"] = Dt_No_Recibo.Rows[0]["NO_CUENTA"].ToString();
                            Dt_Detalles_Factura.Rows[Indice_Pago]["TIPO_SERVICIO"] = Dt_No_Recibo.Rows[0]["TIPO_SERVICIO"].ToString();
                            Dt_Detalles_Factura.Rows[Indice_Pago]["BIMESTRE_PAGADO"] = Dt_No_Recibo.Rows[0]["BIMESTRE_PAGADO"].ToString();
                        }
                    }
                }
                else
                {
                    String Codigo_Barras_Recibo = String.Empty;

                    for (int Indice_Pago = 0; Indice_Pago < Dt_Detalles_Factura.Rows.Count; Indice_Pago++)
                    {
                        //if ()

                        Rs_Factura.Codigo_Barras = Dt_Detalles_Factura.Rows[Indice_Pago]["FOLIO_PAGO"].ToString();

                        if (Codigo_Barras_Recibo != Rs_Factura.Codigo_Barras)
                        {

                            Dt_No_Recibo = Rs_Factura.Consultar_Recibos_Facturados();
                            Dt_Detalles_Factura.Rows[Indice_Pago]["NO_RECIBO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                            Dt_Detalles_Factura.Rows[Indice_Pago]["NO_CUENTA"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                            Dt_Detalles_Factura.Rows[Indice_Pago]["TIPO_SERVICIO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                            Dt_Detalles_Factura.Rows[Indice_Pago]["BIMESTRE_PAGADO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                            Codigo_Barras_Recibo = Dt_Detalles_Factura.Rows[Indice_Pago]["FOLIO_PAGO"].ToString();
                        }
                        else
                        {
                            Dt_Detalles_Factura.Rows[Indice_Pago]["NO_RECIBO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                        }
                    }
                }

                Grid_Partidas_Factura.DataSource = Dt_Detalles_Factura;
                Grid_Partidas_Factura.DataBind();
                Session["Dt_Partidas_Factura"] = Dt_Detalles_Factura;

                if (Dt_Detalles_Factura.Rows.Count > 0)
                {
                    Btn_Agregar_Partida.Style.Add("display", "block");
                }
                else
                {
                    Btn_Agregar_Partida.Style.Add("display", "none");
                }
            }
            else
            {
                Hfd_Cliente.Value = HttpUtility.HtmlDecode(Grid_Facturas.Rows[Grid_Facturas.SelectedIndex].Cells[3].Text);
            }

            if (Grid_Facturas.Rows.Count > 0)
            {
                if (!Cmb_Tipo_Nota.SelectedValue.ToString().Equals("DEVOLUCION"))
                {
                    Btn_Agregar_Factura.Style.Add("display", "block");
                }
                else
                {
                    Btn_Agregar_Factura.Style.Add("display", "none");
                }
            }
            else
            {
                Btn_Agregar_Factura.Style.Add("display", "none");
            }
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Facturas_PageIndexChanging
    ///DESCRIPCIÓN: Cambia de pagina el grid.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Facturas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Facturas.SelectedIndex = -1;
        Llenar_Grid_Facturas(e.NewPageIndex);
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Facturas_Seleccionadas_SelectedIndexChanged
    ///DESCRIPCIÓN: Se elimina la factura seleccionada del grid.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Facturas_Seleccionadas_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Indice = 0;
        try
        {
            Actualizar_Facturas_Seleccionadas();
            DataTable Dt_Auxiliar = new DataTable();
            Dt_Auxiliar = ((DataTable)Session["Dt_Factura_Seleccionada"]);
            if (Dt_Auxiliar != null && Dt_Auxiliar.Rows.Count > 0)
            {
                Indice = Grid_Facturas_Seleccionadas.SelectedIndex;
                Dt_Auxiliar.Rows.RemoveAt(Indice);
                Session["Dt_Partida"] = Dt_Auxiliar;
                Rellena_Grid();

                // Si ya no hay facturas seleccionadas limpia los campos del cliente.
                if (Dt_Auxiliar.Rows.Count == 0)
                {
                    Consulta_Datos_Cliente(String.Empty, true);
                    Txt_Busqueda_Factura.Enabled = true;
                    Cmb_Tipo_Moneda.Enabled = true;
                    Txt_Tipo_Cambio.Enabled = true;
                    Cmb_Tipo_Nota.Enabled = true;
                }
                Llenar_Grid_Facturas(0);
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Factura_SelectedIndexChanged
    ///DESCRIPCIÓN: Cuando se selecciona una partida de la factura se guarda en
    ///             un campo oculto la cantidad de dicha partida.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Partidas_Factura_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Partidas_Factura.SelectedIndex > -1)
            {
                Hfd_Cantidad_Seleccionada.Value = Grid_Partidas_Factura.Rows[Grid_Partidas_Factura.SelectedIndex].Cells[1].Text;
            }
            else
            {
                Hfd_Cantidad_Seleccionada.Value = "-1";
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Factura_PageIndexChanging
    ///DESCRIPCIÓN: Cambia de pagina el grid.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Partidas_Factura_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Partidas_Factura.SelectedIndex = -1;
        Llenar_Grid_Factura_Detalles(e.NewPageIndex);
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Partidas_Devolver_SelectedIndexChanged
    ///DESCRIPCIÓN: Se elimina la partida seleccionada del grid.
    ///PARAMETROS:  
    ///CREO:        Luis A. Salas
    ///FECHA_CREO:  18/Julio/2013
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Partidas_Devolver_SelectedIndexChanged(object sender, EventArgs e)
    {
        Double SubTotal = 0.0;
        Double Total = 0.0;
        Double Importe_Concepto = 0.0;

        try
        {
            int Indice = Grid_Partidas_Devolver.SelectedIndex;
            if (Indice > -1)
            {
                DataTable Dt_Partidas_Devolver = new DataTable();
                DataTable Dt_Partidas = new DataTable();

                //Importe_Concepto = Convert.ToDouble(Grid_Partidas_Devolver.Rows[Grid_Partidas_Devolver.SelectedIndex].Cells[8].Text);
                SubTotal = Convert.ToDouble(Txt_Subtotal.Text);


                Dt_Partidas_Devolver = ((DataTable)Session["Dt_Partidas_Devolver"]);


                for (int Indice_Importe = 0; Indice_Importe < Dt_Partidas_Devolver.Rows.Count; Indice_Importe++)
                {
                    if (Dt_Partidas_Devolver.Rows[Indice_Importe]["NO_RECIBO"].ToString().Trim().Equals(Grid_Partidas_Devolver.Rows[Grid_Partidas_Devolver.SelectedIndex].Cells[1].Text))
                    {
                        Importe_Concepto = Importe_Concepto + Convert.ToDouble(Dt_Partidas_Devolver.Rows[Indice_Importe]["TOTAL_DETALLE"].ToString());
                    }
                }

                Total = SubTotal - Importe_Concepto;
                //Txt_Total.Text = Convert.ToString(Math.Round(Total));
                //Txt_Subtotal.Text = Convert.ToString(Math.Round(Total));
                Txt_Total.Text = Math.Round(Total).ToString("N2");
                Txt_Subtotal.Text = Math.Round(Total).ToString("N2");

                if (Dt_Partidas_Devolver != null && Dt_Partidas_Devolver.Rows.Count > 0)
                {

                    for (int Indice_Pago = 0; Indice_Pago < Dt_Partidas_Devolver.Rows.Count; Indice_Pago++)
                    {
                        for (int Indice_Pago_Siguiente = Indice_Pago + 1; Indice_Pago_Siguiente < Dt_Partidas_Devolver.Rows.Count; Indice_Pago_Siguiente++)
                        {
                            if (Dt_Partidas_Devolver.Rows[Indice_Pago]["NO_RECIBO"].ToString().Trim().Equals(Grid_Partidas_Devolver.Rows[Grid_Partidas_Devolver.SelectedIndex].Cells[1].Text))
                            {

                                Dt_Partidas_Devolver.Rows.RemoveAt(Indice_Pago_Siguiente);
                                Indice_Pago_Siguiente--;
                            }
                        }
                    }

                    for (int Indice_Fila = 0; Indice_Fila < Dt_Partidas_Devolver.Rows.Count; Indice_Fila++)
                    {
                        if (Dt_Partidas_Devolver.Rows[Indice_Fila]["NO_RECIBO"].ToString().Trim().Equals(Grid_Partidas_Devolver.Rows[Grid_Partidas_Devolver.SelectedIndex].Cells[1].Text))
                        {
                            Dt_Partidas_Devolver.Rows.RemoveAt(Indice_Fila);
                        }
                    }

                    Session["Dt_Partidas_Devolver"] = Dt_Partidas_Devolver;

                    // Si ya no hay facturas seleccionadas limpia los campos del cliente.
                    if (Dt_Partidas_Devolver.Rows.Count == 0)
                    {
                        Consulta_Datos_Cliente(String.Empty, true);
                        Txt_Busqueda_Factura.Enabled = true;
                        Cmb_Tipo_Moneda.Enabled = true;
                        Txt_Tipo_Cambio.Enabled = true;
                        Cmb_Tipo_Nota.Enabled = true;
                        Pnl_Busqueda_Facturas.Enabled = true;
                        Dt_Partidas_Devolver = new DataTable();
                    }
                    Grid_Partidas_Devolver.DataSource = Dt_Partidas_Devolver;
                    Grid_Partidas_Devolver.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Grid_Notas_Credito_SelectedIndexChanged
    ///DESCRIPCIÓN: Evento del grid de Notas de Credito para seleccionar y mostrar los datos
    ///             de la Nota de credito.
    ///PARAMETROS:  
    ///CREO:        Miguel Angel Alvarado Enriquez
    ///FECHA_CREO:  28/Agosto/2013 6:24 pm
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Notas_Credito_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Notas_Credito;
        DataTable Dt_Detalle_Notas_Credito;
        DataTable Dt_Facturas;
        DataTable Dt_Detalle_Factura;
        String No_Notas_Credito = String.Empty;
        String No_Factura = String.Empty;
        String Serie_Notas_Credito = String.Empty;
        String Forma_Pago = String.Empty;
        String Estatus = String.Empty;

        if (Grid_Notas_Credito.SelectedIndex > -1)
        {
            Configuracion_Formulario(true);
            Estado_Formulario("Inicial");

            Cls_Ope_Notas_Credito_Negocio Rs_Notas_Credito = new Cls_Ope_Notas_Credito_Negocio();
            No_Notas_Credito = Grid_Notas_Credito.DataKeys[Grid_Notas_Credito.SelectedRow.RowIndex].Values[0].ToString();
            Serie_Notas_Credito = Grid_Notas_Credito.DataKeys[Grid_Notas_Credito.SelectedRow.RowIndex].Values[1].ToString();
            Rs_Notas_Credito.P_No_Nota_Credito = Grid_Notas_Credito.DataKeys[Grid_Notas_Credito.SelectedRow.RowIndex].Values[0].ToString();
            Rs_Notas_Credito.P_Serie = Grid_Notas_Credito.DataKeys[Grid_Notas_Credito.SelectedRow.RowIndex].Values[1].ToString();
            Dt_Notas_Credito = Rs_Notas_Credito.Consultar_Nota_Credito();

            Hfd_Codigo_UUID.Value = HttpUtility.HtmlDecode(Dt_Notas_Credito.Rows[0]["TIMBRE_UUID"].ToString());
            Cmb_Serie.SelectedItem.Text = HttpUtility.HtmlDecode(Dt_Notas_Credito.Rows[0]["SERIE"].ToString());
            Cmb_Tipo_Nota.SelectedIndex = Cmb_Tipo_Nota.Items.IndexOf(Cmb_Tipo_Nota.Items.FindByValue(HttpUtility.HtmlDecode(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Tipo_Nota_Credito].ToString())));
            Cmb_Tipo_Moneda.SelectedIndex = Cmb_Tipo_Moneda.Items.IndexOf(Cmb_Tipo_Moneda.Items.FindByValue(HttpUtility.HtmlDecode(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Tipo_Moneda].ToString())));
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByText(HttpUtility.HtmlDecode(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Estatus].ToString())));
            Txt_No_Nota.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_No_Nota_Credito].ToString();
            Txt_Comentarios.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Comentarios].ToString();
            Txt_Tipo_Cambio.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Tipo_Cambio].ToString();
            Txt_Fecha_Emision.Text = Convert.ToDateTime(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Fecha_Emision]).ToString("dd/MMM/yyyy");

            Hfd_Cliente_Id.Value = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Cliente_Id].ToString();
            Hfd_Correo_Cliente.Value = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Email].ToString();
            Txt_Cliente.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Razon_Social].ToString();
            Txt_Rfc.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Rfc].ToString();
            Txt_Calle.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Calle].ToString();
            Txt_Num_Int.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Numero_Interior].ToString();
            Txt_Num_Ext.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Numero_Exterior].ToString();
            Txt_Colonia.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Colonia].ToString();
            Txt_Ciudad.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Ciudad].ToString();
            Txt_Localidad.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Localidad].ToString();
            Txt_Codigo_Postal.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Cp].ToString();
            Txt_Estado.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Estado].ToString();
            Txt_Pais.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Pais].ToString();

            Estatus = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Cancelada].ToString();
            if (Estatus.Equals("NO"))
            {
                //if (Forma_Pago.Equals("CONTADO"))
                //{
                Btn_Cancelar_Nota.Visible = true;
                //}
                //else
                //{
                //    Double Abono = Double.Parse(Dt_Facturas.Rows[0][Ope_Facturas.Campo_Abono].ToString());
                //    if (Abono == 0)
                //    {
                //        Btn_Cancelar_Factura.Visible = true;
                //    }
                //}
            }
            // Si esta cancelada, muestra el motivo en los comentarios.
            if (Estatus.Equals("SI"))
            {
                Pnl_Comentarios.GroupingText = "Motivo de la Cancelaci&oacute;n";
                Txt_Comentarios.Text = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Motivo_Cancelacion].ToString(); ;
            }

            // Coloca los totales
            String Tipo = "DEVOLUCION";
            Tipo = Cmb_Tipo_Nota.Text.ToString();
            Panel2.Style.Add("display", "none");
            Panel3.Style.Add("display", "none");
            Panel4.Style.Add("display", "none");

            if (Tipo.Equals("DEVOLUCION"))
            {
                Panel2.Style.Add("display", "block");
                Panel3.Style.Add("display", "block");
                Pnl_Busqueda_Facturas.Style.Add("display", "block");
                Txt_Subtotal.Text = String.Format("{0:#,###0.#0}", Double.Parse(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Subtotal].ToString()));
                Txt_Iva.Text = String.Format("{0:#,###0.#0}", Double.Parse(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Iva].ToString()));
                Txt_Total.Text = String.Format("{0:#,###0.#0}", Double.Parse(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Total].ToString()));

                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Facturas = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                Rs_Facturas.P_No_Factura = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_No_Factura].ToString();
                Rs_Facturas.P_Serie = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Serie_Factura].ToString();
                Dt_Facturas = Rs_Facturas.Consultar_Factura_Productos();

                Grid_Facturas.Columns[9].Visible = true;
                Grid_Facturas.DataSource = Dt_Facturas;
                Grid_Facturas.DataBind();
                Grid_Facturas.Columns[9].Visible = false;

                Cls_Ope_Facturas_Detalles_Negocio Rs_Facturas_Detalles = new Cls_Ope_Facturas_Detalles_Negocio();
                Rs_Facturas_Detalles.P_No_Factura = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_No_Factura].ToString();
                Rs_Facturas_Detalles.P_Serie = Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Serie_Factura].ToString();

                Dt_Detalle_Factura = Rs_Facturas_Detalles.Consultar_Factura();

                DataTable Dt_No_Recibo = new DataTable();
                Rs_Facturas.Codigo_Barras = Dt_Facturas.Rows[0]["FOLIO_PAGO"].ToString();
                Dt_No_Recibo = Rs_Facturas.Consultar_Recibos_Facturados();

                Dt_Detalle_Factura.Columns.Add("NO_RECIBO", typeof(String));

                for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle_Factura.Rows.Count; Indice_Pago++)
                {
                    Dt_Detalle_Factura.Rows[Indice_Pago]["NO_RECIBO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                }

                Grid_Partidas_Factura.DataSource = Dt_Detalle_Factura;
                Grid_Partidas_Factura.DataBind();

                Dt_Detalle_Notas_Credito = Rs_Notas_Credito.Consultar_Detalle_Nota_Credito();


                Rs_Facturas.P_No_Factura = Dt_Detalle_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_No_Factura].ToString();
                Rs_Facturas.P_Serie = Dt_Detalle_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Serie_Factura].ToString();
                Dt_Facturas = Rs_Facturas.Consultar_Factura_Productos();

                //Dt_Detalle_Notas_Credito.Columns.Add("NO_RECIBO", typeof(String));

                //String No_Factura_Nota_Credito = String.Empty;

                //for (int Indice_Pago = 0; Indice_Pago < Dt_Detalle_Notas_Credito.Rows.Count; Indice_Pago++)
                //{
                //    Rs_Facturas.P_No_Factura = Dt_Detalle_Notas_Credito.Rows[Indice_Pago][Ope_Notas_Credito.Campo_No_Factura].ToString();
                //    Rs_Facturas.P_Serie = Dt_Detalle_Notas_Credito.Rows[Indice_Pago][Ope_Notas_Credito.Campo_Serie_Factura].ToString();

                //    if (No_Factura_Nota_Credito != Rs_Facturas.P_No_Factura)
                //    {

                //        Dt_Facturas = Rs_Facturas.Consultar_Factura_Productos();

                //        Rs_Facturas.Codigo_Barras = Dt_Facturas.Rows[Indice_Pago]["FOLIO_PAGO"].ToString();
                //        Dt_No_Recibo = Rs_Facturas.Consultar_Recibos_Facturados();

                //        Dt_Detalle_Notas_Credito.Rows[Indice_Pago]["NO_RECIBO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                //        No_Factura_Nota_Credito = Dt_Detalle_Notas_Credito.Rows[Indice_Pago][Ope_Notas_Credito.Campo_No_Factura].ToString();
                //    }
                //    else
                //    {
                //        Dt_Detalle_Notas_Credito.Rows[Indice_Pago]["NO_RECIBO"] = Dt_No_Recibo.Rows[0]["NO_RECIBO"].ToString();
                //    }
                //}

                Grid_Partidas_Devolver.DataSource = Dt_Detalle_Notas_Credito;
                Grid_Partidas_Devolver.DataBind();
            }
            else
            {
                Panel2.Style.Add("display", "none");
                Panel3.Style.Add("display", "none");
                Panel4.Style.Add("display", "block");
                Txt_Descuento_Facturas.Enabled = false;
                Btn_Aplicar_Descuento.Enabled = false;
                Grid_Facturas_Seleccionadas.Enabled = false;
                Pnl_Busqueda_Facturas.Style.Add("display", "none");

                Txt_Subtotal.Text = String.Format("{0:#,###0.#0}", Double.Parse(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Subtotal].ToString()));
                Txt_Iva.Text = String.Format("{0:#,###0.#0}", Double.Parse(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Iva].ToString()));
                Txt_Total.Text = String.Format("{0:#,###0.#0}", Double.Parse(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Total].ToString()));

                Dt_Detalle_Notas_Credito = Rs_Notas_Credito.Consultar_Detalle_Nota_Credito();

                DataTable Dt_Detalles_Facturas = Hacer_Tabla_Factura_Seleccionada();
                DataTable Dt_Factura = new DataTable();
                DataRow Dr;

                Cls_Ope_Cor_Facturacion_Global_Negocio Rs_Facutra = new Cls_Ope_Cor_Facturacion_Global_Negocio();
                foreach (DataRow Dr_Factura_Seleccionada in Dt_Detalle_Notas_Credito.Rows)
                {
                    Rs_Facutra.P_No_Factura = Dr_Factura_Seleccionada[Ope_Notas_Credito_Detalle.Campo_No_Factura].ToString();
                    Rs_Facutra.P_Serie = Dr_Factura_Seleccionada[Ope_Notas_Credito_Detalle.Campo_Serie_Factura].ToString();
                    Dt_Factura = Rs_Facutra.Consultar_Factura();

                    if (Dt_Factura.Rows.Count > 0)
                    {
                        Dr = Dt_Detalles_Facturas.NewRow();
                        Dr[Ope_Facturas.Campo_Serie] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Serie].ToString();
                        Dr[Ope_Facturas.Campo_No_Factura] = Dt_Factura.Rows[0][Ope_Facturas.Campo_No_Factura].ToString();
                        Dr[Ope_Facturas.Campo_Razon_Social] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Razon_Social].ToString();
                        Dr[Ope_Facturas.Campo_Tipo_Moneda] = Dt_Factura.Rows[0][Ope_Facturas.Campo_Tipo_Moneda].ToString();
                        Dr[Ope_Facturas.Campo_Total] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura.Rows[0][Ope_Facturas.Campo_Total].ToString()));
                        Dr[Ope_Facturas.Campo_Saldo] = String.Format("{0:#,###0.#0}", Convert.ToDouble(Dt_Factura.Rows[0][Ope_Facturas.Campo_Saldo].ToString()));
                        Dr["IMPORTE"] = Dr_Factura_Seleccionada[Ope_Notas_Credito_Detalle.Campo_Total_Detalle].ToString();
                        Dt_Detalles_Facturas.Rows.Add(Dr);

                    }
                }
                Grid_Facturas_Seleccionadas.DataSource = Dt_Detalles_Facturas;
                Grid_Facturas_Seleccionadas.DataBind();
                Grid_Facturas_Seleccionadas.Columns[6].Visible = false;
                Grid_Facturas_Seleccionadas.Columns[7].Visible = true;
            }

            Btn_Exportar_PDF.Visible = true;
            Btn_Enviar_Nota_Credito.Visible = false;
            //Btn_Enviar_Nota_Credito.Visible = true;

            // Si la factura tiene fecha creo xml, muestra el boton
            if (!String.IsNullOrEmpty(Dt_Notas_Credito.Rows[0][Ope_Notas_Credito.Campo_Fecha_Creo_Xml].ToString()))
            {
                Btn_Ver_XML.Visible = true;
            }
            if (Estatus.Equals("SI"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Nota de Credito Cancelada.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Cr&eacute;dito", "$.messager.alert('" + Cls_Sessiones.Mensaje_Sistema + "','Nota de Cr&eacute;dito Cancelada.', 'warning');", true);
            }
        }
    }

    protected void Grid_Notas_Credito_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Notas_Credito.SelectedIndex = -1;
        Llenar_Grid_Notas_Credito(e.NewPageIndex);
        Mdp_Notas_Credito.Show();
    }
    #endregion

    #region (Control de Accesos)

    //protected void Configuracion_Acceso(String URL_Pagina)
    //{
    //    List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
    //    DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

    //    try
    //    {
    //        //Agregamos los botones a la lista de botones de la página.
    //        Botones.Add(Btn_Nuevo);
    //        Botones.Add(Btn_Buscar);

    //        if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
    //        {
    //            if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
    //            {
    //                //Consultamos el menu de la página.
    //                Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

    //                if (Dr_Menus.Length > 0)
    //                {
    //                    //Validamos que el menu consultado corresponda a la página a validar.
    //                    if (Dr_Menus[0][Apl_Menus.Campo_Url_Link].ToString().Contains(URL_Pagina))
    //                    {
    //                        Cls_Metodos_Generales.Configuracion_Acceso_Sistema_ERP(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
    //                    }
    //                    else
    //                    {
    //                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    //                    }
    //                }
    //                else
    //                {
    //                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    //                }
    //            }
    //            else
    //            {
    //                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    //            }
    //        }
    //        else
    //        {
    //            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    //        }
    //    }
    //    catch (Exception Ex)
    //    {
    //        throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
    //    }
    //}

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Miguel Angel Alvarado Enriquez
    /// FECHA_CREO  : 26/Marzo/2013 5:54 pm
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }

    #endregion
   
    protected void Btn_Busca_Recibo_Click(object sender, ImageClickEventArgs e)
    {
        if (Grid_Facturas_Seleccionadas.Rows.Count > 0 && !String.IsNullOrEmpty(Txt_Busca_No_Recibo.Text))
        {
            DataTable Dt_Aux = new DataTable();
            Cls_Ope_Notas_Credito_Negocio Obj_Nota_Credito = new Cls_Ope_Notas_Credito_Negocio();
            Obj_Nota_Credito.P_No_Recibo = string.Format("{0:0000000000}", Convert.ToInt32(Txt_Busca_No_Recibo.Text));
            Obj_Nota_Credito.P_No_Factura = Grid_Facturas_Seleccionadas.Rows[0].Cells[1].Text;
            Dt_Aux = Obj_Nota_Credito.Consultar_Recibo();
            if (Dt_Aux.Rows.Count > 0)
            {
                if (Convert.ToDouble(Dt_Aux.Rows[0]["total_iva_cobrado"].ToString()) > 0)
                {
                    Hfd_Impuesto.Value = Dt_Aux.Rows[0]["porcentaje_iva"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Recibo encontrado, contiene IVA.');", true);
                }
                else
                {
                    Hfd_Impuesto.Value = "0";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Recibo encontrado, no contiene IVA.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('No existe el no de recibo, en la factura seleccionada.');", true);
                Txt_Busca_No_Recibo.Text = "";
            }

        }
        else if (Grid_Partidas_Factura.Rows.Count > 0 && !String.IsNullOrEmpty(Txt_Busca_No_Recibo.Text))
        {
            DataTable Dt_Partidas_Facturas = ((DataTable)Session["Dt_Partidas_Factura"]);
            Dt_Partidas_Facturas.DefaultView.RowFilter = "No_Recibo ='" + string.Format("{0:0000000000}", Convert.ToInt32(Txt_Busca_No_Recibo.Text.Trim())) + "'";
            Grid_Partidas_Factura.DataSource = Dt_Partidas_Facturas.DefaultView.ToTable();
            Grid_Partidas_Factura.DataBind();
            Session["Dt_Partidas_Factura"] = Dt_Partidas_Facturas.DefaultView.ToTable();
        }
        else
        {
            if (String.IsNullOrEmpty(Txt_Busca_No_Recibo.Text))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Debes Ingresar un número de recibo.');", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Notas de Credito", "alert('Debes Seleccionar una Factura.');", true);
            Txt_Busca_No_Recibo.Text = "";
        }
    }
}