﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Ope_Alertas_Gerentes_Negocio
    {
        public int Alerta_Gerente_ID { get; set; }
        public int No_Alerta_Roja { set; get; }
        public int Empleado_ID { set; get; }
        public char Aprobado { get; set; }
        public string Email { get; set; }

    }
}