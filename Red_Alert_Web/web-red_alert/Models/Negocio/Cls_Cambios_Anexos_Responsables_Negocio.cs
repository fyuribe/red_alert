﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Cambios_Anexos_Responsables_Negocio
    {
        public int No_Anexo { set; get; }
        public int No_Cambio { set; get; }
        public int No_Extension { set; get; }
        public string URL_Archivo { get; set; }
        public string Nombre { get; set; }
        public int Empleado_ID { set; get; }
        public int Departamento_ID { set; get; }
        public string Departamento { get; set; }
        public string Empleado { get; set; }
    }
}