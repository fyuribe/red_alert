﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_cambios_procesos.Models.Negocio
{
    public class Cls_Cambios_Anexos_Negocio
    {
        public int No_Anexo { set; get; }
        public int No_Cambio { set; get; }
        public int No_Extension { set; get; }
        public string URL_Archivo { get; set; }
        public string Nombre { get; set; }
    }
}