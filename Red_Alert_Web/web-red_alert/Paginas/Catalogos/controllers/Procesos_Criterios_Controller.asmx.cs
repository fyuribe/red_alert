﻿using datos_cambios_procesos;
using LitJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Catalogos.controllers
{
    /// <summary>
    /// Descripción breve de Procesos_Criterios_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Procesos_Criterios_Controller : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Alta(string jsonObject)
        {
          Cls_Cat_Procesos_Criterios Obj_Procesos_Criterios = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Alta registro";
                Obj_Procesos_Criterios = JsonMapper.ToObject<Cls_Cat_Procesos_Criterios>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _procesos = new Cat_Procesos_Criterios();
                    _procesos.Criterio_ID = Obj_Procesos_Criterios.Criterio_ID;
                    _procesos.Estatus_ID = Obj_Procesos_Criterios.Estatus_ID;
                    _procesos.Nombre = Obj_Procesos_Criterios.Nombre;
                    _procesos.Descripcion = Obj_Procesos_Criterios.Descripcion;   
                    _procesos.Usuario_Creo = Cls_Sesiones.Usuario;
                    _procesos.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Cat_Procesos_Criterios.Add(_procesos);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar(string jsonObject)
        {
            Cls_Cat_Procesos_Criterios Obj_Procesos_Criterios = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar registro";
                Obj_Procesos_Criterios = JsonMapper.ToObject<Cls_Cat_Procesos_Criterios>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _procesos = dbContext.Cat_Procesos_Criterios.Where(u => u.Proceso_Criterio_ID == Obj_Procesos_Criterios.Proceso_Criterio_ID).First();

                    _procesos.Criterio_ID = Obj_Procesos_Criterios.Criterio_ID;
                    _procesos.Estatus_ID = Obj_Procesos_Criterios.Estatus_ID;
                    _procesos.Nombre = Obj_Procesos_Criterios.Nombre;
                    _procesos.Descripcion = Obj_Procesos_Criterios.Descripcion;
                    _procesos.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _procesos.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.Message.Contains("Los datos de cadena o binarios se truncarían"))
                    Mensaje.Mensaje =
                        "Alguno de los campos que intenta insertar tiene un tamaño mayor al establecido en la base de datos. <br /><br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Los datos de cadena o binarios se truncarían";
                else if (Ex.InnerException.InnerException.Message.Contains("Cannot insert duplicate key row in object"))
                    Mensaje.Mensaje =
                        "Existen campos definidos como claves que no pueden duplicarse. <br />" +
                        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; Por favor revisar que no este ingresando datos duplicados.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Procesos_Criterios(string jsonObject)
        {
            Cls_Cat_Procesos_Criterios Obj_Procesos_Criterios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Procesos_Criterios> Lista_Procesos_criterios = new List<Cls_Cat_Procesos_Criterios>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Procesos_Criterios = JsonMapper.ToObject<Cls_Cat_Procesos_Criterios>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Procesos_Criterios = (from _procesos in dbContext.Cat_Procesos_Criterios
                                 join _estatus in dbContext.Apl_Estatus on _procesos.Estatus_ID equals _estatus.Estatus_ID
                                 join _criterios in dbContext.Cat_Criterios on _procesos.Criterio_ID equals _criterios.Criterio_ID

                                 where  _estatus.Estatus != "ELIMINADO" &&
                                 (!string.IsNullOrEmpty(Obj_Procesos_Criterios.Nombre) ? _procesos.Nombre.ToLower().Contains(Obj_Procesos_Criterios.Nombre.ToLower()) : true)

                                 select new Cls_Cat_Procesos_Criterios
                                 {
                                     Proceso_Criterio_ID=_procesos.Proceso_Criterio_ID,
                                     Nombre = _procesos.Nombre,
                                     Estatus = _estatus.Estatus,
                                     Descripcion=_procesos.Descripcion,
                                     Estatus_ID =_procesos.Estatus_ID,
                                     Criterio_ID=_procesos.Criterio_ID,
                                     Criterio=_criterios.Nombre,
                                 }).OrderByDescending(u => u.Nombre);



                    foreach (var p in Procesos_Criterios)
                        Lista_Procesos_criterios.Add((Cls_Cat_Procesos_Criterios)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Procesos_criterios);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            List<Apl_Estatus> Lista_estatus = new List<Apl_Estatus>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var Estatus = from _empresas in dbContext.Apl_Estatus
                                  select new { _empresas.Estatus, _empresas.Estatus_ID };

                    Json_Resultado = JsonMapper.ToJson(Estatus.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Procesos_Criterios_Por_Nombre(string jsonObject)
        {
            Cls_Cat_Procesos_Criterios Obj_Procesos_Criterios = null;
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Procesos_Criterios> Lista_Empaques = new List<Cls_Cat_Procesos_Criterios>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Validaciones";
                Obj_Procesos_Criterios = JsonMapper.ToObject<Cls_Cat_Procesos_Criterios>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _procesos = (from _proceso in dbContext.Cat_Procesos_Criterios
                                     join _estatus in dbContext.Apl_Estatus on _proceso.Estatus_ID equals _estatus.Estatus_ID

                                     where _proceso.Proceso_Criterio_ID.Equals(Obj_Procesos_Criterios.Proceso_Criterio_ID) ||
                                     _proceso.Nombre.Equals(Obj_Procesos_Criterios.Nombre)

                                     select new Cls_Cat_Procesos_Criterios
                                     {
                                         Proceso_Criterio_ID=_proceso.Proceso_Criterio_ID,
                                         Nombre = _proceso.Nombre,
                                         Estatus = _estatus.Estatus
                                     }).OrderByDescending(u => u.Nombre);

                    if (_procesos.Any())
                    {
                        if (Obj_Procesos_Criterios.Proceso_Criterio_ID == 0)
                        {
                            Mensaje.Estatus = "error";
                            if (!string.IsNullOrEmpty(Obj_Procesos_Criterios.Nombre))
                                Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                        }
                        else
                        {
                            var item_edit = _procesos.Where(u => u.Proceso_Criterio_ID == Obj_Procesos_Criterios.Proceso_Criterio_ID);

                            if (item_edit.Count() == 1)
                                Mensaje.Estatus = "success";
                            else
                            {
                                Mensaje.Estatus = "error";
                                if (!string.IsNullOrEmpty(Obj_Procesos_Criterios.Nombre))
                                    Mensaje.Mensaje = "El nombre ingresado ya se encuentra registrado.";
                            }
                        }
                    }
                    else
                        Mensaje.Estatus = "success";

                    Json_Resultado = JsonMapper.ToJson(Mensaje);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Criterios()
        {
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Criterios_Negocio> Lista_criterios = new List<Cls_Cat_Criterios_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var criterios= from _criterio in dbContext.Cat_Criterios
                                  select new { _criterio.Nombre, _criterio.Criterio_ID };

                    Json_Resultado = JsonMapper.ToJson(criterios.ToList());
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Eliminar(string jsonObject)
        {
           Cls_Cat_Procesos_Criterios Obj_procesos_criterios = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Eliminar registro";
                Obj_procesos_criterios = JsonMapper.ToObject<Cls_Cat_Procesos_Criterios>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _proceso = dbContext.Cat_Procesos_Criterios.Where(u => u.Proceso_Criterio_ID == Obj_procesos_criterios.Proceso_Criterio_ID).First();
                    dbContext.Cat_Procesos_Criterios.Remove(_proceso);
                    dbContext.SaveChanges();
                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "La operación se completó sin problemas.";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Titulo = "Informe Técnico";
                Mensaje.Estatus = "error";
                if (Ex.InnerException.InnerException.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    try
                    {
                        using (var dbContext = new AAM_Cambios_ProcesosEntities())
                        {
                            var _proceso = dbContext.Cat_Procesos_Criterios.Where(u => u.Proceso_Criterio_ID == Obj_procesos_criterios.Proceso_Criterio_ID).First();
                            var _estatus = dbContext.Apl_Estatus.Where(u => u.Estatus == "ELIMINADO").First();

                            _proceso.Estatus_ID = _estatus.Estatus_ID;
                            _proceso.Usuario_Modifico = Cls_Sesiones.Usuario;
                            _proceso.Fecha_Modifico = new DateTime?(DateTime.Now);

                            dbContext.SaveChanges();
                            Mensaje.Estatus = "success";
                            Mensaje.Mensaje = "La operación se completó sin problemas.";
                        }
                    }
                    catch { }
                }
                //Mensaje.Mensaje =
                //        "La operación de eliminar el registro fue revocada. <br /><br />" +
                //        "<i class='fa fa-angle-double-right' ></i>&nbsp;&nbsp; El registro que intenta eliminar ya se encuentra en uso.";
                else
                    Mensaje.Mensaje = "Informe técnico: " + Ex.Message;
            }
            finally
            {
                Json_Resultado = JsonMapper.ToJson(Mensaje);
            }
            return Json_Resultado;
        }
    }
}
