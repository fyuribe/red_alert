﻿using datos_cambios_procesos;
using LitJson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using web_cambios_procesos.Models.Ayudante;
using web_cambios_procesos.Models.Negocio;

namespace web_cambios_procesos.Paginas.Operacion.controllers
{
    /// <summary>
    /// Summary description for Ope_Alertas_Rojas_Controller
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Ope_Alertas_Rojas_Controller : System.Web.Services.WebService
    {
        #region Métodos
        /// <summary>
        /// Método para guardar la alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Guardar_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            //List<Cls_Cambios_Correos_Negocio> lst_Detalles_Correo = new List<Cls_Cambios_Correos_Negocio>();
            //List<Cls_Cambios_Anexos_Negocio> lst_Detalles_Anexos = new List<Cls_Cambios_Anexos_Negocio>();
            //List<Cls_Ope_Autorizacion_Alertas_Rojas> lst_Detalles_Gerentes = new List<Cls_Ope_Autorizacion_Alertas_Rojas>();
            //List<Cls_Ope_Autorizacion_Alertas_Rojas> lst_Detalles_Directores = new List<Cls_Ope_Autorizacion_Alertas_Rojas>();
            //List<Ope_Cambios_Correos> lst_Responsables = new List<Ope_Cambios_Correos>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Guardar alerta";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);
                //lst_Detalles_Correo = JsonConvert.DeserializeObject<List<Cls_Cambios_Correos_Negocio>>(objCambio_Proceso.Datos_Detalles_Correos);//Lista con los detalles de los responsables
                //lst_Detalles_Gerentes = JsonConvert.DeserializeObject<List<Cls_Ope_Autorizacion_Cambios_Procesos_Negocio>>(objCambio_Proceso.Datos_Detalles_Gerentes);//Lista con los detalles de los gerentes
                //lst_Detalles_Directores = JsonConvert.DeserializeObject<List<Cls_Ope_Autorizacion_Cambios_Procesos_Negocio>>(objCambio_Proceso.Datos_Detalles_Directores);//Lista con los detalles de los directores
                //lst_Detalles_Anexos = JsonConvert.DeserializeObject<List<Cls_Cambios_Anexos_Negocio>>(objCambio_Proceso.Datos_Detalles_Anexos);//Lista con los detalles de los archivos

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _alerta = new Ope_Alertas_Rojas();
                    _alerta.Empresa_ID = Convert.ToInt32(Cls_Sesiones.Empresa_ID);
                    _alerta.Estatus_ID = Obj_Alerta_Roja.Estatus_ID;
                    _alerta.Planta_ID = Obj_Alerta_Roja.Planta_ID;
                    _alerta.Unidad_Negocio_ID = Obj_Alerta_Roja.Unidad_Negocio_ID;
                    _alerta.Producto_ID = Obj_Alerta_Roja.Producto_ID;
                    _alerta.Numero_Parte = Obj_Alerta_Roja.Numero_Parte;
                    _alerta.Cliente_ID = Obj_Alerta_Roja.Cliente_ID;
                    _alerta.Referencia_Producto_ID = Obj_Alerta_Roja.Referencia_Producto_ID;
                    _alerta.Sitio_Cliente_ID = Obj_Alerta_Roja.Sitio_Cliente_ID;
                    _alerta.Vehiculo_ID = Obj_Alerta_Roja.Vehiculo_ID;
                    _alerta.Area_ID = Obj_Alerta_Roja.Area_ID;
                    _alerta.Turno_ID = Obj_Alerta_Roja.Turno_ID;
                    _alerta.Numero_CAR = Obj_Alerta_Roja.Numero_CAR;
                    _alerta.Circunstancia_1 = Obj_Alerta_Roja.Circunstancia_1;
                    _alerta.Circunstancia_2 = Obj_Alerta_Roja.Circunstancia_2;
                    _alerta.Circunstancia_3 = Obj_Alerta_Roja.Circunstancia_3;
                    _alerta.Circunstancia_4 = Obj_Alerta_Roja.Circunstancia_4;
                    _alerta.Circunstancia_5 = Obj_Alerta_Roja.Circunstancia_5;
                    _alerta.Circunstancia_6 = Obj_Alerta_Roja.Circunstancia_6;
                    _alerta.Circunstancia_7 = Obj_Alerta_Roja.Circunstancia_7;
                    _alerta.Circunstancia_8 = Obj_Alerta_Roja.Circunstancia_8;
                    _alerta.Descripcion_1 = Obj_Alerta_Roja.Descripcion_1;
                    _alerta.Descripcion_2 = Obj_Alerta_Roja.Descripcion_2;
                    _alerta.Descripcion_3 = Obj_Alerta_Roja.Descripcion_3;
                    _alerta.Descripcion_4 = Obj_Alerta_Roja.Descripcion_4;
                    _alerta.Descripcion_5 = Obj_Alerta_Roja.Descripcion_5;
                    _alerta.Descripcion_6 = Obj_Alerta_Roja.Descripcion_6;
                    _alerta.Descripcion_7 = Obj_Alerta_Roja.Descripcion_7;
                    _alerta.Condicion_Buena = Obj_Alerta_Roja.Condicion_Buena;
                    _alerta.Condicion_Mala = Obj_Alerta_Roja.Condicion_Mala;
                    _alerta.Usuario_Creo = Cls_Sesiones.Usuario;
                    _alerta.Fecha_Creo = new DateTime?(DateTime.Now).Value;

                    dbContext.Ope_Alertas_Rojas.Add(_alerta);

                    //foreach (var Detalles in lst_Detalles_Correo)
                    //{
                    //    Ope_Cambios_Correos det_correo = new Ope_Cambios_Correos();
                    //    det_correo.No_Cambio = Cambio_new.No_Cambio;
                    //    det_correo.No_Extension = 0;
                    //    det_correo.Nombre_Destinatario = Detalles.Empleado;
                    //    det_correo.Email_Destinatario = Detalles.Email;
                    //    det_correo.Departamento_ID = Detalles.Departamento_ID;
                    //    det_correo.Acciones_Requeridas = Detalles.Acciones_Requeridas;
                    //    det_correo.Empleado_ID = Detalles.Empleado_ID;
                    //    //det_correo.Aprobado = Detalles.Aprobado.ToString();
                    //    //det_correo.Fecha_Compromiso = Detalles.Fecha_Compromiso;

                    //    dbContext.Ope_Cambios_Correos.Add(det_correo);


                    //}
                    //foreach (var Detalles in lst_Detalles_Gerentes)
                    //{
                    //    Ope_Autorizacion_Cambios_Procesos det_gerentes = new Ope_Autorizacion_Cambios_Procesos();
                    //    det_gerentes.No_Cambio = Cambio_new.No_Cambio;
                    //    det_gerentes.No_Extension = 0;
                    //    det_gerentes.Nombre = Detalles.Empleado;
                    //    det_gerentes.Email = Detalles.Email;
                    //    det_gerentes.Departamento_ID = Detalles.Departamento_ID;
                    //    det_gerentes.Empleado_ID = Detalles.Empleado_ID;
                    //    //det_gerentes.Aprobado = Detalles.Aprobado.ToString();
                    //    det_gerentes.Fecha_Creo = DateTime.Now;

                    //    dbContext.Ope_Autorizacion_Cambios_Procesos.Add(det_gerentes);


                    //}
                    //foreach (var Detalles in lst_Detalles_Directores)
                    //{
                    //    Ope_Autorizacion_Cambios_Procesos det_directores = new Ope_Autorizacion_Cambios_Procesos();
                    //    det_directores.No_Cambio = Cambio_new.No_Cambio;
                    //    det_directores.No_Extension = 0;
                    //    det_directores.Nombre = Detalles.Empleado;
                    //    det_directores.Email = Detalles.Email;
                    //    det_directores.Departamento_ID = Detalles.Departamento_ID;
                    //    det_directores.Empleado_ID = Detalles.Empleado_ID;
                    //    //det_directores.Aprobado = Detalles.Aprobado.ToString();
                    //    det_directores.Fecha_Creo = DateTime.Now;

                    //    dbContext.Ope_Autorizacion_Cambios_Procesos.Add(det_directores);


                    //}
                    //foreach (var Detalles_Anexos in lst_Detalles_Anexos)
                    //{
                    //    Ope_Cambios_Anexos det_anexos = new Ope_Cambios_Anexos();
                    //    det_anexos.No_Cambio = Cambio_new.No_Cambio;
                    //    det_anexos.No_Extension = 0;
                    //    det_anexos.URL_Archivo = Detalles_Anexos.URL_Archivo;

                    //    dbContext.Ope_Cambios_Anexos.Add(det_anexos);
                    //}

                    //List<Cls_Cambios_Correos_Negocio> Lst_correos = lst_Detalles_Correo.ToList();
                    //List<Cls_Cambios_Anexos_Negocio> Lst_archivos = lst_Detalles_Anexos.ToList();

                    dbContext.SaveChanges();

                    //var _empleado = dbContext.Cat_Empleados.Where(p => p.Empleado_ID == Cambio_Proceso.Validador_ID).First();
                    //Lst_correos.Add(new Cls_Cambios_Correos_Negocio { No_Cambio = Cambio_Proceso.No_Cambio, No_Extension = Cambio_Proceso.No_Extension, Empleado = "", Email = _empleado.Email, Departamento_ID = 0, Acciones_Requeridas = "", Empleado_ID = Cambio_Proceso.Validador_ID });

                    //Enviar_Correo(Lst_correos, Lst_archivos, Cambio_Proceso);

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert saved." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Technical report: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para actualizar la alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Actualizar_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Actualizar alerta";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _alerta = dbContext.Ope_Alertas_Rojas.Where(u => u.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)).First();

                    _alerta.Estatus_ID = Obj_Alerta_Roja.Estatus_ID;
                    _alerta.Planta_ID = Obj_Alerta_Roja.Planta_ID;
                    _alerta.Unidad_Negocio_ID = Obj_Alerta_Roja.Unidad_Negocio_ID;
                    _alerta.Producto_ID = Obj_Alerta_Roja.Producto_ID;
                    _alerta.Numero_Parte = Obj_Alerta_Roja.Numero_Parte;
                    _alerta.Cliente_ID = Obj_Alerta_Roja.Cliente_ID;
                    _alerta.Referencia_Producto_ID = Obj_Alerta_Roja.Referencia_Producto_ID;
                    _alerta.Sitio_Cliente_ID = Obj_Alerta_Roja.Sitio_Cliente_ID;
                    _alerta.Vehiculo_ID = Obj_Alerta_Roja.Vehiculo_ID;
                    _alerta.Area_ID = Obj_Alerta_Roja.Area_ID;
                    _alerta.Turno_ID = Obj_Alerta_Roja.Turno_ID;
                    _alerta.Numero_CAR = Obj_Alerta_Roja.Numero_CAR;
                    _alerta.Circunstancia_1 = Obj_Alerta_Roja.Circunstancia_1;
                    _alerta.Circunstancia_2 = Obj_Alerta_Roja.Circunstancia_2;
                    _alerta.Circunstancia_3 = Obj_Alerta_Roja.Circunstancia_3;
                    _alerta.Circunstancia_4 = Obj_Alerta_Roja.Circunstancia_4;
                    _alerta.Circunstancia_5 = Obj_Alerta_Roja.Circunstancia_5;
                    _alerta.Circunstancia_6 = Obj_Alerta_Roja.Circunstancia_6;
                    _alerta.Circunstancia_7 = Obj_Alerta_Roja.Circunstancia_7;
                    _alerta.Circunstancia_8 = Obj_Alerta_Roja.Circunstancia_8;
                    _alerta.Descripcion_1 = Obj_Alerta_Roja.Descripcion_1;
                    _alerta.Descripcion_2 = Obj_Alerta_Roja.Descripcion_2;
                    _alerta.Descripcion_3 = Obj_Alerta_Roja.Descripcion_3;
                    _alerta.Descripcion_4 = Obj_Alerta_Roja.Descripcion_4;
                    _alerta.Descripcion_5 = Obj_Alerta_Roja.Descripcion_5;
                    _alerta.Descripcion_6 = Obj_Alerta_Roja.Descripcion_6;
                    _alerta.Descripcion_7 = Obj_Alerta_Roja.Descripcion_7;
                    _alerta.Condicion_Buena = Obj_Alerta_Roja.Condicion_Buena;
                    _alerta.Condicion_Mala = Obj_Alerta_Roja.Condicion_Mala;
                    _alerta.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _alerta.Fecha_Modifico = new DateTime?(DateTime.Now).Value;

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert updated." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para actualizar la alerta roja
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Cancelar_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Mensaje.Titulo = "Cancelar alerta";
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "CANCELADO").First();
                    var _alerta = dbContext.Ope_Alertas_Rojas.Where(u => u.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)).First();

                    _alerta.Estatus_ID = _estatus.Estatus_ID;
                    _alerta.Usuario_Modifico = Cls_Sesiones.Usuario;
                    _alerta.Fecha_Modifico = new DateTime?(DateTime.Now);

                    dbContext.SaveChanges();

                    Mensaje.Estatus = "success";
                    Mensaje.Mensaje = "<i class='fa fa-check'style = 'color: #00A41E;' ></ i > &nbsp; Red Alert updated." + " <br />";
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally { Json_Resultado = JsonMapper.ToJson(Mensaje); }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las alertas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Alertas_Rojas(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Negocio> Lista_alertas = new List<Cls_Ope_Alertas_Rojas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Alertas = (from _alertas in dbContext.Ope_Alertas_Rojas
                                   join _estatus in dbContext.Apl_Estatus on _alertas.Estatus_ID equals _estatus.Estatus_ID
                                   join _plantas in dbContext.Apl_Plantas on new { _alertas.Planta_ID, _alertas.Empresa_ID } equals new { _plantas.Planta_ID, _plantas.Empresa_ID }
                                   join _unidad_negocio in dbContext.Cat_Unidades_Negocio on _alertas.Unidad_Negocio_ID equals _unidad_negocio.Unidad_Negocio_ID

                                   where _alertas.Empresa_ID.Equals(empresa_id) &&
                                   _estatus.Estatus != "ELIMINADO" &&
                                   (Obj_Alerta_Roja.No_Alerta_Roja != 0 ? _alertas.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja) : true) &&
                                   (!string.IsNullOrEmpty(Obj_Alerta_Roja.Numero_Parte) ? _alertas.Numero_Parte.ToLower().Contains(Obj_Alerta_Roja.Numero_Parte.ToLower()) : true) &&
                                   (!string.IsNullOrEmpty(Obj_Alerta_Roja.Numero_CAR) ? _alertas.Numero_CAR.ToLower().Contains(Obj_Alerta_Roja.Numero_CAR.ToLower()) : true) &&
                                   (Obj_Alerta_Roja.Estatus_ID != 0 ? _alertas.Estatus_ID.Equals(Obj_Alerta_Roja.Estatus_ID) : true)

                                   select new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = _alertas.No_Alerta_Roja,
                                       Empresa_ID = _alertas.Empresa_ID,
                                       Estatus = _estatus.Estatus,
                                       Estatus_ID = _alertas.Estatus_ID,
                                       Planta = _plantas.Nombre,
                                       Planta_ID = _alertas.Planta_ID,
                                       Unidad_Negocio = _unidad_negocio.Nombre,
                                       Unidad_Negocio_ID = _alertas.Unidad_Negocio_ID,
                                       Producto_ID = _alertas.Producto_ID,
                                       Numero_Parte = _alertas.Numero_Parte,
                                       Cliente_ID = _alertas.Cliente_ID,
                                       Referencia_Producto_ID = _alertas.Referencia_Producto_ID,
                                       Sitio_Cliente_ID = _alertas.Sitio_Cliente_ID,
                                       Vehiculo_ID = _alertas.Vehiculo_ID,
                                       Area_ID = _alertas.Area_ID,
                                       Turno_ID = _alertas.Turno_ID,

                                   }).OrderByDescending(u => u.No_Alerta_Roja);

                    foreach (var p in Alertas)
                        Lista_alertas.Add((Cls_Ope_Alertas_Rojas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_alertas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar una alerta
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Una_Alerta_Roja(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio Obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Negocio> Lista_alertas = new List<Cls_Ope_Alertas_Rojas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            try
            {
                Obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    string n_producto = Obj_Alerta_Roja.Producto_ID == null ? "" :
                        dbContext.Cat_Productos.Where(u => u.Producto_ID == Obj_Alerta_Roja.Producto_ID).Select(u => u.Nombre).First();
                    string n_cliente = Obj_Alerta_Roja.Cliente_ID == null ? "" :
                        dbContext.Cat_Clientes.Where(u => u.Cliente_ID == Obj_Alerta_Roja.Cliente_ID).Select(u => u.Nombre).First();
                    string n_referencia_producto = Obj_Alerta_Roja.Referencia_Producto_ID == null ? "" :
                        dbContext.Cat_Referencia_Productos.Where(u => u.Referencia_Producto_ID == Obj_Alerta_Roja.Referencia_Producto_ID).Select(u => u.Nombre).First();
                    string n_sitio_cliente = Obj_Alerta_Roja.Sitio_Cliente_ID == null ? "" :
                        dbContext.Cat_Sitios_Clientes.Where(u => u.Sitio_Cliente_ID == Obj_Alerta_Roja.Sitio_Cliente_ID).Select(u => u.Nombre).First();
                    string n_vehiculo = Obj_Alerta_Roja.Vehiculo_ID == null ? "" :
                        dbContext.Cat_Vehiculos.Where(u => u.Vehiculo_ID == Obj_Alerta_Roja.Vehiculo_ID).Select(u => u.Nombre).First();
                    string n_area = Obj_Alerta_Roja.Area_ID == 0 ? "" :
                        dbContext.Cat_Areas.Where(u => u.Area_ID == Obj_Alerta_Roja.Area_ID).Select(u => u.Nombre).First();
                    string n_turno = Obj_Alerta_Roja.Turno_ID == 0 ? "" :
                        dbContext.Cat_Turnos.Where(u => u.Turno_ID == Obj_Alerta_Roja.Turno_ID).Select(u => u.Nombre).First();

                    int empresa_id = string.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                    var Alertas = (from _alertas in dbContext.Ope_Alertas_Rojas
                                   where _alertas.Empresa_ID.Equals(empresa_id) &&
                                   _alertas.No_Alerta_Roja.Equals(Obj_Alerta_Roja.No_Alerta_Roja)

                                   select new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = _alertas.No_Alerta_Roja,
                                       Producto = n_producto,
                                       Numero_Parte = _alertas.Numero_Parte,
                                       Cliente = n_cliente,
                                       Referencia_Producto = n_referencia_producto,
                                       Sitio_Cliente = n_sitio_cliente,
                                       Vehiculo = n_vehiculo,
                                       Area = n_area,
                                       Turno = n_turno,
                                       Numero_CAR = _alertas.Numero_CAR,
                                       Circunstancia_1 = _alertas.Circunstancia_1,
                                       Circunstancia_2 = _alertas.Circunstancia_2,
                                       Circunstancia_3 = _alertas.Circunstancia_3,
                                       Circunstancia_4 = _alertas.Circunstancia_4,
                                       Circunstancia_5 = _alertas.Circunstancia_5,
                                       Circunstancia_6 = _alertas.Circunstancia_6,
                                       Circunstancia_7 = _alertas.Circunstancia_7,
                                       Circunstancia_8 = _alertas.Circunstancia_8,
                                       Descripcion_1 = _alertas.Descripcion_1,
                                       Descripcion_2 = _alertas.Descripcion_2,
                                       Descripcion_3 = _alertas.Descripcion_3,
                                       Descripcion_4 = _alertas.Descripcion_4,
                                       Descripcion_5 = _alertas.Descripcion_5,
                                       Descripcion_6 = _alertas.Descripcion_6,
                                       Descripcion_7 = _alertas.Descripcion_7,
                                       Condicion_Buena = _alertas.Condicion_Buena,
                                       Condicion_Mala = _alertas.Condicion_Mala

                                   }).OrderByDescending(u => u.No_Alerta_Roja);
                    foreach (var p in Alertas)
                        Lista_alertas.Add((Cls_Ope_Alertas_Rojas_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_alertas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// Método para consultar los estatus
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Estatus()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Estatus = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_estatus = (from _estatus in dbContext.Apl_Estatus
                                        where _estatus.Estatus.Contains(q)
                                        select new Cls_Select2
                                        {
                                            id = _estatus.Estatus_ID.ToString(),
                                            text = _estatus.Estatus,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);
                    if (_lst_estatus.Any())
                        foreach (var p in _lst_estatus)
                            Lista_Estatus.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Estatus);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las plantas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Plantas()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Plantas = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_plantas = (from _plantas in dbContext.Apl_Plantas
                                        where _plantas.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                        && _plantas.Nombre.Contains(q)
                                        select new Cls_Select2
                                        {
                                            id = _plantas.Planta_ID.ToString(),
                                            text = _plantas.Nombre,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);

                    if (_lst_plantas.Any())
                        foreach (var p in _lst_plantas)
                            Lista_Plantas.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Plantas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las unidades de negocio
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Unidades_Negocio()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Unidades = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_unidades = (from _unidades in dbContext.Cat_Unidades_Negocio
                                         where _unidades.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _unidades.Nombre.Contains(q)
                                         select new Cls_Select2
                                         {
                                             id = _unidades.Unidad_Negocio_ID.ToString(),
                                             text = _unidades.Nombre,
                                             tag = String.Empty
                                         }).OrderBy(u => u.text);

                    if (_lst_unidades.Any())
                        foreach (var p in _lst_unidades)
                            Lista_Unidades.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Unidades);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los productos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Productos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Productos = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_productos = (from _productos in dbContext.Cat_Productos
                                          where _productos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                          && _productos.Nombre.Contains(q)
                                          select new Cls_Select2
                                          {
                                              id = _productos.Producto_ID.ToString(),
                                              text = _productos.Nombre,
                                              tag = String.Empty
                                          }).OrderBy(u => u.text);

                    if (_lst_productos.Any())
                        foreach (var p in _lst_productos)
                            Lista_Productos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Productos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los turnos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Turnos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Turnos = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_turnos = (from _turnos in dbContext.Cat_Turnos
                                       where _turnos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                       && _turnos.Nombre.Contains(q)
                                       select new Cls_Select2
                                       {
                                           id = _turnos.Turno_ID.ToString(),
                                           text = _turnos.Nombre,
                                           tag = String.Empty
                                       }).OrderBy(u => u.text);

                    if (_lst_turnos.Any())
                        foreach (var p in _lst_turnos)
                            Lista_Turnos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Turnos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las areas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Areas()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Areas = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_areas = (from _areas in dbContext.Cat_Areas
                                      where _areas.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                      && _areas.Nombre.Contains(q)
                                      select new Cls_Select2
                                      {
                                          id = _areas.Area_ID.ToString(),
                                          text = _areas.Nombre,
                                          tag = String.Empty
                                      }).OrderBy(u => u.text);

                    if (_lst_areas.Any())
                        foreach (var p in _lst_areas)
                            Lista_Areas.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Areas);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las areas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Vehiculos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Vehiculos = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_vehiculos = (from _vehiculos in dbContext.Cat_Vehiculos
                                          where _vehiculos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                          && _vehiculos.Nombre.Contains(q)
                                          select new Cls_Select2
                                          {
                                              id = _vehiculos.Vehiculo_ID.ToString(),
                                              text = _vehiculos.Nombre,
                                              tag = String.Empty
                                          }).OrderBy(u => u.text);

                    if (_lst_vehiculos.Any())
                        foreach (var p in _lst_vehiculos)
                            Lista_Vehiculos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Vehiculos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los sitios clientes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Sitios_Clientes()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Sitios_Clientes = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_sitios_clientes = (from _sitios in dbContext.Cat_Sitios_Clientes
                                                where _sitios.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                                && _sitios.Nombre.Contains(q)
                                                select new Cls_Select2
                                                {
                                                    id = _sitios.Sitio_Cliente_ID.ToString(),
                                                    text = _sitios.Nombre,
                                                    tag = String.Empty
                                                }).OrderBy(u => u.text);

                    if (_lst_sitios_clientes.Any())
                        foreach (var p in _lst_sitios_clientes)
                            Lista_Sitios_Clientes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Sitios_Clientes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las referencias de producto
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Referencia_Productos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Referencia_Prodcutos = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_referencia_productos = (from _referencia in dbContext.Cat_Referencia_Productos
                                                     where _referencia.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                                     && _referencia.Nombre.Contains(q)
                                                     select new Cls_Select2
                                                     {
                                                         id = _referencia.Referencia_Producto_ID.ToString(),
                                                         text = _referencia.Nombre,
                                                         tag = String.Empty
                                                     }).OrderBy(u => u.text);

                    if (_lst_referencia_productos.Any())
                        foreach (var p in _lst_referencia_productos)
                            Lista_Referencia_Prodcutos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Referencia_Prodcutos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los clientes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Clientes()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Clientes = new List<Cls_Select2>();
            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_clientes = (from _clientes in dbContext.Cat_Clientes
                                         where _clientes.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _clientes.Nombre.Contains(q)
                                         select new Cls_Select2
                                         {
                                             id = _clientes.Cliente_ID.ToString(),
                                             text = _clientes.Nombre,
                                             tag = String.Empty
                                         }).OrderBy(u => u.text);

                    if (_lst_clientes.Any())
                        foreach (var p in _lst_clientes)
                            Lista_Clientes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Clientes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar las plantas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Unidades_Negocio_Filtro(string jsonObject)
        {
            Cls_Cat_Unidades_Negocio_Negocio Obj_Unidades_Negocio = new Cls_Cat_Unidades_Negocio_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Unidades_Negocio_Negocio> Lista_Unidades = new List<Cls_Cat_Unidades_Negocio_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Unidades_Negocio = JsonMapper.ToObject<Cls_Cat_Unidades_Negocio_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_unidades = (from _unidad_negocio in dbContext.Cat_Unidades_Negocio
                                         join _estatus in dbContext.Apl_Estatus on _unidad_negocio.Estatus_ID equals _estatus.Estatus_ID
                                         where _unidad_negocio.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _unidad_negocio.Planta_ID == Obj_Unidades_Negocio.Planta_ID
                                         && _estatus.Estatus == "ACTIVO"

                                         select new Cls_Cat_Unidades_Negocio_Negocio
                                         {
                                             Unidad_Negocio_ID = _unidad_negocio.Unidad_Negocio_ID,
                                             Planta_ID = _unidad_negocio.Planta_ID,
                                             Nombre = _unidad_negocio.Nombre
                                         }).OrderBy(u => u.Unidad_Negocio_ID);

                    foreach (var p in _lst_unidades)
                        Lista_Unidades.Add((Cls_Cat_Unidades_Negocio_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Unidades);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los vehiculos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Vehiculos_Filtro(string jsonObject)
        {
            Cls_Cat_Vehiculos_Negocio Obj_Vehiculos = new Cls_Cat_Vehiculos_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Vehiculos_Negocio> Lista_Vehiculos = new List<Cls_Cat_Vehiculos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Vehiculos = JsonMapper.ToObject<Cls_Cat_Vehiculos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_vehiculos = (from _vehiculo in dbContext.Cat_Vehiculos
                                          join _estatus in dbContext.Apl_Estatus on _vehiculo.Estatus_ID equals _estatus.Estatus_ID
                                          where _vehiculo.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                          && _estatus.Estatus == "ACTIVO"
                                          && _vehiculo.Sitio_Cliente_ID == Obj_Vehiculos.Sitio_Cliente_ID

                                          select new Cls_Cat_Vehiculos_Negocio
                                          {
                                              Vehiculo_ID = _vehiculo.Vehiculo_ID,
                                              Nombre = _vehiculo.Nombre
                                          }).OrderBy(u => u.Vehiculo_ID);

                    foreach (var p in _lst_vehiculos)
                        Lista_Vehiculos.Add((Cls_Cat_Vehiculos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Vehiculos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las plantas
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Sitios_Clientes_Filtro(string jsonObject)
        {
            Cls_Cat_Sitios_Clientes_Negocio Obj_Sitios_Clientes = new Cls_Cat_Sitios_Clientes_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Sitios_Clientes_Negocio> Lista_Sitios_Clientes = new List<Cls_Cat_Sitios_Clientes_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Sitios_Clientes = JsonMapper.ToObject<Cls_Cat_Sitios_Clientes_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_vehiculos = (from _sitios in dbContext.Cat_Sitios_Clientes
                                          join _estatus in dbContext.Apl_Estatus on _sitios.Estatus_ID equals _estatus.Estatus_ID
                                          where _sitios.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                          && _estatus.Estatus == "ACTIVO"
                                          && _sitios.Sitio_Cliente_ID == _sitios.Sitio_Cliente_ID

                                          select new Cls_Cat_Sitios_Clientes_Negocio
                                          {
                                              Sitio_Cliente_ID = _sitios.Sitio_Cliente_ID,
                                              Nombre = _sitios.Nombre
                                          }).OrderBy(u => u.Sitio_Cliente_ID);

                    foreach (var p in _lst_vehiculos)
                        Lista_Sitios_Clientes.Add((Cls_Cat_Sitios_Clientes_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Sitios_Clientes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar las referencias de productos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Referencia_Productos_Filtro(string jsonObject)
        {
            Cls_Cat_Referencia_Productos_Negocio Obj_Productos = new Cls_Cat_Referencia_Productos_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Referencia_Productos_Negocio> Lista_Productos = new List<Cls_Cat_Referencia_Productos_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Productos = JsonMapper.ToObject<Cls_Cat_Referencia_Productos_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_productoss = (from _productos in dbContext.Cat_Referencia_Productos
                                           join _estatus in dbContext.Apl_Estatus on _productos.Estatus_ID equals _estatus.Estatus_ID
                                           where _productos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                           && _estatus.Estatus == "ACTIVO"
                                           && _productos.Cliente_ID == Obj_Productos.Cliente_ID

                                           select new Cls_Cat_Referencia_Productos_Negocio
                                           {
                                               Referencia_Producto_ID = _productos.Referencia_Producto_ID,
                                               Nombre = _productos.Nombre
                                           }).OrderBy(u => u.Referencia_Producto_ID);

                    foreach (var p in _lst_productoss)
                        Lista_Productos.Add((Cls_Cat_Referencia_Productos_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Productos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los departamentos
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Departamentos()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Departamentos = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _estatus = dbContext.Apl_Estatus.Where(p => p.Estatus == "ACTIVO").First();

                    var _lst_departamentos = (from _departamentos in dbContext.Cat_Departamentos
                                              where _departamentos.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                               && _departamentos.Estatus_ID == _estatus.Estatus_ID
                                              && _departamentos.Nombre.Contains(q)
                                              select new Cls_Select2
                                              {
                                                  id = _departamentos.Departamento_ID.ToString(),
                                                  text = _departamentos.Nombre,
                                                  tag = String.Empty
                                              }).OrderBy(u => u.text);

                    if (_lst_departamentos.Any())
                        foreach (var p in _lst_departamentos)
                            Lista_Departamentos.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Departamentos);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los responsables
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Responsables()
        {
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Responsables = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var niveles_list = new int[] { 5, 6 };

                    var _lst_ordenes = (from _responsables in dbContext.Cat_Empleados

                                        where niveles_list.Contains(_responsables.Nivel)
                                        && _responsables.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                        && _responsables.Estatus == "ACTIVO"
                                        && _responsables.Nombre.Contains(q)

                                        select new Cls_Select2
                                        {
                                            id = _responsables.Empleado_ID.ToString(),
                                            text = _responsables.Nombre + "-" + _responsables.Apellidos,
                                            detalle_1 = _responsables.Email,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);

                    if (_lst_ordenes.Any())
                        foreach (var p in _lst_ordenes)
                            Lista_Responsables.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Responsables);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(Json_Resultado);
                //Context.Response.Write(string.IsNullOrEmpty(Json_Resultado) ? "[]" : Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los datos de un empleado
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Consultar_Datos_Empleados(string jsonObject)
        {
            Cls_Cat_Empleados_Negocio Obj_Empleados = new Cls_Cat_Empleados_Negocio();
            string Json_Resultado = string.Empty;
            List<Cls_Cat_Empleados_Negocio> Lista_Empleados = new List<Cls_Cat_Empleados_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();

            try
            {
                Obj_Empleados = JsonMapper.ToObject<Cls_Cat_Empleados_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var _lst_empleado = (from _empleado in dbContext.Cat_Empleados
                                         join _empresa in dbContext.Apl_Empresas on _empleado.Empresa_ID equals _empresa.Empresa_ID
                                         where _empleado.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                         && _empleado.Estatus == "ACTIVO"
                                         && _empleado.Empleado_ID == Obj_Empleados.Empleado_ID

                                         select new Cls_Cat_Empleados_Negocio
                                         {
                                             Empleado_ID = _empleado.Empleado_ID,
                                             No_Empleado = _empleado.No_Empleado,
                                             Planta = _empleado.Planta,
                                             Email = _empleado.Email,
                                             Empresa_ID = _empleado.Empresa_ID,
                                             Empresa = _empresa.Clave
                                         }).OrderBy(u => u.No_Empleado);

                    foreach (var p in _lst_empleado)
                        Lista_Empleados.Add((Cls_Cat_Empleados_Negocio)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Empleados);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color:#FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }
        /// <summary>
        /// Método para consultar los gerentes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Datos_Gerentes()
        {
            //Cls_Cat_Operaciones_Negocio Obj_Orden_Cambios = new Cls_Cat_Operaciones_Negocio();
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Gerentes = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var niveles_list = new int[] { 7, 8, 9 };
                    var _lst_ordenes = (from _gerentes in dbContext.Cat_Empleados
                                        where niveles_list.Contains(_gerentes.Nivel)
                                        && _gerentes.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                        && _gerentes.Estatus == "ACTIVO"
                                        && _gerentes.Nombre.Contains(q)

                                        select new Cls_Select2
                                        {
                                            id = _gerentes.Empleado_ID.ToString(),
                                            text = _gerentes.Nombre + "-" + _gerentes.Apellidos,
                                            detalle_1 = _gerentes.Apellidos,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);

                    foreach (var p in _lst_ordenes)
                        Lista_Gerentes.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Gerentes);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(string.IsNullOrEmpty(Json_Resultado) ? "[]" : Json_Resultado);
            }
        }
        /// <summary>
        /// Método para consultar los gerentes
        /// </summary>
        /// <returns>Objeto serializado con los resultados de la operación</returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Consultar_Datos_Directores()
        {
            //Cls_Cat_Operaciones_Negocio Obj_Orden_Cambios = new Cls_Cat_Operaciones_Negocio();
            string Json_Resultado = string.Empty;
            Cls_Mensaje Mensaje = new Cls_Mensaje();
            List<Cls_Select2> Lista_Directores = new List<Cls_Select2>();

            try
            {
                string q = string.Empty;
                NameValueCollection nvc = Context.Request.Form;

                if (!String.IsNullOrEmpty(nvc["q"]))
                    q = nvc["q"].ToString().Trim();

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
                {
                    var niveles_list = new int[] { 10, 11 };
                    var _lst_ordenes = (from _directores in dbContext.Cat_Empleados

                                        where niveles_list.Contains(_directores.Nivel)
                                        && _directores.Empresa_ID.ToString() == Cls_Sesiones.Empresa_ID
                                        && _directores.Estatus == "ACTIVO"
                                        && _directores.Nombre.Contains(q)

                                        select new Cls_Select2
                                        {
                                            id = _directores.Empleado_ID.ToString(),
                                            text = _directores.Nombre + "-" + _directores.Apellidos,
                                            detalle_1 = _directores.Apellidos,
                                            tag = String.Empty
                                        }).OrderBy(u => u.text);

                    foreach (var p in _lst_ordenes)
                        Lista_Directores.Add((Cls_Select2)p);

                    Json_Resultado = JsonMapper.ToJson(Lista_Directores);
                }
            }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            finally
            {
                Context.Response.Write(string.IsNullOrEmpty(Json_Resultado) ? "[]" : Json_Resultado);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Ver_Alerta(string jsonObject)
        {
            Cls_Ope_Alertas_Rojas_Negocio obj_Alerta_Roja = null;
            string Json_Resultado = string.Empty;
            List<Cls_Ope_Alertas_Rojas_Negocio> Lista_Alerta_Roja = new List<Cls_Ope_Alertas_Rojas_Negocio>();
            Cls_Mensaje Mensaje = new Cls_Mensaje();


            List<Object> lst = new List<Object>();

         try {
                obj_Alerta_Roja = JsonMapper.ToObject<Cls_Ope_Alertas_Rojas_Negocio>(jsonObject);

                using (var dbContext = new AAM_Cambios_ProcesosEntities())
            {
                int Empresa = String.IsNullOrEmpty(Cls_Sesiones.Empresa_ID) ? -1 : Convert.ToInt32(Cls_Sesiones.Empresa_ID);

                var Alerta_Roja = (from _alerta_roja in dbContext.Ope_Alertas_Rojas
                                   join _empresa in dbContext.Apl_Empresas on _alerta_roja.Empresa_ID equals _empresa.Empresa_ID
                                   join _cliente in dbContext.Cat_Clientes on _alerta_roja.Cliente_ID equals _cliente.Cliente_ID
                                   join _planta in dbContext.Apl_Plantas on _alerta_roja.Planta_ID equals _planta.Planta_ID
                                   join _vehiculo in dbContext.Cat_Vehiculos on _alerta_roja.Vehiculo_ID equals _vehiculo.Vehiculo_ID
                                   join _turno in dbContext.Cat_Turnos on _alerta_roja.Turno_ID equals _turno.Turno_ID
                                   join _unidad_negocio in dbContext.Cat_Unidades_Negocio on _alerta_roja.Unidad_Negocio_ID equals _unidad_negocio.Unidad_Negocio_ID
                                   join _Areas in dbContext.Cat_Areas on _alerta_roja.Area_ID equals _Areas.Area_ID
                                   join _estatus in dbContext.Apl_Estatus on _alerta_roja.Estatus_ID equals _estatus.Estatus_ID
                                   join _producto in dbContext.Cat_Productos on _alerta_roja.Producto_ID equals _producto.Producto_ID
                                   join _sitios_cliente in dbContext.Cat_Sitios_Clientes on _alerta_roja.Sitio_Cliente_ID equals _sitios_cliente.Sitio_Cliente_ID
                                   join _referencia_producto in dbContext.Cat_Referencia_Productos on _alerta_roja.Referencia_Producto_ID equals _referencia_producto.Referencia_Producto_ID

                                   where
                                   _alerta_roja.No_Alerta_Roja.Equals(obj_Alerta_Roja.No_Alerta_Roja)


                                   select new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = _alerta_roja.No_Alerta_Roja,
                                       Empresa_ID = _alerta_roja.Empresa_ID,
                                       Estatus_ID = _alerta_roja.Estatus_ID,
                                       Estatus = _estatus.Estatus,
                                       Planta_ID = _alerta_roja.Planta_ID,
                                       Planta = _planta.Nombre,
                                       Unidad_Negocio_ID = _alerta_roja.Unidad_Negocio_ID,
                                       Unidad_Negocio = _unidad_negocio.Nombre,
                                       Producto_ID = _alerta_roja.Producto_ID,
                                       Producto = _producto.Nombre,
                                       Numero_Parte = _alerta_roja.Numero_Parte,
                                       Cliente_ID = _alerta_roja.Cliente_ID,
                                       Cliente = _cliente.Nombre + " " + _cliente.Apellidos,
                                       Referencia_Producto_ID = _alerta_roja.Referencia_Producto_ID,
                                       Referencia_Producto = _referencia_producto.Nombre,
                                       Sitio_Cliente_ID = _alerta_roja.Sitio_Cliente_ID,
                                       Sitio_Cliente = _sitios_cliente.Nombre,
                                       Vehiculo_ID = _alerta_roja.Vehiculo_ID,
                                       Vehiculo = _vehiculo.Nombre,
                                       Area_ID = _alerta_roja.Area_ID,
                                       Area = _Areas.Nombre,
                                       Turno_ID = _alerta_roja.Turno_ID,
                                       Turno = _turno.Nombre,
                                       Numero_CAR = _alerta_roja.Numero_CAR,
                                       Circunstancia_1 = _alerta_roja.Circunstancia_1,
                                       Circunstancia_2 = _alerta_roja.Circunstancia_2,
                                       Circunstancia_3 = _alerta_roja.Circunstancia_3,
                                       Circunstancia_4 = _alerta_roja.Circunstancia_4,
                                       Circunstancia_5 = _alerta_roja.Circunstancia_5,
                                       Circunstancia_6 = _alerta_roja.Circunstancia_6,
                                       Circunstancia_7 = _alerta_roja.Circunstancia_7,
                                       Circunstancia_8 = _alerta_roja.Circunstancia_8,
                                       Descripcion_1 = _alerta_roja.Descripcion_1,
                                       Descripcion_2 = _alerta_roja.Descripcion_2,
                                       Descripcion_3 = _alerta_roja.Descripcion_3,
                                       Descripcion_4 = _alerta_roja.Descripcion_4,
                                       Descripcion_5 = _alerta_roja.Descripcion_5,
                                       Descripcion_6 = _alerta_roja.Descripcion_6,
                                       Descripcion_7 = _alerta_roja.Descripcion_7,
                                       Condicion_Buena = _alerta_roja.Condicion_Buena,
                                       Condicion_Mala = _alerta_roja.Condicion_Mala,
                                       Estatus_Partes_1 = _alerta_roja.Estatus_Partes_1,
                                       Estatus_Partes_2 = _alerta_roja.Estatus_Partes_2,
                                       Estatus_Partes_3 = _alerta_roja.Estatus_Partes_3,
                                       Estatus_Partes_4 = _alerta_roja.Estatus_Partes_4,
                                       Estatus_Descripcion = _alerta_roja.Estatus_Descripcion,
                                       Estatus_Condicion_1 = _alerta_roja.Estatus_Condicion_1,
                                       Estatus_Condicion_2 = _alerta_roja.Estatus_Condicion_2,
                                       Estatus_Condicion_3 = _alerta_roja.Estatus_Condicion_3,
                                       Director_Responsable = _alerta_roja.Director_Responsable,
                                       Condicion_Gestion_1 = _alerta_roja.Condicion_Gestion_1,
                                       Condicion_Gestion_2 = _alerta_roja.Condicion_Gestion_2,
                                       Condicion_Gestion_3 = _alerta_roja.Condicion_Gestion_3,
                                       Condicion_Gestion_4 = _alerta_roja.Condicion_Gestion_4,
                                       Condicion_Gestion_5 = _alerta_roja.Condicion_Gestion_5,
                                       Condicion_Gestion_6 = _alerta_roja.Condicion_Gestion_6,
                                       Usuario_Creo = _alerta_roja.Usuario_Creo,
                                       Usuario_Modifico = _alerta_roja.Usuario_Modifico,
                                       Fecha_Creo = _alerta_roja.Fecha_Creo.ToString(),
                                       Fecha_Modifico = _alerta_roja.Fecha_Modifico.ToString()

                                   }).GroupBy(x => new
                                   {
                                       x.No_Alerta_Roja,
                                       x.Empresa_ID,
                                       x.Estatus_ID,
                                       x.Estatus,
                                       x.Planta_ID,
                                       x.Planta,
                                       x.Unidad_Negocio_ID,
                                       x.Unidad_Negocio,
                                       x.Producto_ID,
                                       x.Producto,
                                       x.Numero_Parte,
                                       x.Cliente_ID,
                                       x.Cliente,
                                       x.Referencia_Producto_ID,
                                       x.Referencia_Producto,
                                       x.Sitio_Cliente_ID,
                                       x.Sitio_Cliente,
                                       x.Vehiculo_ID,
                                       x.Vehiculo,
                                       x.Area_ID,
                                       x.Area,
                                       x.Turno_ID,
                                       x.Turno,
                                       x.Numero_CAR,
                                       x.Circunstancia_1,
                                       x.Circunstancia_2,
                                       x.Circunstancia_3,
                                       x.Circunstancia_4,
                                       x.Circunstancia_5,
                                       x.Circunstancia_6,
                                       x.Circunstancia_7,
                                       x.Circunstancia_8,
                                       x.Descripcion_1,
                                       x.Descripcion_2,
                                       x.Descripcion_3,
                                       x.Descripcion_4,
                                       x.Descripcion_5,
                                       x.Descripcion_6,
                                       x.Descripcion_7,
                                       x.Condicion_Buena,
                                       x.Condicion_Mala,
                                       x.Estatus_Descripcion,
                                       x.Estatus_Condicion_1,
                                       x.Estatus_Condicion_2,
                                       x.Estatus_Condicion_3,
                                       x.Estatus_Partes_1,
                                       x.Estatus_Partes_2,
                                       x.Estatus_Partes_3,
                                       x.Estatus_Partes_4,
                                       x.Director_Responsable,
                                       x.Condicion_Gestion_1,
                                       x.Condicion_Gestion_2,
                                       x.Condicion_Gestion_3,
                                       x.Condicion_Gestion_4,
                                       x.Condicion_Gestion_5,
                                       x.Condicion_Gestion_6,
                                       x.Usuario_Creo,
                                       x.Usuario_Modifico,
                                       x.Fecha_Creo,
                                       x.Fecha_Modifico

                                   }, (key, group) => new Cls_Ope_Alertas_Rojas_Negocio
                                   {
                                       No_Alerta_Roja = key.No_Alerta_Roja,
                                       Empresa_ID = key.Empresa_ID,
                                       Estatus_ID = key.Estatus_ID,
                                       Estatus = key.Estatus,
                                       Planta_ID = key.Planta_ID,
                                       Planta = key.Planta,
                                       Unidad_Negocio_ID = key.Unidad_Negocio_ID,
                                       Unidad_Negocio = key.Unidad_Negocio,
                                       Producto_ID = key.Producto_ID,
                                       Producto = key.Producto,
                                       Numero_Parte = key.Numero_Parte,
                                       Cliente_ID = key.Cliente_ID,
                                       Cliente = key.Cliente,
                                       Referencia_Producto_ID = key.Referencia_Producto_ID,
                                       Referencia_Producto = key.Referencia_Producto,
                                       Sitio_Cliente_ID = key.Sitio_Cliente_ID,
                                       Sitio_Cliente = key.Sitio_Cliente,
                                       Vehiculo_ID = key.Vehiculo_ID,
                                       Vehiculo = key.Vehiculo,
                                       Area_ID = key.Area_ID,
                                       Area = key.Area,
                                       Turno_ID = key.Turno_ID,
                                       Turno = key.Turno,
                                       Numero_CAR = key.Numero_CAR,
                                       Circunstancia_1 = key.Circunstancia_1,
                                       Circunstancia_2 = key.Circunstancia_2,
                                       Circunstancia_3 = key.Circunstancia_3,
                                       Circunstancia_4 = key.Circunstancia_4,
                                       Circunstancia_5 = key.Circunstancia_5,
                                       Circunstancia_6 = key.Circunstancia_6,
                                       Circunstancia_7 = key.Circunstancia_7,
                                       Circunstancia_8 = key.Circunstancia_8,
                                       Descripcion_1 = key.Descripcion_1,
                                       Descripcion_2 = key.Descripcion_2,
                                       Descripcion_3 = key.Descripcion_3,
                                       Descripcion_4 = key.Descripcion_4,
                                       Descripcion_5 = key.Descripcion_5,
                                       Descripcion_6 = key.Descripcion_6,
                                       Descripcion_7 = key.Descripcion_7,
                                       Condicion_Buena = key.Condicion_Buena,
                                       Condicion_Mala = key.Condicion_Mala,
                                       Estatus_Partes_1 = key.Estatus_Partes_1,
                                       Estatus_Partes_2 = key.Estatus_Partes_2,
                                       Estatus_Partes_3 = key.Estatus_Partes_3,
                                       Estatus_Partes_4 = key.Estatus_Partes_4,
                                       Estatus_Descripcion = key.Estatus_Descripcion,
                                       Estatus_Condicion_1 = key.Estatus_Condicion_1,
                                       Estatus_Condicion_2 = key.Estatus_Condicion_2,
                                       Estatus_Condicion_3 = key.Estatus_Condicion_3,
                                       Director_Responsable = key.Director_Responsable,
                                       Condicion_Gestion_1 = key.Condicion_Gestion_1,
                                       Condicion_Gestion_2 = key.Condicion_Gestion_2,
                                       Condicion_Gestion_3 = key.Condicion_Gestion_3,
                                       Condicion_Gestion_4 = key.Condicion_Gestion_4,
                                       Condicion_Gestion_5 = key.Condicion_Gestion_5,
                                       Condicion_Gestion_6 = key.Condicion_Gestion_6,
                                       Usuario_Creo = key.Usuario_Creo,
                                       Usuario_Modifico = key.Usuario_Modifico,
                                       Fecha_Creo = key.Fecha_Creo,
                                       Fecha_Modifico = key.Fecha_Modifico

                                   }).OrderByDescending(u => u.No_Alerta_Roja);

                foreach (var p in Alerta_Roja)
                    Lista_Alerta_Roja.Add((Cls_Ope_Alertas_Rojas_Negocio)p);

                Json_Resultado = JsonMapper.ToJson(Lista_Alerta_Roja);
            }
        }
            catch (Exception Ex)
            {
                Mensaje.Estatus = "error";
                Mensaje.Mensaje = "<i class='fa fa-times' style='color: #FF0004;'></i>&nbsp;Informe técnico: " + Ex.Message;
            }
            return Json_Resultado;
        }

        #endregion
    }
}
