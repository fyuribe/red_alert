﻿<%@ Page Title="Operación Alertas Rojas" Language="C#" MasterPageFile="~/Paginas/Paginas_Generales/MasterPage.Master" AutoEventWireup="true" CodeBehind="Frm_Ope_Alertas_Rojas.aspx.cs" Inherits="web_cambios_procesos.Paginas.Operacion.Frm_Ope_Alertas_Rojas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Recursos/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <link href="../../Recursos/estilos/demo_form.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-combo/select2.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-table-current/bootstrap-table.css" rel="stylesheet" />
    <link href="../../Recursos/bootstrap-date/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../../Recursos/icon-picker/css/icon-picker.css" rel="stylesheet" />
    <style>
        .fixed-table-toolbar .bars, .fixed-table-toolbar .search, .fixed-table-toolbar .columns {
            margin-top: 0px !important;
        }

        .form-control-icono {
            /*Color de fondo de los controles*/
            background-color: inactiveborder;
            height: 25px;
            font-size: 90%;
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
    <link href="../../Recursos/estilos/css_producto.css" rel="stylesheet" />
    <script src="../../Recursos/plugins/parsley.js"></script>
    <script src="../../Recursos/bootstrap-table-current/bootstrap-table.js"></script>

    <script src="../../Recursos/plugins/jquery.formatCurrency-1.4.0.min.js"></script>   
    <script src="../../Recursos/plugins/jquery.formatCurrency.all.js"></script>
    <script src="../../Recursos/plugins/accounting.min.js"></script>
    <script src="../../Recursos/plugins/pinch.js"></script>

    <script src="../../Recursos/bootstrap-date/moment.min.js"></script>
    <script src="../../Recursos/bootstrap-date/bootstrap-datetimepicker.min.js"></script>
    <script src="../../Recursos/bootstrap-table-current/locale/bootstrap-table-es-MX.js"></script>
    <script src="../../Recursos/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="../../Recursos/bootstrap-table/locale/bootstrap-table-es-MX.min.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-editable.js"></script>
    <script src="../../Recursos/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script>
    <script src="../../Recursos/icon-picker/js/iconPicker.js"></script>
    <script src="../../Recursos/javascript/seguridad/Js_Controlador_Sesion.js"></script>
    <script src="../../Recursos/plugins/jquery.qtip-1.0.0-rc3.min.js"></script>
    <script src="../../Recursos/bootstrap-combo/select2.js"></script>
    <script src="../../Recursos/bootstrap-combo/es.js"></script>
    <script src="../../Recursos/plugins/URI.min.js"></script>
    <script src="../../Recursos/javascript/operacion/Js_Ope_Alertas_Rojas.js"></script>

    <style>
        #tbl_ordenes_cambios_procesos thead tr th{
            border: none !important;
        }
        #tbl_ordenes_cambios_procesos thead tr th:nth-child(n+4){
            border-left: 2px solid #ddd !important;
            border-right: 2px solid #ddd !important;
        }
        .select2-container {
            width: 100% !important;
        }
         .search input:first-of-type {
            min-width: 200px !important;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="div_principal_alertas_rojas">
        
        <div class="container-fluid" style="height: 100vh;">
            <div class="row">
                <div class="col-sm-12 text-left" style="background-color: white!important;">
                    <h3>Red Alert</h3>
                </div>
            </div>

            <hr />
            <div class="panel panel-color panel-info collapsed" id="panel1">
                <div class="panel-heading filter">
                    <h3 class="panel-title">
                        <i style="color: white;" class="glyphicon glyphicon-filter"></i>&nbsp;Filtros de b&uacute;squeda
                    </h3>
                    <div class="panel-options">
                        <a id="ctrl_panel" href="#" data-toggle="panel">
                            <span class="collapse-icon">–</span>
                            <span class="expand-icon">+</span>
                        </a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label>Alert number</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="txt_busqueda_por_no_alerta" class="form-control" placeholder="Search by Alert" />
                        </div>
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label>Part number</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="txt_busqueda_por_no_parte" class="form-control" placeholder="Search by Part" />
                        </div>
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label>CAR number</label>
                        </div>
                        <div class="col-md-3">
                            <input type="text" id="txt_busqueda_por_no_car" class="form-control" placeholder="Search by CAR" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1" style="margin-top: 8px;">
                            <label>Status</label>
                        </div>
                        <div class="col-md-3">
                            <select id="cmb_busqueda_por_estatus" class="form-control"></select>
                        </div>
                        <div class="col-md-8" align="right">
                            <button type="button" id="btn_busqueda" class="btn btn-secondary btn-icon btn-icon-standalone btn-lg">
                                <i class="fa fa-search"></i>
                                <span>Search</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <hr />
            <div  id="toolbar" style="margin-left: 5px;">
                <div class="btn-group" role="group" style="margin-left: 5px;">
                    <button id="btn_inicio" type="button" class="btn btn-info btn-sm" title="Inicio"><i class="glyphicon glyphicon-home"></i></button>
                    <button id="btn_nuevo"  type="button" class="btn btn-info btn-sm" title="Nuevo"><i class="glyphicon glyphicon-plus"></i></button>
                </div>
            </div>
            <table id="tbl_alertas_rojas" data-toolbar="#toolbar" class="table table-responsive"></table>
        </div>

    </div>
     <div id="div_Informacion" style="display:none">
        <div id="div_orden_cambio_proceso">
            <div class="container-fluid" style="height: 100vh;">
                <div id="div_solo_informacion">
                    <div class="page-header">
                        <div class="row">
                            <div class="col-sm-8 text-left" style="background-color: white!important;">
                                <h3>New Red Alert</h3>
                            </div>
                                    <div class="row">
                                        <div class="col-sm-12" style="text-align:right">
                                          <button id="btn_salir" type="button" class="btn btn-primary btn-sm" title="">
                                              <i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Cancel Red Alert
                                          </button>

                                          <button id="btn_guardar" type="button" class="btn btn-primary btn-sm" title="">
                                              <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;&nbsp;Save Red Alert
                                          </button>

                                          <button id="btn_regresar" type="button" class="btn btn-primary btn-sm" title="" style="display:none;">
                                              <i class="glyphicon glyphicon-share-alt"></i>&nbsp;&nbsp;Regresar
                                          </button>

                                        </div>
                                    </div>
                            </div>
                        </div>
                        <%--Contenedor de rows de formulario--%>   

                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                 <label for="cmb_plantas" class="fuente_lbl_controles">(*) AAM Plant:</label>
                                 <select id="cmb_plantas" class="form-control"></select>
                                <input type="text" id="txt_no_alerta_roja" style="display:none" />
                            </div>
                            <div class="col-md-3 col-xs-3">
                                 <label for="cmb_unidades_negocio" class="fuente_lbl_controles">(*) Bussines Unit:</label>
                                 <select id="cmb_unidades_negocio" class="form-control"></select>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                 <label for="cmb_productos" class="fuente_lbl_controles">(*) Product Name:</label>
                                 <select id="cmb_productos" class="form-control"></select>                                   
                            </div>
                            <div class="col-md-3 col-xs-3">
                                 <label for="txt_numero_parte" class="fuente_lbl_controles">(*) Part Number:</label>
                                 <input id="txt_numero_parte" type="text" class="form-control" placeholder="Part Number" />                                  
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_clientes" class="fuente_lbl_controles">(*) Customer:</label>
                                   <select id="cmb_clientes" class="form-control"></select>
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_referencia_productos" class="fuente_lbl_controles">(*) Product Reference:</label>
                                   <select id="cmb_referencia_productos" class="form-control"></select>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_sitios_clientes" class="fuente_lbl_controles">(*) Customer Site:</label>
                                   <select id="cmb_sitios_clientes" class="form-control"></select>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_vehiculos" class="fuente_lbl_controles">(*) Vehicle / Model:</label>
                                   <select id="cmb_vehiculos" class="form-control"></select>                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_areas" class="fuente_lbl_controles">(*) Afected Area:</label>
                                   <select id="cmb_areas" class="form-control"></select>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_turnos" class="fuente_lbl_controles">(*) Related Shift:</label>
                                   <select id="cmb_turnos" class="form-control"></select>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="txt_numero_car" class="fuente_lbl_controles">(*) CAR #:</label>
                                   <input id="txt_numero_car" type="text" class="form-control" placeholder="CAR #"/>                                
                            </div>
                            <div class="col-md-3 col-xs-3">
                                   <label for="cmb_estatus" class="fuente_lbl_controles">(*) Status:</label>
                                   <select id="cmb_estatus" class="form-control"></select>                                
                            </div>
                        </div>
                        <div class="page-header">
                            &nbsp;
                        </div>
                        <div class="page-header">
                            <div class="row">
                                <div id="circunstancias_calidad">
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_1" name="cb_circunstancia_1" value="1"/>
                                        "Yard Hold”, “Recall”, stop shipment potential condition.
                                    </label>
                                </div>
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_2" name="cb_circunstancia_2" value="2"/>
                                        Any internal and/or customer issue related to a safety critical operation.
                                    </label>
                                </div>
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_3" name="cb_circunstancia_3" value="3"/>
                                        All metallic failures identified either internally and/or by the customer.
                                    </label>
                                </div>
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_4" name="cb_circunstancia_4" value="4"/>
                                        Customer and/or IAFT certification lost.
                                    </label>
                                </div>
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_5" name="cb_circunstancia_5" value="5"/>
                                        Warranty ISOCHRONE IPVs >5
                                    </label>
                                </div>
                            </div><%--Fin circunstancias calidad--%> 

                                <div id="circunstancias_lanzador" style="display:none">
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_6" name="cb_circunstancia_6" value="6"/>
                                        Customer Key Milestones 
                                    </label>
                                </div>
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_7" name="cb_circunstancia_7" value="7"/>
                                        SOPR Risk 
                                    </label>
                                </div>
                                <div class="row col-md-12">
                                    <label class="fuente_lbl_controles">
                                        <input type="checkbox" id="cb_circunstancia_8" name="cb_circunstancia_8" value="8"/>
                                        Customer Approvals in Risk
                                    </label>
                                </div>
                            </div><%--Fin circunstancias lanzador--%>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">What happened?</label>
                                <input type="text" id="txt_descripcion_1" class="form-control input-sm" placeholder="What happened?" data-parsley-required="true" maxlength="250"/> <%-- style="min-height: 50px !important;" --%>
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Why is it a problem?</label>
                                <input type="text" id="txt_descripcion_2" class="form-control input-sm" placeholder="Why is it a problem?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">When did it happen?</label>
                                <input type="text" id="txt_descripcion_3" class="form-control input-sm" placeholder="When did it happen?" data-parsley-required="true" maxlength="250"/>
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Who has detected it?</label>
                                <input type="text" id="txt_descripcion_4" class="form-control input-sm" placeholder="Who has detected it?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Where has it been detected?</label>
                                <input type="text" id="txt_descripcion_5" class="form-control input-sm" placeholder="Where has it been detected?" data-parsley-required="true" maxlength="250"/>
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">How has it been detected?</label>
                                <input type="text" id="txt_descripcion_6" class="form-control input-sm" placeholder="How has it been detected?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">How many?</label>
                                <input type="text" id="txt_descripcion_7" class="form-control input-sm" placeholder="How many?" data-parsley-required="true" maxlength="250"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Goog condition</label>
                                <input type="text" id="txt_condicion_buena" class="form-control input-sm" placeholder="Good condition" data-parsley-required="true" maxlength="250" />
                            </div>
                            <div class="col-md-6">
                                <label  class="fuente_lbl_controles">Bad condition</label>
                                <input type="text" id="txt_condicion_mala" class="form-control input-sm" placeholder="Bad condition" data-parsley-required="true" maxlength="250" />
                            </div>
                        </div>
                        
                    </div>

                    <%--</div>--%>
                    <div class="row space"></div>
                    <hr />
                    <%-- TABS & TABLES--%>
                    <div class="row">
                        <div class="col-md-12" style="padding-left: 3px !important;"> 
                            <div id="div_Tabs">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-primary" style="padding: 0px !important;">
                                            <div class="panel-heading">
                                                <h4 class="panel-title" style="color: #000 !important;">&nbsp;</h4>
                                                <span class="pull-left">
                                                    <ul id="tabs" class="nav panel-tabs" data-tabs="tab">
                                                        <li id="lista_destinatarios_id" class="active" ><a href="#lista_destinatarios" data-toggle="tab">Responsables</a></li>
                                                        <li  id="lista_gerentes_id" ><a href="#lista_gerentes" data-toggle="tab">Gerentes</a></li>  
                                                        <li  id="lista_directores_id" ><a href="#lista_directores" data-toggle="tab">Directores</a></li>       
                                                        <li  id="lista_anexos_id" ><a href="#lista_anexos" data-toggle="tab">Anexos</a></li>     
                                                        <li  id="lista_anexos_responsables_id" ><a href="#lista_anexos_responsables" data-toggle="tab">Anexos Responsables</a></li>                                
                                                    </ul>
                                                </span>
                                            </div>
                                            <div class="panel-body">
                                                <div id="my-tab-content" class="tab-content">
                                                    <%-- //Lista Destinatarios--%>
                                                    <div class="tab-pane active" id="lista_destinatarios">
                                                        <div class="row">
                                                            <div class="col-md-3 col-xs-3">
                                                                  <label for="cmb_departamento" class="fuente_lbl_controles">(*) Departamento</label>
                                                                  <select id="cmb_departamento" class="form-control"></select>
                                                            </div>

                                                            <div class="col-md-3 col-xs-3">
                                                                 <label for="cmb_lista_usuario" class="fuente_lbl_controles">Reponsable</label>
                                                                 <select id="cmb_lista_usuario" class="form-control"></select>
                                                            </div>
                                                            <div class="col-md-2 col-xs-2">
                                                                <button id="btn_agregar_destinatarios" type="button" class="btn btn-info btn-sm" title="Agregar Lista de Destinatarios" style="margin:20px; padding: 4px 6px;" >
                                                                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
                                                                </button>
                                                            </div>
                                                            <div class="col-md-4 col-xs-4">
                                                                <input id="txt_acciones" type="text" class="form-control" style="display:none"/>
                                                            </div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="col-md-1 col-xs-1">
                                                                <input id="txt_validado" type="text"  class="form-control"  style="margin-left: 4px; text-align: center; width: 100%; display:none"/>
                                                                
                                                            </div>
                                                            <div class="col-md-1" style="text-align: right !important;">
                                                                
                                                                
                                                            </div>
                                                            <div class="col-md-7 col-xs-7">
                                                            </div>

                                                        </div>
                                                        <div class="row" style="display:none">
                                                           
                                                            <div class="col-md-4" style="padding-top: 8px !important;">
                                                                <label for="txt_correo" class="fuente_lbl_controles">Email</label>
                                                                <input id="txt_correo" type="text" class="form-control" style="width: 100%; padding-top: 5px;" disabled="disabled" />
                                                            </div>
                                                        </div>

                                                        <hr />
                                                         <div class="table-responsive">
                                                            <table id="tbl_lista_destinatarios"   class="table table-responsive"></table>
                                                         </div>
                                                        
                                                    </div>
                                                    <%-- //Lista Gerentes--%>
                                                    <div class="tab-pane" id="lista_gerentes">
                                                           <div class="row">
                                                                <div class="col-md-3 col-xs-3">
                                                                      <label for="cmb_departamento_gerentes" class="fuente_lbl_controles">(*) Departamento</label>
                                                                      <select id="cmb_departamento_gerentes" class="form-control"></select>
                                                                </div>
                                                                <div class="col-md-4 col-xs-4">
                                                                     <label for="cmb_lista_empleados" class="fuente_lbl_controles">Gerente</label>
                                                                     <select id="cmb_lista_empleados" class="form-control"></select>
                                                                </div>
                                                                <div class="col-md-2 col-xs-2">
                                                                    <button id="btn_agregar_gerentes" type="button" class="btn btn-info btn-sm" title="Agregar Lista de Gerentes" style="margin:20px; padding: 4px 6px;" >
                                                                        <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
                                                                    </button>
                                                                </div>
                                                                <div class="col-md-3 col-xs-3">
                                                                     <input id="txt_aprobado" type="text"  class="form-control"  style="margin-left: 4px; text-align: center; width: 100%; display:none"/>
                                                                </div>
                                                            </div>
                                                            <div class="row" style="display:none">
                                                           
                                                                <div class="col-md-4" style="padding-top: 8px !important;">
                                                                    <label for="txt_correo_gerentes" class="fuente_lbl_controles">Email</label>
                                                                    <input id="txt_correo_gerentes" type="text" class="form-control" style="width: 100%; padding-top: 5px;" disabled="disabled" />
                                                                </div>
                                                            </div>
                                                            <hr />
                                                            <table id="tbl_lista_gerentes"  class="table table-responsive"></table>

                                                    </div>
                                                    <%-- //Lista Directores--%>
                                                    <div class="tab-pane" id="lista_directores">
                                                        <div class="row">
                                                            <div class="col-md-3 col-xs-3">
                                                                  <label for="cmb_departamento_directores" class="fuente_lbl_controles">(*) Departamento</label>
                                                                  <select id="cmb_departamento_directores" class="form-control"></select>
                                                            </div>
                                                            <div class="col-md-4 col-xs-4">
                                                                 <label for="cmb_lista_directores" class="fuente_lbl_controles">Director</label>
                                                                 <select id="cmb_lista_directores" class="form-control"></select>
                                                            </div>
                                                            <div class="col-md-2 col-xs-2">
                                                                <button id="btn_agregar_directores" type="button" class="btn btn-info btn-sm" title="Agregar Lista de Directores" style="margin:20px; padding: 4px 6px;" >
                                                                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
                                                                </button>
                                                            </div>
                                                            <div class="col-md-3 col-xs-3">
                                                                     <input id="txt_aprobado_directores" type="text"  class="form-control"  style="margin-left: 4px; text-align: center; width: 100%; display:none"/>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="display:none">
                                                                <div class="col-md-4" style="padding-top: 8px !important;">
                                                                    <label for="txt_correo_directores" class="fuente_lbl_controles">Email</label>
                                                                    <input id="txt_correo_directores" type="text" class="form-control" style="width: 100%; padding-top: 5px;" disabled="disabled" />
                                                                </div>
                                                        </div>                                                       
                                                        <hr />
                                                        <table id="tbl_lista_directores"  class="table table-responsive"></table>

                                                    </div>
                                                    <%-- //Lista Anexos--%>
                                                    <div class="tab-pane" id="lista_anexos">
                                                          
                                                        <div class="row">
    
                                                            <div class="col-md-6 form-inline">
                                                               <input id="fl_doc" style="width: 100%;" type="file" name="file" accept="(.*).(.pdf|.PDF|.docx|.DOCX|.doc|.DOC|.xlsx|.XLSX|.xls|.XLS)$"/>
                                                            </div>
                                                            <div class="col-md-4 form-inline">
                                                                <button id="btn_agregar_anexos" type="button" class="btn btn-info btn-sm" title="Agregar Lista de Anexos" style="margin: 0px; padding: 4px 6px;">
                                                                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar
                                                                </button>
                                                            </div>  
                                                        </div>

                                                            <hr />
                                                        <table id="tbl_lista_anexos"  class="table table-responsive"></table>
                                                    </div>
                                                    <%-- //Lista Anexos Responsables--%>
                                                    <div class="tab-pane" id="lista_anexos_responsables" style="display:none">
                                                       <div class="table-responsive">
                                                            <table id="tbl_anexos_responsables" data-toolbar="#toolbar"  class="table table-responsive"></table>
                                                       </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>

