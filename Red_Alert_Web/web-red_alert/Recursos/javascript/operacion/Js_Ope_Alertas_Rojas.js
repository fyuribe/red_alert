﻿var $table = null;
var $table_Correo = null;
var $table_Anexos = null;
var $table_Gerentes = null;
var $table_Directores = null;
var closeModal = true;
var p;
var noalerta = 0;

$(document).on('ready', function () {
    _inicializar_pagina();
    $table = $('#tbl_alertas_rojas');
    $table_Correo = $('#tbl_lista_destinatarios');
    $table_Gerentes = $('#tbl_lista_gerentes');
    $table_Directores = $('#tbl_lista_directores');
    $table_Anexos = $('#tbl_lista_anexos');
    _limpiar_controles();
    _set_location_toolbar();
    _eventos();
    try {
        //Obtener el numero de orden
        objURI = new URI(window.location.href);

        //Obtener la cadena del query string
        objParametros = objURI.search(true);

        //verificar si no es nulo
        if (objParametros != null) {
            //Obtener el numero de la orden de produccion
            noalerta = parseInt(objParametros.No_Alerta_Roja);
            //noextension = parseInt(objParametros.No_Extension);

            //Llenar los detalles de la orden de produccion
            _search_alerta_roja();
        }
    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex.message);
    }
    
});
function _inicializar_pagina() {
    try {
        //_estado_inicial()
        _crear_tbl_destinatarios();
        _crear_tbl_anexos();
        _crear_tbl_gerentes();
        _crear_tbl_directores();
        _agregar_tooltip();
        _agregar_tooltip_tabla();
        _load_estatus('#cmb_busqueda_por_estatus');
        _crear_tbl_alertas_rojas();
        _habilitar_controles('Inicial');
        _set_location_toolbar();
        _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}
function _eventos() {
    try {
        $('#modal_datos').on('hidden.bs.modal', function () {
            if (!closeModal)
                $(this).modal('show');
        });
        $('#btn_nuevo').on('click', function (e) {
            $('#div_Informacion').css('display', 'block');
            $('#div_principal_alertas_rojas').css('display', 'none');
            _limpiar_controles();
            _consultar_departamentos('#cmb_departamento');
            _consultar_departamentos('#cmb_departamento_iniciador');
            _consultar_departamentos('#cmb_departamento_gerentes');
            _consultar_departamentos('#cmb_departamento_directores');
            _consultar_usuarios('#cmb_lista_usuario');
            _consultar_Directores('#cmb_lista_directores');
            _consultar_Gerentes('#cmb_lista_empleados');
            _load_unidades_negocio('#cmb_unidades_negocio');
            _load_productos('#cmb_productos');
            _load_plantas('#cmb_plantas');
            _load_turnos('#cmb_turnos');
            _load_areas('#cmb_areas');
            _load_vehiculos('#cmb_vehiculos');
            _load_sitios_cliente('#cmb_sitios_clientes');
            _load_referencia_productos('#cmb_referencia_productos');
            _load_clientes('#cmb_clientes');
            _load_estatus('#cmb_estatus');
            $('#cmb_estatus').attr("disabled", true);
        });
        $('#btn_inicio').on('click', function (e) { e.preventDefault(); window.location.href = '../Paginas_Generales/Frm_Apl_Principal.aspx'; });
        $('#btn_busqueda').on('click', function (e) {
            _search_alertas_rojas_por_filtros();
        });
        $('#btn_salir').on('click', function (e) {

            if ($('#txt_descripcion_1').val() !== '') {
                bootbox.dialog({
                    message: "The entered data will not be saved<br/> Are you sure to continue?",
                    title: "Red Alert",
                    locale: 'es',
                    closeButton: true,
                    buttons: [{
                        label: 'Ok',
                        className: 'btn-default',
                        callback: function () {
                            $('#div_Informacion').css('display', 'none');
                            $('#div_principal_alertas_rojas').css('display', 'block');
                            _crear_tbl_alertas_rojas();
                            _search_alertas_rojas_por_filtros();
                            _set_location_toolbar();
                            _limpiar_controles();
                            _limpiar_datos_destinatario();
                        }
                    }, {
                        label: 'Cancel',
                        className: 'btn-default',
                        callback: function () {
                        }
                    }]
                });
            } else {
                $('#div_Informacion').css('display', 'none');
                $('#div_principal_alertas_rojas').css('display', 'block');
                _limpiar_controles();
            }
        });
        $('#btn_guardar').on('click', function (e) {
            e.preventDefault();
            var destinatarios = "";
            var anexos = "";
            bootbox.confirm({
                title: 'New Red Alert',
                message: 'Are you sure to save the alert?',
                callback: function (result) {
                    if (result) {
                        var output = _validar_datos();
                        if (output.Estatus) {

                            //validar anexos
                            if ($table_Anexos.bootstrapTable('getOptions').totalRows != 2) {
                                _mostrar_mensaje('Validación', 'Se deben agregar por lo menos dos archivos en el cambio de proceso.');
                                return;
                            }
                            //validar los departamentos con los registros de la tabla
                            var lista = _lista_departamentos();
                            if ($table_Correo.bootstrapTable('getOptions').totalRows != lista) {
                                _mostrar_mensaje('Validación', 'No se han agregado todos los responsables en el cambio de proceso.');
                                return;
                            }
                            //validar los gerentes con los registros de la tabla 
                            var lista_gerentes = _lista_gerentes();
                            if ($table_Gerentes.bootstrapTable('getOptions').totalRows != lista_gerentes) {
                                _mostrar_mensaje('Validación', 'No se han agregado todos los gerentes en el cambio de proceso.');
                                return;
                            }
                            //validar los directores con los registros de la tabla 
                            var lista_directores = _lista_gerentes();
                            if ($table_Directores.bootstrapTable('getOptions').totalRows != lista_directores) {
                                _mostrar_mensaje('Validación', 'No se han agregado todos los directores en el cambio de proceso.');
                                return;
                            }
                            if ($('#txt_no_alerta_roja').val() == '')
                                _guardar_alerta_roja();
                            else
                                _actualizar_alerta_roja();
                            _limpiar_controles();
                            $('#div_Informacion').css('display', 'none');
                            $('#div_principal_alertas_rojas').css('display', 'block');
                            _crear_tbl_alertas_rojas();
                            _search_alertas_rojas_por_filtros();

                        } else _mostrar_mensaje('Validation report', output.Mensaje);
                    }
                }
            });
        });
        $('#btn_regresar').on('click', function (e) { e.preventDefault(); window.location.href = '../Paginas_Generales/Frm_Apl_Principal.aspx'; });
        $('#btn_agregar_destinatarios').on('click', function (e) {
            e.preventDefault();
            _agregar_destinatario();
        });
        $('#btn_agregar_gerentes').on('click', function (e) {
            e.preventDefault();
            _agregar_gerentes();
        });
        $('#btn_agregar_directores').on('click', function (e) {
            e.preventDefault();
            _agregar_directores();
        });
        $('#btn_agregar_anexos').on('click', function (e) {
            e.preventDefault();
            _agregar_anexos();
        });
        $('#modal_datos input[type=text]').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
        $('#modal_datos select').each(function (index, element) {
            $(this).on('focus', function () {
                _remove_class_error('#' + $(this).attr('id'));
            });
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _agregar_tooltip() {
    _init_btn_config($('#btn_nuevo'), 'New Red Alert', 'bottomRight');
    _init_btn_config($('#btn_inicio'), 'Home', 'bottomRight');
    _init_btn_config($('#btn_salir'), 'Cancel Red Alert', 'bottomRight');
    _init_btn_config($('#btn_guardar'), 'Save Red Alert', 'bottomRight');
    _init_btn_config($('#btn_busqueda'), 'Search Red Alert', 'bottomRight');
}
function _init_btn_config(btn, text, alineacionTooltip) {
    $(btn).qtip({
        content: text,
        position: {
            corner: {
                target: 'topMiddle',
                tooltip: alineacionTooltip
            }
        },
        show: {
            when: { event: 'mouseover' },
            ready: false
        },
        hide: { event: 'mouseout' },
        style: {
            border: {
                width: 5,
                radius: 7
            },
            padding: 5,
            textAlign: 'center',
            tip: {
                corner: true,
                method: "polygon",
                border: 1,
                height: 20,
                width: 9
            },
            background: '#F5F6CE',
            color: '#2d2d30',
            width: 200,
            'font-size': 'small',
            'font-family': 'Calibri',
            'font-weight': 'Bold',
            tip: true,
            name: 'blue'
        }
    });
}
function _limpiar_controles() {
    $('input[type=text]').each(function () { $(this).val(''); });
    $('input[type=checkbox]').each(function () { $(this).attr('checked', false) });
    $("#cmb_plantas").empty().trigger("change");;
    $("#cmb_unidades_negocio").empty().trigger("change");
    $("#cmb_productos").empty().trigger("change");
    $('#txt_numero_parte').val('');
    $("#cmb_clientes").empty().trigger("change");
    $('#cmb_referencia_productos').empty().trigger("change");
    $('#cmb_sitios_clientes').empty().trigger("change");
    $('#cmb_vehiculos').empty().trigger("change");
    $('#cmb_areas').empty().trigger("change");
    $('#cmb_turnos').empty().trigger("change");
    $('#cmb_estatus').empty().trigger("change");
    $('#txt_numero_car').val('');
    _validation_sumary(null);

    $('#tbl_lista_destinatarios').bootstrapTable('load', []);
    $('#tbl_lista_gerentes').bootstrapTable('load', []);
    $('#tbl_lista_directores').bootstrapTable('load', []);
    $('#tbl_lista_anexos').bootstrapTable('load', []);
}
function _formato(row) {
    if (!row.id) { return row.text; }
    else if (row.id == row.text) return row.text;

    var _salida = '<span style="text-transform:uppercase;">' +
        '&nbsp;&nbsp;&nbsp;<b> &nbsp;&nbsp;' + row.text + '</b>' +
        '&nbsp;&nbsp;<i class="glyphicon glyphicon-minus"></i>&nbsp;' + row.detalle_1 +
        '</span>';

    return $(_salida);
}
function _templateSelection(row) {
    if (!row.id) { return row.text; }
    else if (row.id == row.text) return row.text;

    var _salida = '<span style="text-transform:uppercase;">' +
       '&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;' + row.text + '</b>' +
       '&nbsp;&nbsp;<i class="glyphicon glyphicon-minus"></i>&nbsp;' + row.detalle_1 +
       '</span>';

    return $(_salida);
}
function _validar_datos_requeridos_agregar_destinatarios() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';

        if ($('#cmb_lista_usuario').val() == '' || $('#cmb_lista_usuario').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione un el destinatario.<br />';
        }
        if ($('#cmb_departamento :selected').val() == '' || $('#cmb_departamento :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;El departamento es un dato requerido.<br />';
        }
        //if ($("#txt_validado").val() == null || $("#txt_validado").val() == "") {
        //    _add_class_error('#txt_validado');
        //    _output.Estatus = false;
        //    _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;La aprobación es un dato requerido.<br />';
        //}
        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe técnico', e);
    } finally {
        return _output;
    }
}
function _validar_no_duplicar_destinatarios(id) {
    var _destinatarios_agregados = null;
    var _destinatarios_existes_tabla = false;
    try {
        _destinatarios_agregados = $("#tbl_lista_destinatarios").bootstrapTable('getData');
        $.each(_destinatarios_agregados, function (index, value) {

            if (value.Empleado_ID == id)
                _destinatarios_existes_tabla = true;
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _destinatarios_existes_tabla;
}
function _validar_no_duplicar_departamento(id) {
    var _destinatarios_agregados = null;
    var _destinatarios_existes_tabla = false;
    try {
        _destinatarios_agregados = $("#tbl_lista_destinatarios").bootstrapTable('getData');
        $.each(_destinatarios_agregados, function (index, value) {

            if (value.Departamento_ID == id)
                _destinatarios_existes_tabla = true;
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _destinatarios_existes_tabla;
}
function _agregar_lista() {
    var Destinatario = null;
    var lst_destinatario = null;
    try {
        $('#tbl_lista_destinatarios').bootstrapTable('insertRow', {
            index: 0,
            row: {
                Departamento_ID: $('#cmb_departamento :selected').val(),
                Departamento: $('#cmb_departamento :selected').text(),
                Acciones_Requeridas: $('#txt_acciones').val(),
                Empleado_ID: $('#cmb_lista_usuario :selected').val(),
                Empleado: $('#cmb_lista_usuario').text(),
                //Aprobado:$('#txt_validado').val(),
                Email: $('#txt_correo').val()
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _agregar_destinatario() {
    try {
        var valida_datos_requerido = _validar_datos_requeridos_agregar_destinatarios();
        if (valida_datos_requerido.Estatus) {
            if (!_validar_no_duplicar_departamento($('#cmb_departamento :selected').val())) {
                if (!_validar_no_duplicar_destinatarios($('#cmb_lista_usuario :selected').val())) {
                    _agregar_lista();
                    _limpiar_datos_destinatario();
                } else {
                    _mostrar_mensaje('Información', 'El responsable que desea agregar ya se encuentra en la lista de los responsables.');
                }
            } else {
                _mostrar_mensaje('Información', 'El departamento que desea agregar ya se encuentra en la lista de los responsables.');
            }
        } else { _mostrar_mensaje('Información', valida_datos_requerido.Mensaje); }

    } catch (ex) {
        _mostrar_mensaje('Error Técnico', ex.message);
    }
}
function _limpiar_datos_destinatario() {
    $('#txt_correo').val('');
    $('#txt_modificar_orden').val('');
    $("#cmb_lista_usuario").empty().trigger("change");
    $("#cmb_departamento").empty().trigger("change");
    $('#txt_acciones').val('');
}
function _crear_tbl_destinatarios() {
    try {
        $('#tbl_lista_destinatarios').bootstrapTable('destroy');
        $('#tbl_lista_destinatarios').bootstrapTable({
            idField: 'Empleado_ID',
            uniqueId: 'Empleado_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Departamento_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Departamento', title: 'Departamento', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Acciones_Requeridas', title: 'Acciones Requeridas', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Empleado_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Empleado', title: 'Responsable', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Aprobado', title: 'Aprobado', align: 'center', valign: 'bottom', sortable: true, visible: true },
                {
                    field: 'Fecha_Compromiso', title: 'Fecha Compromiso', align: 'center', formatter: function (value, row) {
                        if (value != null && value != '')
                            return Date.parse(value).toString("MM/dd/yyyy");
                        else
                            return value;
                    }, fixed: true, resizable: true
                },
                { field: 'Email', title: 'Email', align: 'left', valign: 'bottom', sortable: true, visible: false },

                {
                    field: 'eliminar', title: 'Quitar', align: 'center', valign: 'bottom', formatter: function (value, row) {
                        return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-destinatario=\'' + JSON.stringify(row) + '\' onclick="_quitar_destinatarios(this);"><i class="glyphicon glyphicon-remove"></i></a></div>';
                    }
                }
            ],
            onLoadSuccess: function (data) {
            }
        });
    } catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _quitar_destinatarios(destinatario) {
    var indexrow = null;
    var row = $(destinatario).data('destinatario');
    try {
        $('#tbl_lista_destinatarios').bootstrapTable('remove', {
            field: 'Empleado',
            values: [$.trim(row.Empleado.toString())]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Close',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}
function _validation_sumary(validation) {
    var header_message = '<i class="fa fa-exclamation-triangle fa-2x"></i><span>Observaciones</span><br />';
    if (validation == null) {
        $('#lbl_msg_error').html('');
        $('#sumary_error').css('display', 'none');
    } else {
        $('#lbl_msg_error').html(header_message + validation.Mensaje);
        $('#sumary_error').css('display', 'block');
    }
}
function _validar_datos() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';
        if ($('#cmb_plantas :selected').val() == '' || $('#cmb_plantas :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;AAM plant is a required data.<br />';
        }
        if ($('#cmb_unidades_negocio :selected').val() == '' || $('#cmb_unidades_negocio :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Bussines unit is a required data.<br />';
        }
        if ($('#cmb_productos :selected').val() == '' || $('#cmb_productos :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Product name is a required data.<br />';
        }
        if ($('#cmb_clientes :selected').val() == '' || $('#cmb_clientes :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Customer is a required data.<br />';
        }
        if ($('#cmb_referencia_productos :selected').val() == '' || $('#cmb_referencia_productos :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Product reference is a required data.<br />';
        }
        if ($('#cmb_sitios_clientes :selected').val() == '' || $('#cmb_sitios_clientes :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Customer site is a required data.<br />';
        }
        if ($('#cmb_vehiculos :selected').val() == '' || $('#cmb_vehiculos :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Vehicle is a required data.<br />';
        }
        if ($('#cmb_turnos :selected').val() == '' || $('#cmb_turnos :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Related shift is a required data.<br />';
        }
        if ($('#cmb_areas :selected').val() == '' || $('#cmb_areas :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Afected area is a required data.<br />';
        }
        if ($('#cmb_estatus :selected').val() == '' || $('#cmb_estatus :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Status is a required data.<br />';
        }
        if ($('#txt_numero_parte').val() == '' || $('#txt_numero_parte').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;Part number is a required data.<br />';
        }
        if ($('#txt_numero_car').val() == '' || $('#txt_numero_car').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;CAR number is a required data.<br />';
        }
        if ($table_Correo.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;No se ha agregado ningún destinatario a la orden de cambio de proceso.<br />';
        }
        if ($table_Anexos.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;No se ha agregado ningún archivo a la orden de cambio de proceso.<br />';
        }
        if ($table_Gerentes.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;No se ha agregado ningún gerente a la orden de cambio de proceso.<br />';
        }
        if ($table_Directores.bootstrapTable('getOptions').totalRows == 0) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;No se ha agregado ningún director a la orden de cambio de proceso.<br />';
        }

        if (_output.Mensaje != "")
            _output.Mensaje = "Please fill the next field(s): <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Technical report', e);
    } finally {
        return _output;
    }
}
function _crear_tbl_anexos() {
    try {
        $('#tbl_lista_anexos').bootstrapTable('destroy');
        $('#tbl_lista_anexos').bootstrapTable({
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'URL_Archivo', title: 'Archivo', align: 'left', valign: 'bottom', sortable: true, visible: false },
                { field: 'Nombre', title: 'Archivo', align: 'left', valign: 'bottom', sortable: true, visible: true },
                {
                    field: 'eliminar', title: 'Quitar', align: 'center', valign: 'bottom', formatter: function (value, row) {
                        return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-archivo=\'' + JSON.stringify(row) + '\' onclick="_quitar_archivos(this);"><i class="glyphicon glyphicon-remove"></i></a></div>';
                    }
                },
                {
                    field: 'mostrar', title: 'Ver', align: 'center', valign: 'bottom', formatter: function (value, row) {
                        return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-archivo=\'' + JSON.stringify(row) + '\' onclick="_ver_archivos(this);"><i class="glyphicon glyphicon-eye-open"></i></a></div>';
                    }
                }
            ],
            onLoadSuccess: function (data) {
            }
        });
    } catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _agregar_anexos() {
    var Archivos = $('#fl_doc').get(0).files;
    var data = new FormData();
    var Ruta = '';
    var Nombre = '';
    try {
        if (Archivos.length > 0) {
            data.append('file' + 0, Archivos[0]);
            Ruta = '../../doc_cambio_proceso/' + Archivos[0].name;
            Nombre = Archivos[0].name;
            _copiar_archivo_diretorio(data);

            $('#tbl_lista_anexos').bootstrapTable('insertRow', {
                index: 0,
                row: {
                    URL_Archivo: Ruta,
                    Nombre: Nombre
                }
            });
        } else {
            _mostrar_mensaje('Validación', 'No se ha seleccionado ningún archivo.');
        }
    } catch (ex) {
        _mostrar_mensaje('Error Técnico', ex.message);
    }
}
//--------------------------------------
//  Funcion: _copiar_archivo_diretorio
//  Descripción: Función que guarda el archivo del perfil 
//--------------------------------------
function _copiar_archivo_diretorio(data) {
    try {
        $.ajax({
            type: "POST",
            url: "../../FileUploadHandler.ashx",
            contentType: false,
            processData: false,
            data: data,
            async: false,
            success: function (result) {
                if (result) {

                    $("#fl_doc").val('');
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _quitar_archivos(archivo) {
    var indexrow = null;
    var row = $(archivo).data('archivo');

    try {
        $('#tbl_lista_anexos').bootstrapTable('remove', {
            field: 'Nombre',
            values: [row.Nombre]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _ver_archivos(archivo) {
    var indexrow = null;
    var row = $(archivo).data('archivo');
    var Ruta = '';
    var Nombre = '';
    try {
        Ruta = row.URL_Archivo;
        Nombre = row.Nombre;
        //window.open(Ruta);
        window.open(Ruta, Nombre, 'toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _guardar_alerta_roja() {
    var Alerta = null;
    try {
        Alerta = new Object();
        Alerta.Estatus_ID = $('#cmb_estatus').val() == '' ? 0 : parseInt($('#cmb_estatus').val());
        Alerta.Planta_ID = $('#cmb_plantas').val() == '' ? 0 : parseInt($('#cmb_plantas').val());
        Alerta.Unidad_Negocio_ID = $('#cmb_unidades_negocio').val() == '' ? 0 : parseInt($('#cmb_unidades_negocio').val());
        Alerta.Producto_ID = $('#cmb_productos').val() == '' ? 0 : parseInt($('#cmb_productos').val());
        Alerta.Numero_Parte = $('#txt_numero_parte').val();
        Alerta.Cliente_ID = $('#cmb_clientes').val() == '' ? 0 : parseInt($('#cmb_clientes').val());
        Alerta.Referencia_Producto_ID = $('#cmb_referencia_productos').val() == '' ? 0 : parseInt($('#cmb_referencia_productos').val());
        Alerta.Sitio_Cliente_ID = $('#cmb_sitios_clientes').val() == '' ? 0 : parseInt($('#cmb_sitios_clientes').val());
        Alerta.Vehiculo_ID = $('#cmb_vehiculos').val() == '' ? 0 : parseInt($('#cmb_vehiculos').val());
        Alerta.Area_ID = $('#cmb_areas').val() == '' ? 0 : parseInt($('#cmb_areas').val());
        Alerta.Turno_ID = $('#cmb_turnos').val() == '' ? 0 : parseInt($('#cmb_turnos').val());
        Alerta.Numero_CAR = $('#txt_numero_car').val();
        //Checkboxes
        Alerta.Circunstancia_1 = $('#cb_circunstancia_1').attr('checked') ? true : false;
        Alerta.Circunstancia_2 = $('#cb_circunstancia_2').attr('checked') ? true : false;
        Alerta.Circunstancia_3 = $('#cb_circunstancia_3').attr('checked') ? true : false;
        Alerta.Circunstancia_4 = $('#cb_circunstancia_4').attr('checked') ? true : false;
        Alerta.Circunstancia_5 = $('#cb_circunstancia_5').attr('checked') ? true : false;
        Alerta.Circunstancia_6 = $('#cb_circunstancia_6').attr('checked') ? true : false;
        Alerta.Circunstancia_7 = $('#cb_circunstancia_7').attr('checked') ? true : false;
        Alerta.Circunstancia_8 = $('#cb_circunstancia_8').attr('checked') ? true : false;
        //Descripcion
        Alerta.Descripcion_1 = $('#txt_descripcion_1').val();
        Alerta.Descripcion_2 = $('#txt_descripcion_2').val();
        Alerta.Descripcion_3 = $('#txt_descripcion_3').val();
        Alerta.Descripcion_4 = $('#txt_descripcion_4').val();
        Alerta.Descripcion_5 = $('#txt_descripcion_5').val();
        Alerta.Descripcion_6 = $('#txt_descripcion_6').val();
        Alerta.Descripcion_7 = $('#txt_descripcion_7').val();
        Alerta.Condicion_Buena = $('#txt_condicion_buena').val();
        Alerta.Condicion_Mala = $('#txt_condicion_mala').val();

        //Detalles
        Alerta.Datos_Detalles_Correos = JSON.stringify($('#tbl_lista_destinatarios').bootstrapTable('getData'));
        Alerta.Datos_Detalles_Gerentes = JSON.stringify($('#tbl_lista_gerentes').bootstrapTable('getData'));
        Alerta.Datos_Detalles_Directores = JSON.stringify($('#tbl_lista_directores').bootstrapTable('getData'));
        Alerta.Datos_Detalles_Anexos = JSON.stringify($('#tbl_lista_anexos').bootstrapTable('getData'));

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Guardar_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                        _inicializar_pagina();
                        _limpiar_controles();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}
function parseDateIntercambiarDiaMes(dateString) {
    //Intercambia el dia y el mes de los formatos de fecha( DD/MM/YYYY o MM/DD/YYYY )
    var dateTime = dateString.split(" ");
    var dateOnly = dateTime[0];
    var dates = dateOnly.split("/");
    var temp = dates[1] + "/" + dates[0] + "/" + dates[2];
    return temp;
}
function _search_alertas_rojas_por_filtros() {
    var filtros = null;
    try {
        filtros = new Object();

        if ($.trim($('#txt_busqueda_por_no_alerta').val()) !== '')
            filtros.No_Alerta_Roja = parseInt($('#txt_busqueda_por_no_alerta').val());
        if ($.trim($('#txt_busqueda_por_no_parte').val()) !== '')
            filtros.Numero_Parte = $('#txt_busqueda_por_no_parte').val();
        if ($.trim($('#txt_busqueda_por_no_car').val()) !== '')
            filtros.Numero_CAR = $('#txt_busqueda_por_no_car').val();
        if ($('#cmb_busqueda_por_estatus :selected').val() !== undefined && $('#cmb_busqueda_por_estatus').val() !== '')
            filtros.Estatus_ID = parseInt($('#cmb_busqueda_por_estatus :selected').val());

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Alertas_Rojas',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_alertas_rojas').bootstrapTable('load', JSON.parse(datos.d));
                    _agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _crear_tbl_alertas_rojas() {
    try {
        $('#tbl_alertas_rojas').bootstrapTable('destroy');
        $('#tbl_alertas_rojas').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,
            columns: [
                { field: 'No_Alerta_Roja', title: 'Red Alert Number', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Planta', title: 'AAM Plant', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Unidad_Negocio', title: 'Bussines Unit', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Numero_Parte', title: 'Part Number', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                {
                    field: 'Estatus', title: 'Status', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {
                        var color = '';
                        switch (value) {
                            case 'CANCELADO':
                                color = "rgba(221, 24, 24, 1)";
                                break;
                            case 'ABIERTO':
                                color = "yellow";
                                break;
                            case 'PROCESO':
                                color = 'rgba(85, 171, 237, 1)';
                                break;
                            case 'VENCIDO':
                                color = "orange";
                                break;
                            case 'FINALIZADO':
                                color = "green";
                                break;
                            case 'RECHAZADO':
                                color = "black";
                                break;
                            case 'AUTORIZADO':
                                color = 'rgba(3, 251,16, 1)';
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }
                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },
                {
                    field: 'Modificar', title: '', align: 'center', width: '3%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _editar_cambio" id="remove_' + row.No_Alerta_Roja + '" href="javascript:void(0)" data-alerta_roja=\'' + JSON.stringify(row) +
                                    '\' onclick="btn_editar_click(this);"><i class="glyphicon glyphicon-duplicate" title=""></i></a>' +
                               '</div>';
                    }
                },
                {
                    field: 'Cancelado', title: '', align: 'center', width: '3%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit _editar_cancelar" id="remove_' + row.No_Alerta_Roja + '" href="javascript:void(0)" data-alerta_roja=\'' + JSON.stringify(row) +
                                    '\' onclick="btn_cancelar_click(this);"><i class="glyphicon glyphicon-remove" title=""></i></a>' +
                               '</div>';
                    }
                }
            ]
        });
    } catch (e) {

    }
}
function btn_editar_click(renglon) {
    var row = $(renglon).data('alerta_roja');
    if (row.Estatus == "CANCELADO" || row.Estatus == "VENCIDO" || row.Estatus == "FINALIZADO") {
        _mostrar_mensaje('Red Alert', 'Unable to edit information of the selected alert.');
        return;
    }
    _habilitar_controles('Modificar');
    _crear_tbl_destinatarios();
    _crear_tbl_anexos();
    _crear_tbl_gerentes();
    _crear_tbl_directores();
    $('#cmb_estatus').attr("disabled", false);
    var Alerta = null;

    try {
        _consultar_orden_cambios_proceso_correo(row.No_Cambio, row.No_Extension);
        _consultar_orden_cambios_proceso_gerentes(row.No_Cambio, row.No_Extension);
        _consultar_orden_cambios_proceso_directores(row.No_Cambio, row.No_Extension);
        _consultar_orden_cambios_proceso_anexos(row.No_Cambio, row.No_Extension);

        Alerta = new Object();
        Alerta.No_Alerta_Roja = row.No_Alerta_Roja;
        if(row.Producto_ID != null)
            Alerta.Producto_ID = row.Producto_ID;
        if(row.Cliente_ID != null)
            Alerta.Cliente_ID = row.Cliente_ID;
        if (row.Referencia_Producto_ID != null)
            Alerta.Referencia_Producto_ID = row.Referencia_Producto_ID;
        if(row.Sitio_Cliente_ID != null)
            Alerta.Sitio_Cliente_ID = row.Sitio_Cliente_ID;
        if(row.Vehiculo_ID != null)
            Alerta.Vehiculo_ID = row.Vehiculo_ID;
        if(row.Area_ID != null)
            Alerta.Area_ID = row.Area_ID;
        if(row.Turno_ID != null)
            Alerta.Turno_ID = row.Turno_ID;

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Una_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    $("#txt_no_alerta_roja").val(row.No_Alerta_Roja);
                    $('#txt_numero_parte').val(Resultado[0].Numero_Parte);
                    $('#txt_numero_car').val(Resultado[0].Numero_CAR);

                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: row.Estatus_ID, text: row.Estatus }] });
                    $("#cmb_estatus").val(row.Estatus_ID).trigger("change");

                    $("#cmb_plantas")[0].innerHTML = "";
                    $("#cmb_plantas").select2({ data: [{ id: row.Planta_ID, text: row.Planta }] });
                    $("#cmb_plantas").val(row.Planta_ID).trigger("change");

                    $("#cmb_unidades_negocio")[0].innerHTML = "";
                    $("#cmb_unidades_negocio").select2({ data: [{ id: row.Unidad_Negocio_ID, text: row.Unidad_Negocio }] });
                    $("#cmb_unidades_negocio").val(row.Unidad_Negocio_ID).trigger("change");

                    if (Resultado[0].Producto != "") {
                        $("#cmb_productos")[0].innerHTML = "";
                        $("#cmb_productos").select2({ data: [{ id: row.Producto_ID, text: Resultado[0].Producto }] });
                        $("#cmb_productos").val(row.Producto_ID).trigger("change");
                    }
                    if (Resultado[0].Cliente != "") {
                        $("#cmb_clientes")[0].innerHTML = "";
                        $("#cmb_clientes").select2({ data: [{ id: row.Cliente_ID, text: Resultado[0].Cliente }] });
                        $("#cmb_clientes").val(row.Cliente_ID).trigger("change");
                    }
                    if (Resultado[0].Referencia_Producto != "") {
                        $("#cmb_referencia_productos")[0].innerHTML = "";
                        $("#cmb_referencia_productos").select2({ data: [{ id: row.Referencia_Producto_ID, text: Resultado[0].Referencia_Producto }] });
                        $("#cmb_referencia_productos").val(row.Referencia_Producto_ID).trigger("change");
                    }
                    if (Resultado[0].Sitio_Cliente != "") {
                        $("#cmb_sitios_clientes")[0].innerHTML = "";
                        $("#cmb_sitios_clientes").select2({ data: [{ id: row.Sitio_Cliente_ID, text: Resultado[0].Sitio_Cliente }] });
                        $("#cmb_sitios_clientes").val(row.Sitio_Cliente_ID).trigger("change");
                    }
                    if (Resultado[0].Vehiculo != ""){
                        $("#cmb_vehiculos")[0].innerHTML = "";
                        $("#cmb_vehiculos").select2({ data: [{ id: row.Vehiculo_ID, text: Resultado[0].Vehiculo }] });
                        $("#cmb_vehiculos").val(row.Vehiculo_ID).trigger("change");
                    }
                    if(Resultado[0].Area!=""){
                        $("#cmb_areas")[0].innerHTML = "";
                        $("#cmb_areas").select2({ data: [{ id: row.Area_ID, text: Resultado[0].Area }] });
                        $("#cmb_areas").val(row.Area_ID).trigger("change");
                    }
                    if(Resultado[0].Turno!=""){
                        $("#cmb_turnos")[0].innerHTML = "";
                        $("#cmb_turnos").select2({ data: [{ id: row.Turno_ID, text: Resultado[0].Turno }] });
                        $("#cmb_turnos").val(row.Turno_ID).trigger("change");
                    }
                    $('#cb_circunstancia_1').attr('checked', Resultado[0].Circunstancia_1);
                    $('#cb_circunstancia_2').attr('checked', Resultado[0].Circunstancia_2);
                    $('#cb_circunstancia_3').attr('checked', Resultado[0].Circunstancia_3);
                    $('#cb_circunstancia_4').attr('checked', Resultado[0].Circunstancia_4);
                    $('#cb_circunstancia_5').attr('checked', Resultado[0].Circunstancia_5);
                    $('#cb_circunstancia_6').attr('checked', Resultado[0].Circunstancia_6);
                    $('#cb_circunstancia_7').attr('checked', Resultado[0].Circunstancia_7);
                    $('#cb_circunstancia_8').attr('checked', Resultado[0].Circunstancia_8);

                    $('#txt_descripcion_1').val(Resultado[0].Descripcion_1);
                    $('#txt_descripcion_2').val(Resultado[0].Descripcion_2);
                    $('#txt_descripcion_3').val(Resultado[0].Descripcion_3);
                    $('#txt_descripcion_4').val(Resultado[0].Descripcion_4);
                    $('#txt_descripcion_5').val(Resultado[0].Descripcion_5);
                    $('#txt_descripcion_6').val(Resultado[0].Descripcion_6);
                    $('#txt_descripcion_7').val(Resultado[0].Descripcion_7);
                    $('#txt_condicion_buena').val(Resultado[0].Condicion_Buena);
                    $('#txt_condicion_mala').val(Resultado[0].Condicion_Mala);

                    _load_plantas('#cmb_plantas');
                    _load_unidades_negocio('#cmb_unidades_negocio');
                    _load_productos('#cmb_productos');
                    _load_clientes('#cmb_clientes');
                    _load_referencia_productos('#cmb_referencia_productos');
                    _load_sitios_cliente('#cmb_sitios_clientes');
                    _load_vehiculos('#cmb_vehiculos');
                    _load_areas('#cmb_areas');
                    _load_turnos('#cmb_turnos');
                    _load_estatus('#cmb_estatus');

                    _consultar_departamentos('#cmb_departamento');
                    _consultar_departamentos('#cmb_departamento_iniciador');
                    _consultar_departamentos('#cmb_departamento_gerentes');
                    _consultar_departamentos('#cmb_departamento_directores');
                    _consultar_usuarios('#cmb_lista_usuario');
                    _consultar_Directores('#cmb_lista_directores');
                    _consultar_Gerentes('#cmb_lista_empleados');

                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _habilitar_controles(opcion) {
    $('#div_principal_alertas_rojas').css('display', 'none');
    $('#div_Informacion').css('display', 'none');
    switch (opcion) {
        case 'Inicial':
            $('#div_principal_alertas_rojas').css('display', 'block');
            break;
        case 'Nuevo':
            $('#div_Informacion').css('display', 'block');
            //$('#tbl_alertas_rojas').bootstrapTable('hideColumn', 'Modificar');
            break;
        case 'Modificar':
            $('#div_Informacion').css('display', 'block');
            //$('#tbl_alertas_rojas').bootstrapTable('hideColumn', 'Modificar');
            break;
        case 'Visualizar':
            $('#div_Informacion').css('display', 'block');
            //$('#tbl_alertas_rojas').bootstrapTable('hideColumn', 'Modificar');
            $("#btn_salir").css('display', 'none');
            $("#btn_guardar").css('display', 'none');
            $("#btn_regresar").css('display', 'block');
            break;
    }
}
function _consultar_orden_cambios_proceso_correo(no_orden, no_extension) {
    var filtros = null;
    try {
        filtros = new Object();
        filtros.No_Cambio = parseInt(no_orden);
        filtros.No_Extension = parseInt(no_extension);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Correo',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_destinatarios').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });
    } catch (e) {

    }
}
function _consultar_orden_cambios_proceso_gerentes(no_orden, no_extension) {
    var filtros = null;
    try {
        filtros = new Object();
        filtros.No_Cambio = parseInt(no_orden);
        filtros.No_Extension = parseInt(no_extension);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Gerentes',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_gerentes').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });
    } catch (e) {

    }
}
function _consultar_orden_cambios_proceso_directores(no_orden, no_extension) {
    var filtros = null;
    try {
        filtros = new Object();
        filtros.No_Cambio = parseInt(no_orden);
        filtros.No_Extension = parseInt(no_extension);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Directores',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_directores').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });
    } catch (e) {

    }
}
function _consultar_orden_cambios_proceso_anexos(no_orden, no_extension) {
    var filtros = null;
    try {
        filtros = new Object();
        filtros.No_Cambio = parseInt(no_orden);
        filtros.No_Extension = parseInt(no_extension);
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Anexos',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_lista_anexos').bootstrapTable('load', JSON.parse(datos.d));
                }
            }
        });
    } catch (e) {

    }
}
function _actualizar_alerta_roja() {
    var Alerta = null;
    try {
        Alerta = new Object();
        Alerta.No_Alerta_Roja = parseInt($('#txt_no_alerta_roja').val());
        Alerta.Estatus_ID = $('#cmb_estatus').val() == '' ? 0 : parseInt($('#cmb_estatus').val());
        Alerta.Planta_ID = $('#cmb_plantas').val() == '' ? 0 : parseInt($('#cmb_plantas').val());
        Alerta.Unidad_Negocio_ID = $('#cmb_unidades_negocio').val() == '' ? 0 : parseInt($('#cmb_unidades_negocio').val());
        Alerta.Producto_ID = $('#cmb_productos').val() == '' ? 0 : parseInt($('#cmb_productos').val());
        Alerta.Numero_Parte = $('#txt_numero_parte').val();
        Alerta.Cliente_ID = $('#cmb_clientes').val() == '' ? 0 : parseInt($('#cmb_clientes').val());
        Alerta.Referencia_Producto_ID = $('#cmb_referencia_productos').val() == '' ? 0 : parseInt($('#cmb_referencia_productos').val());
        Alerta.Sitio_Cliente_ID = $('#cmb_sitios_clientes').val() == '' ? 0 : parseInt($('#cmb_sitios_clientes').val());
        Alerta.Vehiculo_ID = $('#cmb_vehiculos').val() == '' ? 0 : parseInt($('#cmb_vehiculos').val());
        Alerta.Area_ID = $('#cmb_areas').val() == '' ? 0 : parseInt($('#cmb_areas').val());
        Alerta.Turno_ID = $('#cmb_turnos').val() == '' ? 0 : parseInt($('#cmb_turnos').val());
        Alerta.Numero_CAR = $('#txt_numero_car').val();
        //Checkboxes
        Alerta.Circunstancia_1 = $('#cb_circunstancia_1').attr('checked') ? true : false;
        Alerta.Circunstancia_3 = $('#cb_circunstancia_3').attr('checked') ? true : false;
        Alerta.Circunstancia_4 = $('#cb_circunstancia_4').attr('checked') ? true : false;
        Alerta.Circunstancia_5 = $('#cb_circunstancia_5').attr('checked') ? true : false;
        Alerta.Circunstancia_6 = $('#cb_circunstancia_6').attr('checked') ? true : false;
        Alerta.Circunstancia_7 = $('#cb_circunstancia_7').attr('checked') ? true : false;
        Alerta.Circunstancia_8 = $('#cb_circunstancia_8').attr('checked') ? true : false;
        Alerta.Circunstancia_2 = $('#cb_circunstancia_2').attr('checked') ? true : false;
        //Descripcion
        Alerta.Descripcion_1 = $('#txt_descripcion_1').val();
        Alerta.Descripcion_2 = $('#txt_descripcion_2').val();
        Alerta.Descripcion_3 = $('#txt_descripcion_3').val();
        Alerta.Descripcion_4 = $('#txt_descripcion_4').val();
        Alerta.Descripcion_5 = $('#txt_descripcion_5').val();
        Alerta.Descripcion_6 = $('#txt_descripcion_6').val();
        Alerta.Descripcion_7 = $('#txt_descripcion_7').val();
        Alerta.Condicion_Buena = $('#txt_condicion_buena').val();
        Alerta.Condicion_Mala = $('#txt_condicion_mala').val();

        //Detalles
        Alerta.Datos_Detalles_Correos = JSON.stringify($('#tbl_lista_destinatarios').bootstrapTable('getData'));
        Alerta.Datos_Detalles_Gerentes = JSON.stringify($('#tbl_lista_gerentes').bootstrapTable('getData'));
        Alerta.Datos_Detalles_Directores = JSON.stringify($('#tbl_lista_directores').bootstrapTable('getData'));
        Alerta.Datos_Detalles_Anexos = JSON.stringify($('#tbl_lista_anexos').bootstrapTable('getData'));

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Actualizar_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                var Resultado = JSON.parse($result.d);

                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                        _inicializar_pagina();
                        _limpiar_controles();
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje); }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _consultar_Gerentes(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar gerentes',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Datos_Gerentes',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $('#cmb_lista_empleados').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_lista_empleados :selected').val() !== '' || $('#cmb_lista_empleados :selected').val() !== undefined) {
                var Destinatario = new Object();
                Destinatario.Empleado_ID = parseInt($('#cmb_lista_empleados :selected').val());

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Destinatario) });
                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Datos_Empleados',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        var data = JSON.parse(datos.d)[0];
                        if (data !== null && data !== undefined) {
                            $('#txt_correo_gerentes').val(data.Email);
                        }
                    }
                });
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _consultar_Directores(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar directores',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Datos_Directores',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $('#cmb_lista_directores').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_lista_directores :selected').val() !== '' || $('#cmb_lista_directores :selected').val() !== undefined) {
                var Destinatario = new Object();
                Destinatario.Empleado_ID = parseInt($('#cmb_lista_directores :selected').val());

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Destinatario) });
                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Datos_Empleados',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        var data = JSON.parse(datos.d)[0];
                        if (data !== null && data !== undefined) {
                            $('#txt_correo_directores').val(data.Email);
                        }
                    }
                });
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _validar_datos_requeridos_agregar_gerentes() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';

        if ($('#cmb_lista_empleados').val() == '' || $('#cmb_lista_empleados').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione un gerente.<br />';
        }
        if ($('#cmb_departamento_gerentes :selected').val() == '' || $('#cmb_departamento_gerentes :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;El departamento es un dato requerido.<br />';
        }
        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    } finally {
        return _output;
    }
}
function _validar_no_duplicar_gerentes(id) {
    var _gerentes_agregados = null;
    var _gerentes_existes_tabla = false;
    try {
        _gerentes_agregados = $("#tbl_lista_gerentes").bootstrapTable('getData');
        $.each(_gerentes_agregados, function (index, value) {
            if (value.Empleado_ID == id)
                _gerentes_existes_tabla = true;
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _gerentes_existes_tabla;
}
function _validar_no_duplicar_departamento_gerentes(id) {
    var _destinatarios_agregados = null;
    var _destinatarios_existes_tabla = false;
    try {
        _destinatarios_agregados = $("#tbl_lista_gerentes").bootstrapTable('getData');
        $.each(_destinatarios_agregados, function (index, value) {

            if (value.Departamento_ID == id)
                _destinatarios_existes_tabla = true;
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _destinatarios_existes_tabla;
}
function _agregar_lista_gerentes() {
    try {
        $('#tbl_lista_gerentes').bootstrapTable('insertRow', {
            index: 0,
            row: {
                Departamento_ID: $('#cmb_departamento_gerentes :selected').val(),
                Departamento: $('#cmb_departamento_gerentes :selected').text(),
                Empleado_ID: $('#cmb_lista_empleados :selected').val(),
                Empleado: $('#cmb_lista_empleados').text(),
                //Aprobado: $('#txt_aprobado').val(),
                Email: $('#txt_correo_gerentes').val()
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _agregar_gerentes() {
    try {
        var valida_datos_requerido = _validar_datos_requeridos_agregar_gerentes();
        if (valida_datos_requerido.Estatus) {
            if (!_validar_no_duplicar_departamento_gerentes($('#cmb_departamento_gerentes :selected').val())) {
                if (!_validar_no_duplicar_gerentes($('#cmb_lista_empleados :selected').val())) {
                    _agregar_lista_gerentes();
                    _limpiar_datos_gerentes();
                } else {
                    _mostrar_mensaje('Información', 'El gerente que desea agregar ya se encuentra en la lista de los gerentes.');
                }
            } else {
                _mostrar_mensaje('Información', 'El departamento que desea agregar ya se encuentra en la lista de los gerentes.');
            }
        } else {
            _mostrar_mensaje('Información', valida_datos_requerido.Mensaje);
        }
    } catch (ex) {
        _mostrar_mensaje('Error Técnico', ex.message);
    }
}
function _limpiar_datos_gerentes() {
    $('#txt_correo_gerentes').val('');
    $("#cmb_lista_empleados").empty().trigger("change");
    $("#cmb_departamento_gerentes").empty().trigger("change");
}
function _crear_tbl_gerentes() {
    try {
        $('#tbl_lista_gerentes').bootstrapTable('destroy');
        $('#tbl_lista_gerentes').bootstrapTable({
            idField: 'Empleado_ID',
            uniqueId: 'Empleado_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Departamento_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Departamento', title: 'Departamento', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Empleado_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Empleado', title: 'Gerentes', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Aprobado', title: 'Aprobado', align: 'center', valign: 'bottom', sortable: true, visible: true },
                { field: 'Motivo_Rechazo', title: 'Motivo Rechazo', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Email', title: 'Email', align: 'left', valign: 'bottom', sortable: true, visible: false },
                {
                    field: 'eliminar', title: 'Quitar', align: 'center', valign: 'bottom', formatter: function (value, row) {
                        return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-gerentes=\'' + JSON.stringify(row) + '\' onclick="_quitar_gerentes(this);"><i class="glyphicon glyphicon-remove"></i></a></div>';
                    }
                }
            ],
            onLoadSuccess: function (data) {
            }
        });
    } catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _quitar_gerentes(gerentes) {
    var indexrow = null;
    var row = $(gerentes).data('gerentes');

    try {
        $('#tbl_lista_gerentes').bootstrapTable('remove', {
            field: 'Empleado',
            values: [$.trim(row.Empleado.toString())]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _validar_datos_requeridos_agregar_directores() {
    var _output = new Object();

    try {
        _output.Estatus = true;
        _output.Mensaje = '';

        if ($('#cmb_lista_directores').val() == '' || $('#cmb_lista_directores').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;<i class="fa fa-angle-double-right"></i>&nbsp;Seleccione un director.<br />';
        }
        if ($('#cmb_departamento_directores :selected').val() == '' || $('#cmb_departamento_directores :selected').val() == undefined) {
            _output.Estatus = false;
            _output.Mensaje += '&nbsp;-&nbsp;El departamento es un dato requerido.<br />';
        }
        if (_output.Mensaje != "")
            _output.Mensaje = "Favor de proporcionar lo siguiente: <br />" + _output.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    } finally {
        return _output;
    }
}
function _validar_no_duplicar_directores(id) {
    var _directores_agregados = null;
    var _directores_existes_tabla = false;
    try {
        _directores_agregados = $("#tbl_lista_directores").bootstrapTable('getData');
        $.each(_directores_agregados, function (index, value) {
            if (value.Empleado_ID == id)
                _directores_existes_tabla = true;
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _directores_existes_tabla;
}
function _validar_no_duplicar_departamento_directores(id) {
    var _destinatarios_agregados = null;
    var _destinatarios_existes_tabla = false;
    try {
        _destinatarios_agregados = $("#tbl_lista_directores").bootstrapTable('getData');
        $.each(_destinatarios_agregados, function (index, value) {

            if (value.Departamento_ID == id)
                _destinatarios_existes_tabla = true;
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
    return _destinatarios_existes_tabla;
}
function _agregar_lista_directores() {
    try {
        $('#tbl_lista_directores').bootstrapTable('insertRow', {
            index: 0,
            row: {
                Departamento_ID: $('#cmb_departamento_directores :selected').val(),
                Departamento: $('#cmb_departamento_directores :selected').text(),
                Empleado_ID: $('#cmb_lista_directores :selected').val(),
                Empleado: $('#cmb_lista_directores').text(),
                //Aprobado: $('#txt_aprobado_directores').val(),
                Email: $('#txt_correo_directores').val()
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _agregar_directores() {
    try {
        var valida_datos_requerido = _validar_datos_requeridos_agregar_directores();
        if (valida_datos_requerido.Estatus) {
            if (!_validar_no_duplicar_departamento_directores($('#cmb_departamento_directores :selected').val())) {
                if (!_validar_no_duplicar_directores($('#cmb_lista_directores :selected').val())) {
                    _agregar_lista_directores();
                    _limpiar_datos_directores();
                } else {
                    _mostrar_mensaje('Información', 'El director que desea agregar ya se encuentra en la lista de los directores.');
                }
            } else {
                _mostrar_mensaje('Información', 'El departamento que desea agregar ya se encuentra en la lista de los directores.');
            }
        } else {
            _mostrar_mensaje('Información', valida_datos_requerido.Mensaje);
        }
    } catch (ex) {
        _mostrar_mensaje('Error Técnico', ex.message);
    }
}
function _limpiar_datos_directores() {
    $('#txt_correo_directores').val('');
    $("#cmb_lista_directores").empty().trigger("change");
    $("#cmb_departamento_directores").empty().trigger("change");
}
function _crear_tbl_directores() {
    try {
        $('#tbl_lista_directores').bootstrapTable('destroy');
        $('#tbl_lista_directores').bootstrapTable({
            idField: 'Empleado_ID',
            uniqueId: 'Empleado_ID',
            method: 'POST',
            async: false,
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: false,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            editable: true,
            showFooter: true,
            columns: [
                { field: 'Departamento_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Departamento', title: 'Departamento', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Empleado_ID', title: '', align: 'center', valign: 'bottom', sortable: true, visible: false },
                { field: 'Empleado', title: 'Gerentes', align: 'left', valign: 'bottom', sortable: true, visible: true },
                { field: 'Aprobado', title: 'Aprobado', align: 'center', valign: 'bottom', sortable: true, visible: true },
                { field: 'Motivo_Rechazo', title: 'Motivo Rechazo', align: 'center', valign: 'bottom', sortable: true, visible: true },
                { field: 'Email', title: 'Email', align: 'left', valign: 'bottom', sortable: true, visible: false },
                {
                    field: 'eliminar', title: 'Quitar', align: 'center', valign: 'bottom', formatter: function (value, row) {
                        return '<div><a class="remove ml10" id="' + value + '" href="javascript:void(0)"  data-directores=\'' + JSON.stringify(row) + '\' onclick="_quitar_directores(this);"><i class="glyphicon glyphicon-remove"></i></a></div>';
                    }
                }
            ],
            onLoadSuccess: function (data) {
            }
        });
    } catch (e) {
        _mostrar_mensaje('Error Técnico', 'Error al crear la tabla');
    }
}
function _quitar_directores(directores) {
    var indexrow = null;
    var row = $(directores).data('directores');

    try {
        $('#tbl_lista_directores').bootstrapTable('remove', {
            field: 'Empleado',
            values: [$.trim(row.Empleado.toString())]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _agregar_tooltip_tabla() {
    _init_tbl_btn_config('._editar_cambio', 'Edit Alert', 'bottomLeft');
    _init_tbl_btn_config('._editar_cancelar', 'Cancel Alert', 'bottomLeft');
}
function _init_tbl_btn_config(classCss, text, alineacionTooltip) {
    $(classCss).each(function () {
        $(this).qtip({
            content: text,
            position: {
                corner: {
                    target: 'topMiddle',
                    tooltip: alineacionTooltip
                }
            },
            show: {
                when: { event: 'mouseover' },
                ready: false
            },
            hide: { event: 'mouseout' },
            style: {
                border: {
                    width: 5,
                    radius: 7
                },
                padding: 5,
                textAlign: 'center',
                tip: {
                    corner: true,
                    method: "polygon",
                    border: 1,
                    height: 20,
                    width: 9
                },
                background: '#F5F6CE',
                color: '#2d2d30',
                width: 200,
                'font-size': 'small',
                'font-family': 'Calibri',
                'font-weight': 'Bold',
                tip: true,
                name: 'blue'
            }
        });
    });
}
function _lista_departamentos() {
    var filtros = null;
    var datos_emp;
    try {

        filtros = new Object();
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_lista_departamentos',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {

                    datos_emp = JSON.parse(datos.d);


                }

            }
        });
        return datos_emp.Mensaje;
    } catch (e) {

    }
}
function _lista_gerentes() {
    var filtros = null;
    var datos_emp;
    try {
        filtros = new Object();
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_lista_gerentes',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_emp = JSON.parse(datos.d);
                }
            }
        });
        return datos_emp.Mensaje;
    } catch (e) {

    }
}
function _lista_directores() {
    var filtros = null;
    var datos_emp;
    try {
        filtros = new Object();
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_lista_directores',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_emp = JSON.parse(datos.d);
                }
            }
        });
        return datos_emp.Mensaje;
    } catch (e) {

    }
}
function _cargar_tabla() {
    try {
        $('#tbl_anexos_responsables').bootstrapTable('destroy');
        $('#tbl_anexos_responsables').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,
            toggle: 'tbl_anexos_responsables',
            detailView: true,
            detailFormatter: "detailFormatter",
            columns: [
                { field: 'Empleado_ID', title: '', width: 0, align: 'center', valign: 'bottom', sortable: false, visible: false },
                { field: 'Departamento_ID', title: '', width: 0, align: 'center', valign: 'bottom', sortable: false, visible: false },
                { field: 'Departamento', title: 'Departamento', width: 200, align: 'left', valign: 'bottom', sortable: false },
                { field: 'Empleado', title: 'Responsables', width: 0, align: 'center', valign: 'bottom', sortable: false, visible: true }
            ]
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function detailFormatter(index, row) {
    var html = [];
    var $data;
    var filtros = new Object();
    filtros.Empleado_ID = row.Empleado_ID;
    $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
    $.ajax({
        type: 'POST',
        url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Anexos_Hijos',
        data: $data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        cache: false,
        success: function (result) {
            Datos = JSON.parse(result.d);
            if (Datos != null) {
                var largo = 0;
                largo = $(window).width() / 3;
                largo = largo * 2.5;
                //creamos el encabezado
                if (Datos.length == 0) {
                    html.push('<div id="sub" class="alert alert-info" role="alert"><i class="fa fa-info-circle" style="color:black;">&nbsp;No contiene anexos</i></div>');
                } else {
                    html.push('<center><table class="table table-responsive" style="width: ' + largo + 'px;  ">');
                    html.push('<tr>');
                    html.push('<td class="hidden" style="border: solid 1px #dddddd; background-color:#607D8B;"><b><font color="white">ID</font></b></td>');
                    html.push('<td style="border: solid 1px #dddddd; background-color:#607D8B;"><b><font color="white">Nombre</font></b></td>');
                    html.push('<td style="border: solid 1px #dddddd; background-color:#607D8B;"><b><font color="white">URL</font></b></td>');
                    html.push('<td style="border: solid 1px #dddddd; background-color:#607D8B;"></td>');
                    html.push('</tr>');
                    $.each(Datos, function (key, value) {
                        html.push('<tr>');
                        html.push('<td class="hidden" style="text-align:left; border: solid 1px #dddddd;">' + value.Empleado_ID + '</td>');
                        html.push('<td onclick="" style="text-align:left; border: solid 1px #dddddd;">' + value.Nombre + '</td>');
                        html.push('<td onclick="" style="text-align:left; border: solid 1px #dddddd;">' + value.URL_Archivo + '</td>');
                        html.push('<td onclick="" style="text-align:center; border: solid 1px #dddddd;">' + '<div> ' +
                            '<a class="remove ml10 edit" id="' + value.Empleado_ID + '" href="javascript:void(0)" data-archivo=\'' + JSON.stringify(value) + '\' onclick="_ver_archivos_responsables(this);" title="Ver"><i class="glyphicon glyphicon-eye-open"></i></button>' +
                            '</div>' + '</td>');
                        html.push('</tr>');
                    });
                    html.push('</table></center>');
                }
            }
        }
    });
    return html.join('');
}
function _ver_archivos_responsables(archivo) {
    var indexrow = null;
    var row = $(archivo).data('archivo');
    var Ruta = '';
    var Nombre = '';
    try {
        Ruta = row.URL_Archivo;
        Nombre = row.Nombre;
        //window.open(Ruta);
        window.open(Ruta, Nombre, 'toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _search(No_Cambio, No_Extension) {
    var filtros = null;
    try {
        show_loading_bar({
            pct: 78,
            wait: .5,
            delay: .5,
            finish: function (pct) {
                filtros = new Object();
                filtros.No_Cambio = parseInt(No_Cambio);
                filtros.No_Extension = parseInt(No_Extension);

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
                jQuery.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Anexos_Padres',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null) {
                            $('#tbl_anexos_responsables').bootstrapTable('load', JSON.parse(datos.d));
                            hide_loading_bar();
                        }
                    }
                });
            }
        });
    } catch (e) {

    }
}
function btn_cancelar_click(renglon) {
    var row = $(renglon).data('alerta_roja');
    if (row.Estatus.toUpperCase() == "FINALIZADO") {
        _mostrar_mensaje('Validation', 'Unable to cancel a finished alert ');
        return;
    }
    if (row.Estatus.toUpperCase() == "CANCELADO") {
        _mostrar_mensaje('Validation', 'You cannot calcel an alert already canceled ');
        return;
    }

    if (row.Estatus.toUpperCase() == "RECHAZADO") {
        _mostrar_mensaje('Validation', 'You cannot calcel an alert already refused ');
        return;
    }
    if (row.Estatus.toUpperCase() == "AUTORIZADO") {
        _mostrar_mensaje('Validation', 'You cannot cancel an alert already authorized')
        return;
    }
    bootbox.confirm({
        title: 'Red Alert',
        message: 'Are you sure to cancel the alert?',
        callback: function (result) {
            if (result) {
                //validar si es usuario
                //var usuario = _valida_usuarios();
                //if (usuario == true) {
                //    _mostrar_mensaje('Validation Report', 'Invalid user: unable to cancel the alert.');
                //    return;
                //}
                _cancelar_alerta_roja(row.No_Alerta_Roja);
                _mostrar_mensaje("Validation Report", "Red Alert successfully canceled.");
            }
        }
    }); 
}
function _cancelar_alerta_roja(no_alerta_roja) {
    var Alerta = null;
    var isComplete = false;
    try {
        Alerta = new Object();
        Alerta.No_Alerta_Roja = parseInt(no_alerta_roja);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Cancelar_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;
}
function _valida_usuarios() {
    var filtros = null;
    var datos_emp;
    try {
        filtros = new Object();
        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Validar_Usuario',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_emp = JSON.parse(datos.d);
                }
            }
        });
        return datos_emp.Mensaje;
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_estatus(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Plant',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Estatus',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_plantas(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Plant',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Plantas',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $('#cmb_plantas').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_plantas :selected').val() !== '' || $('#cmb_plantas :selected').val() !== undefined) {
                var planta = new Object();
                planta.Planta_ID = parseInt($('#cmb_plantas :selected').val());
                //$('#txt_no_empleado').val('');
                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(planta) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Unidades_Negocio_Filtro',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null && datos !== undefined) {
                            datos_proceso = JSON.parse(datos.d);

                            $("#cmb_unidades_negocio").select2("trigger", "select", {
                                data: { id: datos_proceso[0].Unidad_Negocio_ID, text: datos_proceso[0].Nombre }
                            });
                        }
                    }
                });
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_unidades_negocio(cmb) { 
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Bussines Unit',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Unidades_Negocio',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_productos(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Product',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Productos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_turnos(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Related Shift',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Turnos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_areas(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Area',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Areas',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_vehiculos(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Vehicle',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Vehiculos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_sitios_cliente(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Customer Site',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Sitios_Clientes',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $('#cmb_sitios_clientes').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_sitios_clientes :selected').val() !== '' || $('#cmb_sitios_clientes :selected').val() !== undefined) {
                var sitio_cliente = new Object();
                sitio_cliente.Sitio_Cliente_ID = parseInt($('#cmb_sitios_clientes :selected').val());
                //$('#txt_no_empleado').val('');
                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(sitio_cliente) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Vehiculos_Filtro',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null && datos !== undefined) {
                            datos_proceso = JSON.parse(datos.d);

                            $("#cmb_vehiculos").select2("trigger", "select", {
                                data: { id: datos_proceso[0].Vehiculo_ID, text: datos_proceso[0].Nombre }
                            });
                        }
                    }
                });
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_referencia_productos(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Reference Product',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Referencia_Productos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _load_clientes(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Search Customer',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Clientes',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $('#cmb_clientes').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_clientes :selected').val() !== '' || $('#cmb_cientes :selected').val() !== undefined) {
                var cliente = new Object();
                cliente.Cliente_ID = parseInt($('#cmb_clientes :selected').val());
                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(cliente) });

                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Referencia_Productos_Filtro',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null && datos !== undefined) {
                            datos_proceso = JSON.parse(datos.d);

                            $("#cmb_referencia_productos").select2("trigger", "select", {
                                data: { id: datos_proceso[0].Referencia_Producto_ID, text: datos_proceso[0].Nombre }
                            });
                        }
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Sitios_Clientes_Filtro',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        if (datos !== null && datos !== undefined) {
                            datos_proceso = JSON.parse(datos.d);

                            $("#cmb_sitios_clientes").select2("trigger", "select", {
                                data: { id: datos_proceso[0].Sitio_Cliente_ID, text: datos_proceso[0].Nombre }
                            });
                        }
                    }
                });
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _consultar_usuarios(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar responsables',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Responsables',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
        $('#cmb_lista_usuario').on("select2:select", function (evt) {
            var id = evt.params.data.id;

            if ($('#cmb_lista_usuario :selected').val() !== '' || $('#cmb_lista_usuario :selected').val() !== undefined) {
                var Destinatario = new Object();
                Destinatario.Empleado_ID = parseInt($('#cmb_lista_usuario :selected').val());

                var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Destinatario) });
                $.ajax({
                    type: 'POST',
                    url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Datos_Empleados',
                    data: $data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    cache: false,
                    success: function (datos) {
                        var data = JSON.parse(datos.d)[0];
                        if (data !== null && data !== undefined) {
                            $('#txt_correo').val(data.Email);
                        }
                    }
                });
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _consultar_departamentos(cmb) {
    try {
        $(cmb).select2({
            language: "es",
            theme: "classic",
            placeholder: 'Buscar departamentos',
            allowClear: true,
            ajax: {
                url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Consultar_Departamentos',
                cache: "true",
                dataType: 'json',
                type: "POST",
                delay: 250,
                cache: true,
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                quietMillis: 100,
                results: function (data) {
                    return { results: data };
                },
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
        });
    } catch (e) {
        _mostrar_mensaje('Informe Técnico', e);
    }
}
function _search_alerta_roja() {
    var filtros = null;
    try {
        filtros = new Object();
        filtros.No_Alerta_Roja = noalerta;
        

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });

        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Alertas_Rojas_Controller.asmx/Ver_Alerta',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    datos_emp = JSON.parse(datos.d);

                    //Colocar los datos en los controles
                    _habilitar_controles('Visualizar');
                    _crear_tbl_destinatarios();
                    _crear_tbl_anexos();
                    _crear_tbl_gerentes();
                    _crear_tbl_directores();
                    _cargar_tabla();

                    

                    

                    $("#txt_no_alerta_roja").val(parseInt(datos_emp[0].No_Alerta_Roja));
                   
                    $("#cmb_plantas")[0].innerHTML = "";
                    $("#cmb_plantas").select2({ data: [{ id: datos_emp[0].Planta_ID, text: datos_emp[0].Planta }] });
                    $("#cmb_plantas").val(datos_emp[0].Planta_ID).trigger("change");

                    $("#cmb_unidades_negocio")[0].innerHTML = "";
                    $("#cmb_unidades_negocio").select2({ data: [{ id: datos_emp[0].Unidad_Negocio_ID, text: datos_emp[0].Unidad_Negocio }] });
                    $("#cmb_unidades_negocio").val(datos_emp[0].Unidad_Negocio_ID).trigger("change");

                    $("#cmb_productos")[0].innerHTML = "";
                    $("#cmb_productos").select2({ data: [{ id: datos_emp[0].Producto_ID, text: datos_emp[0].Producto}] });
                    $("#cmb_productos").val(datos_emp[0].Producto_ID).trigger("change");
                  
                    $("#txt_numero_parte").val(datos_emp[0].Numero_Parte);

                    $("#cmb_clientes")[0].innerHTML = "";
                    $("#cmb_clientes").select2({ data: [{ id: datos_emp[0].Cliente_ID, text: datos_emp[0].Cliente }] });
                    $("#cmb_clientes").val(datos_emp[0].Cliente_ID).trigger("change");

                    $("#cmb_referencia_productos")[0].innerHTML = "";
                    $("#cmb_referencia_productos").select2({ data: [{ id: datos_emp[0].Referencia_Producto_ID, text: datos_emp[0].Referencia_Producto }] });
                    $("#cmb_referencia_productos").val(datos_emp[0].Referencia_Producto_ID).trigger("change");

                    $("#cmb_sitios_clientes")[0].innerHTML = "";
                    $("#cmb_sitios_clientes").select2({ data: [{ id: datos_emp[0].Sitio_Cliente_ID, text: datos_emp[0].Sitio_Cliente }] });
                    $("#cmb_sitios_clientes").val(datos_emp[0].Sitio_Cliente_ID).trigger("change");

                    $("#cmb_vehiculos")[0].innerHTML = "";
                    $("#cmb_vehiculos").select2({ data: [{ id: datos_emp[0].Vehiculo_ID, text: datos_emp[0].Vehiculo }] });
                    $("#cmb_vehiculos").val(datos_emp[0].Vehiculo_ID).trigger("change");

                    $("#cmb_areas")[0].innerHTML = "";
                    $("#cmb_areas").select2({ data: [{ id: datos_emp[0].Area_ID, text: datos_emp[0].Area }] });
                    $("#cmb_areas").val(datos_emp[0].Area_ID).trigger("change");

                    $("#cmb_turnos")[0].innerHTML = "";
                    $("#cmb_turnos").select2({ data: [{ id: datos_emp[0].Turno_ID, text: datos_emp[0].Turno }] });
                    $("#cmb_turnos").val(datos_emp[0].Turno_ID).trigger("change");

                    $("#txt_numero_car").val(datos_emp[0].Numero_CAR);

                    $('#cmb_estatus').val(datos_emp[0].Estatus_ID);
                    $("#cmb_estatus")[0].innerHTML = "";
                    $("#cmb_estatus").select2({ data: [{ id: datos_emp[0].Estatus_ID, text: datos_emp[0].Estatus}] });
                    $("#cmb_estatus").val(datos_emp[0].Estatus_ID).trigger("change");

                    var _check_circunstancia1 = (datos_emp[0].Circunstancia_1);
                    var _check_circunstancia2 = (datos_emp[0].Circunstancia_2);
                    var _check_circunstancia3 = (datos_emp[0].Circunstancia_3);
                    var _check_circunstancia4 = (datos_emp[0].Circunstancia_4);
                    var _check_circunstancia5 = (datos_emp[0].Circunstancia_5);
                    var _check_circunstancia6 = (datos_emp[0].Circunstancia_6);
                    var _check_circunstancia7 = (datos_emp[0].Circunstancia_7);
                    var _check_circunstancia8= (datos_emp[0].Circunstancia_8);

                    if (_check_circunstancia1 == 1) {
                        $('#cb_circunstancia_1').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_1').attr('checked', false);
                    }
                    if (_check_circunstancia2 == 1) {
                        $("#cb_circunstancia_2").attr('checked', true);
                    }
                    else {
                        $("#cb_circunstancia_2").attr('checked', false);
                    }
                    if (_check_circunstancia3 == 1) {
                        $('#cb_circunstancia_3').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_3').attr('checked', false);
                    }
                    if (_check_circunstancia4 == 1) {
                        $('#cb_circunstancia_4').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_4').attr('checked', false);
                    }
                    if (_check_circunstancia5 == 1) {
                        $('#cb_circunstancia_5').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_5').attr('checked', false);
                    }
                    if (_check_circunstancia6  == 1) {
                        $('#cb_circunstancia_6').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_6').attr('checked', false);
                    }
                    if (_check_circunstancia7 !=null || _check_circunstancia7 != 0) {
                        $('#cb_circunstancia_7').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_7').attr('checked',false);
                    }
                    if (_check_circunstancia8 == 1) {
                        $('#cb_circunstancia_8').attr('checked', true);
                    }
                    else {
                        $('#cb_circunstancia_8').attr('checked', false);
                    }

                    $('#txt_descripcion_1').val(datos_emp[0].Descripcion_1);
                    $('#txt_descripcion_2').val(datos_emp[0].Descripcion_2);
                    $('#txt_descripcion_3').val(datos_emp[0].Descripcion_3);
                    $('#txt_descripcion_4').val(datos_emp[0].Descripcion_4);
                    $('#txt_descripcion_5').val(datos_emp[0].Descripcion_5);
                    $('#txt_descripcion_6').val(datos_emp[0].Descripcion_6);
                    $('#txt_descripcion_7').val(datos_emp[0].Descripcion_7);
                    $('#txt_condicion_buena').val(datos_emp[0].Condicion_Buena);
                    $('#txt_condicion_mala').val(datos_emp[0].Condicion_Mala);

                 
                }
            }
        });
    } catch (e) { }
}