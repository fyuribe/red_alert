﻿
$(document).on('ready', function () {
    _inicializar_pagina();
    $table = $('#tbl_alertas_rojas');
  
    
    _set_location_toolbar();
   
});
function _inicializar_pagina() {
    try {

        _agregar_tooltip_tabla();
     
        _crear_tbl_alertas_rojas();
        
        _set_location_toolbar();
        _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _set_location_toolbar() {
    $('#toolbar').parent().removeClass("pull-left");
    $('#toolbar').parent().addClass("pull-right");
}

function _init_btn_config(btn, text, alineacionTooltip) {
    $(btn).qtip({
        content: text,
        position: {
            corner: {
                target: 'topMiddle',
                tooltip: alineacionTooltip
            }
        },
        show: {
            when: { event: 'mouseover' },
            ready: false
        },
        hide: { event: 'mouseout' },
        style: {
            border: {
                width: 5,
                radius: 7
            },
            padding: 5,
            textAlign: 'center',
            tip: {
                corner: true,
                method: "polygon",
                border: 1,
                height: 20,
                width: 9
            },
            background: '#F5F6CE',
            color: '#2d2d30',
            width: 200,
            'font-size': 'small',
            'font-family': 'Calibri',
            'font-weight': 'Bold',
            tip: true,
            name: 'blue'
        }
    });
}
function _formato(row) {
    if (!row.id) { return row.text; }
    else if (row.id == row.text) return row.text;

    var _salida = '<span style="text-transform:uppercase;">' +
        '&nbsp;&nbsp;&nbsp;<b> &nbsp;&nbsp;' + row.text + '</b>' +
        '&nbsp;&nbsp;<i class="glyphicon glyphicon-minus"></i>&nbsp;' + row.detalle_1 +
        '</span>';

    return $(_salida);
}
function _templateSelection(row) {
    if (!row.id) { return row.text; }
    else if (row.id == row.text) return row.text;

    var _salida = '<span style="text-transform:uppercase;">' +
       '&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;' + row.text + '</b>' +
       '&nbsp;&nbsp;<i class="glyphicon glyphicon-minus"></i>&nbsp;' + row.detalle_1 +
       '</span>';

    return $(_salida);
}
function _mostrar_mensaje(Titulo, Mensaje) {
    bootbox.dialog({
        message: Mensaje,
        title: Titulo,
        locale: 'es',
        closeButton: true,
        buttons: [{
            label: 'Close',
            className: 'btn-default',
            callback: function () { }
        }]
    });
}
function _search_alertas_rojas_por_filtros() {
    var filtros = null;
    try {
        filtros = new Object();

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(filtros) });
        jQuery.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Alertas_Controller.asmx/Consultar_Alertas_Rojas',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function (datos) {
                if (datos !== null) {
                    $('#tbl_alertas_rojas').bootstrapTable('load', JSON.parse(datos.d));
                    _agregar_tooltip_tabla();
                    datos.d = (datos.d == undefined || datos.d == null) ? '[]' : datos.d;
                    //if (JSON.parse(datos.d).length > 0)
                    //    $("#ctrl_panel").click();
                }
            }
        });
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
}
function _crear_tbl_alertas_rojas() {
    try {
        $('#tbl_alertas_rojas').bootstrapTable('destroy');
        $('#tbl_alertas_rojas').bootstrapTable({
            cache: false,
            striped: true,
            pagination: true,
            pageSize: 10,
            pageList: [10, 25, 50, 100, 200],
            smartDysplay: false,
            search: true,
            showColumns: false,
            showRefresh: false,
            minimumCountColumns: 2,
            clickToSelect: false,
            columns: [
                { field: 'No_Alerta_Roja', title: 'Red Alert Number', width: '10%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Planta', title: 'AAM Plant', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Unidad_Negocio', title: 'Bussines Unit', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                { field: 'Numero_Parte', title: 'Part Number', width: '20%', align: 'left', valign: 'bottom', sortable: true, clickToSelect: false },
                {
                    field: 'Estatus', title: 'Status', width: '10%', align: 'left', valign: '', halign: 'left', sortable: true, clickToSelect: false, formatter: function (value, row) {
                        var color = '';
                        switch (value) {
                            case 'CANCELADO':
                                color = "rgba(221, 24, 24, 1)";
                                break;
                            case 'ABIERTO':
                                color = "yellow";
                                break;
                            case 'PROCESO':
                                color = 'rgba(85, 171, 237, 1)';
                                break;
                            case 'VENCIDO':
                                color = "orange";
                                break;
                            case 'FINALIZADO':
                                color = "green";
                                break;
                            case 'RECHAZADO':
                                color = "black";
                                break;
                            case 'AUTORIZADO':
                                color = 'rgba(3, 251,16, 1)';
                                break;
                            default:
                                color = 'rgba(85, 171, 237, 1)';
                        }
                        return '<i class="fa fa-circle" style="color: ' + color + '"></i>&nbsp;' + value;
                    }
                },
              {
                  field: 'Autorizar', title: 'Autorizar', align: 'center', width: '2%',
                  formatter: function (value, row) {
                      return '<div>' +
                                  '<a class="remove ml10 edit" id="remove_' + row.No_Alerta_Roja + '" href="javascript:void(0)" data-alerta_roja=\'' + JSON.stringify(row) + '\' onclick="btn_autorizar_click(this);"><i class="glyphicon glyphicon-ok"  title=""></i></a>' +
                             '</div>';
                  }
              },
                {
                    field: 'Rechazar', title: 'Rechazar', align: 'center', width: '2%',
                    formatter: function (value, row) {
                        return '<div>' +
                                    '<a class="remove ml10 edit " id="remove_' + row.No_Alerta_Roja + '" href="javascript:void(0)" data-alerta_roja=\'' + JSON.stringify(row) + '\' onclick="btn_rechazar_click(this);"><i class="glyphicon glyphicon-remove" title=""></i></a>' +
                               '</div>';
                    }
                },
                   {
                       field: 'Visualizar', title: 'Visualizar', align: 'center', width: '2%',
                   formatter: function (value, row) {
                      return '<div>' +
                                '<a class="remove ml10 edit _ver_orden_cambio" id="remove_' + row.No_Alerta_Roja + '" href="javascript:void(0)" data-alerta_roja=\'' + JSON.stringify(row) + '\' onclick="btn_visualizar_click(this);"><i class="glyphicon glyphicon-eye-open" title=""></i></a>' +
                             '</div>';
                         }
                     }


            ]
        });
    } catch (e) {

    }
}
function _agregar_tooltip_tabla() {
    _init_tbl_btn_config('._editar_cambio', 'Edit Alert', 'bottomLeft');
    _init_tbl_btn_config('._editar_cancelar', 'Cancel Alert', 'bottomLeft');
}
function _init_tbl_btn_config(classCss, text, alineacionTooltip) {
    $(classCss).each(function () {
        $(this).qtip({
            content: text,
            position: {
                corner: {
                    target: 'topMiddle',
                    tooltip: alineacionTooltip
                }
            },
            show: {
                when: { event: 'mouseover' },
                ready: false
            },
            hide: { event: 'mouseout' },
            style: {
                border: {
                    width: 5,
                    radius: 7
                },
                padding: 5,
                textAlign: 'center',
                tip: {
                    corner: true,
                    method: "polygon",
                    border: 1,
                    height: 20,
                    width: 9
                },
                background: '#F5F6CE',
                color: '#2d2d30',
                width: 200,
                'font-size': 'small',
                'font-family': 'Calibri',
                'font-weight': 'Bold',
                tip: true,
                name: 'blue'
            }
        });
    });
}
function btn_rechazar_click(renglon) {
    var row = $(renglon).data('alerta_roja');
    if (row.Estatus.toUpperCase() == "FINALIZADO") {
        _mostrar_mensaje('Validation', 'Unable to refuse a finished alert ');
        return;
    }

    if (row.Estatus.toUpperCase() == "RECHAZADO") {
        _mostrar_mensaje('Validation', 'You cannot refuse an alert already refused ');
        return;
    }
    if (row.Estatus.toUpperCase() == "AUTORIZADO") {
        _mostrar_mensaje('Validation', 'You cannot refuse an alert already authorized');
        return;
    }
    bootbox.confirm({
        title: 'Red Alert',
        message: 'Are you sure to refuse  the alert?',
        callback: function (result) {
            if (result) {
                //validar si es usuario
                //var usuario = _valida_usuarios();
                //if (usuario == true) {
                //    _mostrar_mensaje('Validation Report', 'Invalid user: unable to cancel the alert.');
                //    return;
                //}
                _rechazar_alerta_roja(row.No_Alerta_Roja);
                _mostrar_mensaje("Validation Report", "Red Alert successfully refuse.");
            }
        }
    });
}
function _rechazar_alerta_roja(no_alerta_roja) {
    var Alerta = null;
    var isComplete = false;
    try {
        Alerta = new Object();
        Alerta.No_Alerta_Roja = parseInt(no_alerta_roja);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Alertas_Controller.asmx/Rechazar_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;
}
function btn_autorizar_click(renglon) {
    var row = $(renglon).data('alerta_roja');
    if (row.Estatus.toUpperCase() == "FINALIZADO") {
        _mostrar_mensaje('Validation', 'Unable to refuse a finished alert ');
        return;
    }
  
    if (row.Estatus.toUpperCase() == "RECHAZADO") {
        _mostrar_mensaje('Validation', 'You cannot authorize an alert already refused ');
        return;
    }
    if (row.Estatus.toUpperCase() == "AUTORIZADO") {
        _mostrar_mensaje('Validation', 'You cannot authorize an alert already authorized ');
        return;
    }
    bootbox.confirm({
        title: 'Red Alert',
        message: 'Are you sure  to authorize the alert?',
        callback: function (result) {
            if (result) {
                //validar si es usuario
                //var usuario = _valida_usuarios();
                //if (usuario == true) {
                //    _mostrar_mensaje('Validation Report', 'Invalid user: unable to cancel the alert.');
                //    return;
                //}
                _autorizar_alerta_roja(row.No_Alerta_Roja);
                _mostrar_mensaje("Validation Report", "Red Alert successfully refuse.");
            }
        }
    });
}
function _autorizar_alerta_roja(no_alerta_roja) {
    var Alerta = null;
    var isComplete = false;
    try {
        Alerta = new Object();
        Alerta.No_Alerta_Roja = parseInt(no_alerta_roja);

        var $data = JSON.stringify({ 'jsonObject': JSON.stringify(Alerta) });
        $.ajax({
            type: 'POST',
            url: 'controllers/Ope_Autorizacion_Alertas_Controller.asmx/Autorizar_Alerta_Roja',
            data: $data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            cache: false,
            success: function ($result) {
                Resultado = JSON.parse($result.d);
                if (Resultado != null && Resultado != undefined && Resultado != '') {
                    if (Resultado.Estatus == 'success') {
                        validacion = true;
                    } else if (Resultado.Estatus == 'error') {
                        _mostrar_mensaje(Resultado.Titulo, Resultado.Mensaje);
                    }
                } else { _mostrar_mensaje("Error", "Unable to get results"); }
            }
        });
        _search_alertas_rojas_por_filtros();
    } catch (e) {
        _mostrar_mensaje('Technical Report', e);
    }
    return isComplete;
}
function btn_visualizar_click(alerta_roja) {
    var row = $(alerta_roja).data('alerta_roja');
    _mostrar_cambio_proceso(row.No_Alerta_Roja);
}
function lnk_panel_control_click(No_Alerta_Roja) {

    //Declaracion de variables
    var objURI = null; //variable para el objeto del URI
    var objParametros = null; //variable para los parametros

    try {
        window.open('Frm_Ope_Alertas_Rojas.aspx?No_Alerta_Roja='+ No_Alerta_Roja,'_self');

    } catch (ex) {
        _mostrar_mensaje("Informe T&eacute;cnico", ex);
    }
}
function _mostrar_cambio_proceso(No_Alerta_Roja) {
    lnk_panel_control_click(No_Alerta_Roja);
}

